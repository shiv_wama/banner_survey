<?php
class Signboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('SignboardModel');
        $this->load->model('CommonModel');
        $this->user_id = isset($_SESSION['USER']['uid']) ?  $_SESSION['USER']['uid'] : 0;
        $this->user_type = isset($_SESSION['USER']['user_type']) ?  $_SESSION['USER']['user_type'] : 0;
        // echo $this->user_id;die;
        if ($this->user_id  ==  0)
        redirect('AuthController');
        // if (($this->user_type != 'google_street_user')||($this->user_type != 'Admin')) {
        if(!in_array($this->user_type,array('google_street_user','Admin'))){
           echo json_encode(['status' => 401, 'msg' => "You don't have permission to access this page."]);
            die;
        }
    }
    public function index()
    {
        $page_data['title'] = 'Signboard | Banner Survey';
        $this->load->view('includes/header');
        $this->load->view('signboard/list');
        $this->load->view('includes/footer');
    }

    public function list_data()
    {
        $all_data = array();
        $where['s.status']=1;
        $tab_col = "s.signboard_id,s.proper_road_name,b.first_name,b.last_name,m.media_type_value,w.ward_name,n.nature_of_property_value";
        $all_data = $this->SignboardModel->get_data_in_single_table($where, $tab_col);
        $output_var['data'] = array();
        if(isset($all_data)&&!empty($all_data))
        {
            $data_count=0;
            foreach ($all_data as $key=> $value) {
                if(!empty($value))
                {
                    $output_var['data'][$data_count][0]="GHR".$value['signboard_id'];
                    $output_var['data'][$data_count][1]=$value['first_name']." ".$value['last_name'];
                    $output_var['data'][$data_count][2]=$value['media_type_value'];
                    $output_var['data'][$data_count][3]=$value['ward_name'];
                    $output_var['data'][$data_count][4]=$value['proper_road_name'];
                    $output_var['data'][$data_count][5]=$value['nature_of_property_value'];
                    $output_var['data'][$data_count][6]='<div class="dropdown d-inline-block">
                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="ri-more-fill align-middle"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-end">
                                   
                                    <li><button type="button" onclick="editBoard(this)" 
                                    data-id="' . base64_encode($value['signboard_id']) . '" 
                                 
                                    class="dropdown-item edit-item-btn">
                                    <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit</button>
                                    </li></ul>
                            </div>';
                    $data_count++;
                }
            }
        }
        echo json_encode($output_var); 
    }
   public function type_list()
    {
        $GET = $this->input->get();
        // print_r($GET);
        $data = $this->CommonModel->getMultipleData($GET['table_name'],NULL,NULL);
        http_response_code(200);
        echo json_encode(['status'=>http_response_code(),'msg'=>'success',$GET['table_name']=>$data]);
        die;
    }
    //store
    public function save()
    {
        // echo '<pre>';
        // print_r($_POST);
        // die;
        $userId = $_POST['signboard_id'];
        if($userId > 0)
        {
            //update
            $oldData = $this->CommonModel->getSingleData('signboard_google_street',['signboard_id'=>$userId],NULL);
            $oldJsonData = json_encode($oldData);
            $updateArr = [
                'media_type_id'=>$_POST['media_type_id'],
                'ward_name_id'=>$_POST['ward_name_id'],
                'proper_road_name'=>$_POST['proper_road_name'],
                'property_id'=>$_POST['property_id'],
                'building_id'=>$_POST['building_id'],
                'no_of_visible_board'=>$_POST['no_of_visible_board'],	
                'legalization_possibilities'=>$_POST['legalization_possibilities'],	
                'nature_of_property_id'=>$_POST['nature_of_property_id'],
                'other_property_name'=>(($_POST['nature_of_property_id']==6)?$_POST['other_property_name']:""),
                'mobile_number'=>$_POST['mobile_number'],
                'p_name'=>$_POST['p_name'],	
                'p_address'=>$_POST['p_address'],	
                'photo_no'=>$_POST['photo_no'],	
                'approx_l1'=>$_POST['approx_l1'],	
                'approx_w1'=>$_POST['approx_w1'],
                'approx_l2'=>$_POST['approx_l2'],
                'approx_w2'=>$_POST['approx_w2'],
                'approx_l3'=>$_POST['approx_l3'],
                'approx_w3'=>$_POST['approx_w3'],
                'approx_l4'=>$_POST['approx_l4'],
                'approx_w4'=>$_POST['approx_w4'],
                'approx_l5'=>$_POST['approx_l5'],
                'approx_w5'=>$_POST['approx_w5'],
                'created_by_user_id'=> $this->user_id,
                'created_datetime'=>date('Y-m-d H:i:s')
            ];
            
          

            $res = $this->CommonModel->updateData('signboard_google_street',['signboard_id'=>$userId],$updateArr);

            $newData =  $this->CommonModel->getSingleData('signboard_google_street', ['signboard_id' => $userId], NULL);
            $newJsonData = json_encode($newData);

            $insertArr = [
                'table_name'=>'signboard_google_street', 
                'old_data'=>$oldJsonData, 
                'new_data'=>$newJsonData, 
                'updated_by'=> $this->user_id, 
                'created_at'=>date('Y-m-d H:i:s')

            ];
            $res = $this->CommonModel->insertData('logs',$insertArr);
            http_response_code(200);
            echo json_encode(['status'=>http_response_code(),'msg'=>'Data updated.']);die;
        }
        else{
            
            $insertArr = [
                'media_type_id'=>$_POST['media_type_id'],
                'ward_name_id'=>$_POST['ward_name_id'],
                'proper_road_name'=>$_POST['proper_road_name'],
                'property_id'=>$_POST['property_id'],
                'building_id'=>$_POST['building_id'],
                'no_of_visible_board'=>$_POST['no_of_visible_board'],	
                'legalization_possibilities'=>$_POST['legalization_possibilities'],	
                'nature_of_property_id'=>$_POST['nature_of_property_id'],
                'other_property_name'=>(($_POST['nature_of_property_id']==6)?$_POST['other_property_name']:""),
                'mobile_number'=>$_POST['mobile_number'],
                'p_name'=>$_POST['p_name'],	
                'p_address'=>$_POST['p_address'],	
                'photo_no'=>$_POST['photo_no'],	
                'approx_l1'=>$_POST['approx_l1'],	
                'approx_w1'=>$_POST['approx_w1'],
                'approx_l2'=>$_POST['approx_l2'],
                'approx_w2'=>$_POST['approx_w2'],
                'approx_l3'=>$_POST['approx_l3'],
                'approx_w3'=>$_POST['approx_w3'],
                'approx_l4'=>$_POST['approx_l4'],
                'approx_w4'=>$_POST['approx_w4'],
                'approx_l5'=>$_POST['approx_l5'],
                'approx_w5'=>$_POST['approx_w5'],
                'created_by_user_id'=> $this->user_id,
                'created_datetime'=>date('Y-m-d H:i:s')
            ];

            $res = $this->CommonModel->insertData('signboard_google_street',$insertArr);
            http_response_code(200);
            echo json_encode(['status' => http_response_code(), 'msg' => 'Data Submitted.']);
            die;
        }
    }
    public function signboard_data()
    {
        $data = json_decode(file_get_contents('php://input'),true);
        $id = base64_decode($data['id']);

        $media_typeData = $this->CommonModel->getMultipleData('media_type',NULL,NULL);
        $nature_of_propertyData = $this->CommonModel->getMultipleData('nature_of_property',NULL,NULL);
        $wardsData = $this->CommonModel->getMultipleData('wards',NULL,NULL);
        $signboard_data = $this->CommonModel->getSingleData('signboard_google_street',['signboard_id'=>$id], [
            'signboard_id',
            'media_type_id',
            'ward_name_id',
            'proper_road_name',
            'property_id',
            'building_id',
            'no_of_visible_board',
            'legalization_possibilities',
            'nature_of_property_id',
            'other_property_name',
            'mobile_number',
            'p_name',
            'p_address',
            'photo_no',
            'approx_l1',
            'approx_w1',
            'approx_l2',
            'approx_w2',
            'approx_l3',
            'approx_w3',
            'approx_l4',
            'approx_w4',
            'approx_l5',
            'approx_w5',
        ]);
        http_response_code(200);
        echo json_encode(['status'=>http_response_code(),'msg'=>'Success','data'=>$signboard_data, 'media_type' => $media_typeData, 'nature_of_property' => $nature_of_propertyData, 'wards' => $wardsData]);die;
    }
}
