        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <style>
            th,
            td {
                white-space: nowrap;
            }

   
        </style>
        <div class="main-content">
            <div class="page-content">
                <div class="container-fluid">
                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0">Ward Summary</h4>
                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-md-4 text-left">
                                            <div class="mb-1">
                                                <label for="">Date Range</label>
                                                <input type="text" name="ward__summary__date__range" data-provider="flatpickr" data-date-format="Y-m-d" data-range-date="true" class="form-control ward__summary__date__range">
                                            </div>
                                        </div>

                                        <div class="col-md-4 text-left">
                                            <div class="mb-1">
                                                <label for="">Media Type</label>
                                                <select  name="media__type__id"  class="form-control media__type__id" onchange="wardSummaries();">
                                                    <option value="" selected disabled>Please select media type</option>
                                                <?php
                                                if(isset($media_type_arr)) :
                                                    foreach($media_type_arr as $value):
                                                        echo '<option value="'.$value['media_type_id'].'">'.$value['media_type_value'].'</option>';
                                                    endforeach;

                                                endif;
                                                
                                                ?>
                                                    
                                            </select>
                                            </div>
                                        </div>


                                        <div class="col-md-4 text-right">
                                            <a class="btn btn-primary btn-sm csv_download_btn" href="<?= site_url('download-ward-summary') ?>"> <i class=" bx bx-down-arrow-circle"> CSV</i></a>
                                        </div>
                                        <!-- <div class="col-md-12 ward__name__div" > -->
                                        <!-- <div class="mb-1">
                                                <label for="">Ward Name</label>
                                                <select multiple name="ward__id[]" id="ward__ids" class="form-select">
                                                    <?php
                                                    if (isset($ward_data_arr) && !empty($ward_data_arr)) :
                                                        foreach ($ward_data_arr as $key => $value) :
                                                            echo '<option value="' . $value['ward_name_id'] . '">' . $value['ward_name'] . '</option>';
                                                        endforeach;
                                                    endif;
                                                    ?></select>
                                            </div> -->
                                        <!-- </div> -->
                                        <!-- <div class="col-md-3">
                                            <div class="mb-1">
                                                <label for="">Date Range</label>
                                                <input type="text" name="date-range-filter" data-provider="flatpickr" data-date-format="Y-m-d" data-range-date="true" id="date-range-filter" class="form-control date_range_filter">
                                            </div>
                                        </div> -->
                                        <!-- <div class="col-md-9 text-right">
                                            <a class="btn btn-primary btn-sm csv_download_btn" href="<?= site_url('download-survey-list-csv/null') ?>"> <i class=" bx bx-down-arrow-circle"> CSV</i></a>
                                        </div> -->
                                        <!-- </div> -->
                                    </div>
                                </div><!-- header end -->
                                <div class="card-body">
                                    <table class="ward_summary_datatable table nowrap align-middle  stripe row-border order-column" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Ward Name</th>
                                                <th>Surveyed Count</th>
                                                <th>Illegal</th>
                                                <th>Signing In Progress</th>
                                                <th>Total Notice Signed</th>
                                                <th>Notice Delivered</th>
                                                <th>Notice To Be Delivered</th>
                                                <th>Approched For Sanction </th>
                                                <th>Ready For Action</th>
                                                <th>Legalized</th>
                                                <th>Demolished</th>
                                                <th>Waiting For Response</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Ward Name</th>
                                                <th>Surveyed Count</th>
                                                <th>Illegal</th>
                                                <th>Signing In Progress</th>
                                                <th>Total Notice Signed</th>
                                                <th>Notice Delivered</th>
                                                <th>Notice To Be Delivered</th>
                                                <th>Approched For Sanction </th>
                                                <th>Ready For Action</th>
                                                <th>Legalized</th>
                                                <th>Demolished</th>
                                                <th>Waiting For Response</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>