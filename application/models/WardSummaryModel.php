<?php
class WardSummaryModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        
    }

    //getWardSurvedCount
    public function getWardSurvedCount($ward_id,$start_date,$end_date,$media_type_id)
    {
        $this->db->from('banner_survey bS');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        if($media_type_id!=NULL)
        $this->db->where('bS.media_type_id',$media_type_id);
        $this->db->where('bS.ward_name_id',$ward_id);
       
        if($start_date!=NULL && $end_date!=NULL)
        $this->db->where(['bS.created_date >='=>$start_date,'bS.created_date <='=>$end_date]);
        return $this->db->get()->num_rows();
    }

    //getWardIllegaCount
    public function getWardBannerStatusCount($ward_id,$banner_status,$start_date,$end_date,$media_type_id)
    {

        // echo $start_date.'||'.$end_date;die;
        // $this->db->select('count(bS.banner_survey_id)');


        // $this->db->from('banner_survey bS');
        // $this->db->join('banner_status bnS', 'bS.banner_status_id = bnS.id');
        // // $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        // // $this->db->like('mT.media_type_value', 'Hoarding', 'both');
        // $this->db->like('bnS.banner_status_value', 'Ready For Sign', 'both');
        // $hoardingSignedProgress = $this->db->get()->num_rows();

        $this->db->from('banner_survey bS');
        $this->db->join('banner_status bnS','bS.banner_status_id = bnS.id','INNER');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        if($media_type_id!=NULL)
        $this->db->where('bS.media_type_id',$media_type_id);
        $this->db->like('bnS.banner_status_value',$banner_status,'both');
        $this->db->where('bS.ward_name_id',$ward_id);    
        if($start_date!=NULL && $end_date!=NULL)
        $this->db->where(['bS.created_date >='=>$start_date,'bS.created_date <='=>$end_date]);
        // $data = $this->db->get_compiled_select();
        // echo '<pre>';
        // print_r($data);
        // die;

        return $this->db->get()->num_rows();
    }
    
   
}
