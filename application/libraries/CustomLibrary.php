<?php
class CustomLibrary
{


    function array_push_assoc($array, $key, $value)
    {
        $array[$key] = $value;
        return $array;
    }



    //donwnloadCsv
    public function downloadCSV($header,$data,$file_name)
    {
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$file_name");
        header("Content-Type: application/csv; ");
        // file creation
        $fileName = fopen('php://output', 'w');
        $hederArr = $header;
        fputcsv($fileName, $hederArr);
        if (isset($data) && !empty($data)) {
                foreach ($data as $key => $value) {

                fputcsv($fileName, $value);
            };
        }
        fclose($fileName);
        exit;
    }

    public function dateDiff($start_date,$end_date)
    {
        $diff = abs(strtotime($start_date) - strtotime($end_date));
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
        return [
            'days'=>$days,
            'months'=>$months,
            'years'=>$years

        ];
    }


    //uploadImage
    public function uploadImage($image_name,$path,$files,$temp_name)
    {
        try {
        
        $banner=$files;
        $expbanner=explode('.',$banner);
        $extention=$expbanner[1];
        $bannername=$image_name.'.'.$extention;
       
        $bannerpath=$_SERVER["DOCUMENT_ROOT"].'/banner_survey_v1/'.$path.'/'.$bannername;
        // echo $bannerpath;die;
        move_uploaded_file($temp_name,$bannerpath);
           return true;
        } catch (\Exception $e) {
            return $e->getMessage();
            
        }

        
    }
    
}
