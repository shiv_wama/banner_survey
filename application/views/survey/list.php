        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0">Banner Survey List</h4>

                                <!-- <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Tables</a></li>
                                        <li class="breadcrumb-item active">Datatables</li>
                                    </ol>
                                </div> -->

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->



                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row">

                                    <div class="col-md-12 ward__name__div" >
                                            <div class="mb-1">
                                                <label for="">Ward Name</label>
                                                <select multiple name="ward__id[]" id="ward__ids" class="form-select">
                                                    <?php
                                                    if (isset($ward_data_arr) && !empty($ward_data_arr)) :
                                                        foreach ($ward_data_arr as $key => $value) :
                                                            echo '<option value="' . $value['ward_name_id'] . '">' . $value['ward_name'] . '</option>';
                                                        endforeach;
                                                    endif;
                                                    ?></select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="mb-1">
                                                <label for="">Date Range</label>

                                                <input type="text" name="date-range-filter" data-provider="flatpickr" data-date-format="Y-m-d" data-range-date="true" id="date-range-filter" class="form-control date_range_filter">
                                            </div>
                                        </div>
                                        
                                                    <div class="col-md-3">
                                                        <label for="">Banner ID</label>
                                                        <select name="banner_id" id="banner_id" class="form-select">
                                                            <option value="" selected disabled>Please select banner id</option>
                                                            <?php
                                                            if(isset($banner_id_data) && !empty($banner_id_data)):
                                                                foreach ($banner_id_data as $key => $value) {
                                                                    echo '<option value="'.$value['banner_id'].'"># '.$value['banner_id'].'</option>';
                                                                }
                                                                endif
                                                            ?>
                                                        </select>
                                                    </div>



                                        <div class="col-md-4 text-right">
                                            <a class="btn btn-primary btn-sm csv_download_btn" href="<?= site_url('download-survey-list-csv/null')?>"> <i class=" bx bx-down-arrow-circle"> CSV</i></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body">
                                    <table class="banner__survey_datatable table nowrap align-middle" style="width:100%">
                                        <thead>
                                            <tr>

                                                <th>Banner Id</th>
                                                <th>Ward Name</th>
                                                <th>Map</th>
                                                <th>Media Type</th>
                                                <th>Address</th>

                                                <th>Advirtisor Name</th>
                                                <th>Society Name</th>
                                                <th>Survey Date</th>

                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>



                                        </tbody>

                                        <tfoot>
                                            <tr>

                                                <th>Banner Id</th>
                                                <th>Ward Name</th>
                                                <th>Map</th>
                                                <th>Media Type</th>
                                                <th>Address</th>

                                                <th>Advirtisor Name</th>
                                                <th>Society Name</th>
                                                <th>Survey Date</th>

                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>