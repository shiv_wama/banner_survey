        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0">User List</h4>

                                <!-- <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Tables</a></li>
                                        <li class="breadcrumb-item active">Datatables</li>
                                    </ol>
                                </div> -->

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->



                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <button type="button" class="btn btn-primary btn-sm" onclick="addUser()" >Add User</button>
                                        </div>
                                        <!-- <div class="col-md-3">
                                            <div class="mb-1">
                                                <label for="">Date Range</label>

                                                <input type="text" name="date-range-filter" data-provider="flatpickr" data-date-format="Y-m-d" data-range-date="true" id="date-range-filter" class="form-control date_range_filter">
                                            </div>
                                        </div> -->
                                    </div>
                                </div>

                                <div class="card-body">
                                    <table class="user_list_datatable table nowrap align-middle" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>User ID</th>
                                                <th>User Detail</th>
                                                <th>User Type</th>
                                                <th>SQID</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>



                                        </tbody>

                                        <tfoot>
                                            <tr>
                                                <th>User ID</th>
                                                <th>User Detail</th>
                                                <th>User Type</th>
                                                <th>SQID</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>