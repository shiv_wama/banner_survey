<style>
    .section_info {
        font-weight: 700;
        color: #074954fc;
    }
</style>
<!-- Modal (form1) -->
<div class="modal fade zoomIn" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Data (Form 1) <span class="banner_id_heading"></span></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?= site_url('update-survey-data') ?>" class="banner_survey_frm" method="POST" onsubmit="return false" enctype="multipart/form-data">
                    <div class="row">

                        <div class="col-md-12">
                            <span class="section_info">Location details</span>
                        </div>

                        <div class="col-md-3">
                            <label for="">Survey location</label>
                            <input readonly type="text" class="form-control" placeholder="Survey location" name="survey__location">
                        </div>

                        <div class="col-md-3">
                            <label for="">Banner location</label>
                            <input type="text" class="form-control" readonly placeholder="Banner location" name="banner__location">
                        </div>

                        <div class="col-md-3">
                            <label for="">Ward name</label>
                            <select name="ward_name_id" class="form-control">
                            <option value="" selected disabled>Please select ward name</option>
                            <?php
                            if(isset($ward_data_arr) && !empty($ward_data_arr)):
                                    foreach ($ward_data_arr as $key => $value) {
                                        echo '<option value="'.$value['ward_name_id'].'">'.$value['ward_name'].'</option>';
                                    }

                            endif;
                            ?>
                            </select>

                        </div>

                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-12">
                            <span class="section_info">Banner Details</span>
                        </div>

                        <input type="hidden" name="form__number" value="1">
                        <input type="hidden" name="banner__id">
                        <div class="col-md-3">
                            <label for="">Media Type</label>
                            <select name="media__type" id="media__type" class="form-select">

                                <option value="">Select Media Type</option>
                            </select>
                        </div>

                        <div class="col-md-3 hoarding__media__type">
                            <label for="">Plate Available</label>
                            <select name="plate__available" id="plate__available" class="form-select">

                                <option value="">Select Available Plate</option>
                            </select>
                        </div>


                        <div class="col-md-3 hoarding__media__type">
                            <label for="">Installed On</label>
                            <select name="installed__on" id="installed__on" class="form-select">

                                <option value="">Select Installed on</option>
                            </select>
                        </div>



                        <div class="col-md-3 hoarding__media__type">
                            <label for="">Banner Status</label>
                            <select name="banner__status__id" id="banner__status__id" class="form-select"></select>
                        </div>

                        <div class="col-md-3 hoarding__media__type">
                            <label for="">1.5 meters from Road</label>
                            <select name="1_5_meater_from_road" id="1_5_meater_from_road" class="form-select">

                                <option value="">Select 1.5 meters from road</option>
                            </select>
                        </div>

                        <div class="col-md-3 hoarding__media__type">
                            <label for="">Board Nature</label>
                            <select name="board__nature" id="board__nature" class="form-select">

                                <option value="">Select Board Nature</option>
                            </select>
                        </div>


                        <div class="col-md-3 hoarding__media__type">
                            <label for="">Old Id</label>
                            <input type="text" class="form-control" placeholder="Old Id" name="old__id">
                        </div>



                        <div class="col-md-3">
                            <label for="">Banner owner name</label>
                            <input type="text" class="form-control" placeholder="Banner owner name" name="banner__owner__name">
                        </div>


                        <div class="col-md-3">
                            <label for="">Banner contact number</label>
                            <input type="number" class="form-control" placeholder="Banner owner name" name="banner__contact__number">
                        </div>

                        <div class="col-md-12 hoarding__media__type">
                            <hr>
                        </div>
                        <!-- banner aprox dimensions -->
                        <div class="col-md-12 hoarding__media__type">
                            <span class="section_info">Approx. Banner Dimensions (In Feet)</span>
                        </div>

                        <div class="col-md-3 hoarding__media__type">
                            <label for="">Approx Banner Length (in feet)</label>
                            <input type="text" class="form-control" placeholder="Approx Banner Length (in feet)" name="approx_banner_length">
                        </div>

                        <div class="col-md-3 hoarding__media__type">
                            <label for="">Approx Banner Width (in feet)</label>
                            <input type="text" class="form-control" placeholder="Approx Banner Width (in feet)" name="approx_banner_width">
                        </div>

                        <div class="col-md-3 hoarding__media__type">
                            <label for="">Approx Banner Height (in feet)</label>
                            <input type="text" class="form-control" placeholder="Approx Banner Height (in feet)" name="approx_banner_height">
                        </div>

                        <div class="col-md-12 hoarding__media__type">
                            <span class="info_sub_heading">
                                Existing measurements as on field measurements
                            </span>
                        </div>

                        <div class="col-md-3 hoarding__media__type">
                            <label for="">Existing Length</label>
                            <input type="text" class="form-control exisiting__length" onkeyup="calculateExistingArea()" placeholder="Existing length" name="exisiting__length">
                        </div>

                        <div class="col-md-3 hoarding__media__type">
                            <label for="">Existing Breadth</label>
                            <input type="text" class="form-control existing__breadth" onkeyup="calculateExistingArea()" placeholder="Existing breadth" name="existing__breadth">
                        </div>

                        <div class="col-md-6 hoarding__media__type">
                            <label for="">Existing Area</label>
                            <input type="text" readonly class="form-control" placeholder="Existing area" id="existing__area" name="existing__area">
                        </div>



                        <div class="col-md-12 hoarding__media__type">
                            <span class="info_sub_heading">
                                Actual measurements as on PMC record measurements
                            </span>
                        </div>

                        <div class="col-md-3 hoarding__media__type">
                            <label for="">Actual Length</label>
                            <input type="text" class="form-control actual__length" onkeyup="calculateActualArea()" placeholder="Actual length" name="actual__length">
                        </div>

                        <div class="col-md-3 hoarding__media__type">
                            <label for="">Actual Breadth</label>
                            <input type="text" class="form-control actual__breadth" onkeyup="calculateActualArea()" placeholder="Actual breadth" name="actual__breadth">
                        </div>

                        <div class="col-md-6 hoarding__media__type">
                            <label for="">Actual Area</label>
                            <input type="text" class="form-control" readonly placeholder="Actual area" id="actual__area" name="actual__area">
                        </div>


                        <div class="col-md-3 hoarding__media__type">
                            <label for="">Difference in square feet</label>
                            <input readonly type="text" class="form-control diff__square__feet" placeholder="Difference in square feet" name="diff__square__feet">
                        </div>


                        <div class="col-md-3 hoarding__media__type">
                            <label for="">Additional ratable value</label>
                            <input type="text" class="form-control additional__ratable__value" readonly placeholder="Additional ratable value" name="additional__ratable__value">
                        </div>


                        <div class="col-md-3 hoarding__media__type">
                            <label for="">New square feet</label>
                            <input type="text" readonly class="form-control new__square__feet" placeholder="New square feet" name="new__square__feet">
                        </div>


                        <div class="col-md-3 hoarding__media__type">
                            <label for="">New ratable value in square feet</label>
                            <input type="text" readonly class="form-control new__ratable__value__square__feet" placeholder="New ratable value in square feet" name="new__ratable__value__square__feet">
                        </div>

                        <div class="col-md-12">
                            <hr>
                        </div>


                        <div class="col-md-12">
                            <span class="section_info">Advertiser Details </span>
                        </div>

                        <div class="col-md-3 ">
                            <label for="">Advertisor Name</label>
                            <input type="text" class="form-control advertiser__name" placeholder="Advertise Name (Optional)" name="advertiser__name">
                        </div>


                        <div class="col-md-9">
                            <label for="">Advertisor Address</label>
                            <textarea class="form-control" placeholder="Advertiser address (optional)" name="advertisor__address"></textarea>
                        </div>




                        <div class="col-md-12 signboard__details">
                            <div class="col-md-12">
                                <hr>
                            </div>

                            <span class="section_info">Signboard Details </span>
                        </div>


                        <div class="col-md-3 signboard__details">
                            <label for="">Signboard Type</label>
                            <select name="signboard__type__id" class="form-control">
                                <option value="" selected disabled>Select Signboard type</option>
                                <?php
                                if (isset($signboard_types) && !empty($signboard_types)) :
                                    foreach ($signboard_types as $key => $value) :
                                        echo '<option value="' . $value['id'] . '">' . $value['signboard_type_name'] . '</option>';
                                    endforeach;

                                endif;
                                ?>
                            </select>
                        </div>



                        <div class="col-md-3 signboard__details">
                            <label for="">Legalization Possibility</label>
                            <select name="legalization__possibility__id" class="form-control">
                                <option value="" selected disabled>Select legalization posibility</option>
                                <?php
                                if (isset($leglization_posibilities) && !empty($leglization_posibilities)) :
                                    foreach ($leglization_posibilities as $key => $value) :
                                        echo '<option value="' . $value['legalization_possibility_id'] . '">' . $value['legalization_possibility_value'] . '</option>';
                                    endforeach;

                                endif;

                                ?>

                            </select>
                        </div>





                        <div class="col-md-12 signboard__details">
                            <hr>
                        </div>


                        <div class="col-md-12 signboard__details">
                            <span class="section_info">Mandatory Signboard Measurements</span>
                        </div>

                        <div class="signboard__div signboard__details">

                            <div class="row">

                                <div class="col-md-2 ">
                                    <label for="">Length</label>
                                    <input onkeyup="calculateArea(0)" type="number" class="form-control signboard__length__0" placeholder="Length" name="signboard__length">
                                </div>

                                <div class="col-md-2">
                                    <label for="">Breadth</label>
                                    <input type="number" onkeyup="calculateArea(0)" class="form-control signboard__breadth__0" placeholder="Breadth" name="signboard__breadth">
                                </div>


                                <div class="col-md-2">
                                    <label for="">Area (in SQ FT)</label>
                                    <input type="number" class="form-control signboard__sqft__0" placeholder="Signboard Area " name="signboard__sqft" readonly>
                                </div>

                                <div class="col-md-3">
                                    <label for="">Additional SQ FT</label>
                                    <input type="number" readonly class="form-control signboard__additional_sqft__0" placeholder="Additional SQ FT" name="signboard__additional_sqft">
                                </div>



                                <div class="col-md-3">
                                    <label for="">Additional rateble value</label>
                                    <input type="number" readonly class="form-control signboard__additional__rateble__value__0" placeholder="Additional rateble value" name="signboard__additional__rateble__value">
                                </div>



                                <div class="col-md-4 signboard__image1__upload">
                                    <label for="">Signboard Image 1</label>
                                    <input class="form-control" type="file" name="signboard__image1">
                                </div>


                                <div class="col-md-3 signboard__image1__preview" style="display:none">
                                    <label for="">Signboard Image 1</label>
                                    <img src="" alt="" id="signboard__image1__preview" onerror="this.src='https://via.placeholder.com/150'" class="form-control banner-img">
                                </div>


                                <div class="col-md-4 signboard__image2__upload">
                                    <label for="">Signboard Image 2</label>
                                    <input class="form-control" type="file" name="signboard__image2">
                                </div>

                                <div class="col-md-3 signboard__image2__preview" style="display:none">
                                    <label for="">Signboard Image 2</label>
                                    <img src="" alt="" id="signboard__image2__preview" onerror="this.src='https://via.placeholder.com/150'" class="form-control banner-img">
                                </div>



                                <div class="col-md-4" style="margin-top: 28px;">

                                    <a onclick="addMoreSignboards()" class="btn btn-sm btn-primary">Add more</a>
                                </div>
                            </div>

                        </div>

                        <div class="signboard__details">
                            <div class="row">
                                <div class="col-md-12 ">
                                    <span class="section_info">Additional Board (Total Measurements)</span>
                                </div>

                                <div class="col-md-6 mt-1">
                                    <span> Total Area (Sq Ft.)</span>
                                </div>

                                <div class="col-md-6 mt-1">
                                    <input type="text" readonly name="total__additional__area" class="form-control">
                                </div>



                                <div class="col-md-6 mt-1">
                                    <span> Total Rateble Value</span>
                                </div>

                                <div class="col-md-6 mt-1">
                                    <input type="text" readonly name="total__additional__rateble__value" class="form-control">
                                </div>


                                <div class="col-md-6 mt-1">
                                    <span> Additional Boards (Count)</span>
                                </div>

                                <div class="col-md-6 mt-1">
                                    <input type="text" readonly name="total__additional__board__count" class="form-control">
                                </div>





                                <div class="col-md-12  mt-1">
                                    <span class="section_info">Total Mesurements</span>
                                </div>

                                <div class="col-md-6 mt-1">
                                    <span> Total Area (Sq Ft.)</span>
                                </div>

                                <div class="col-md-6 mt-1">
                                    <input type="text" readonly name="total__area" class="form-control">
                                </div>



                                <div class="col-md-6 mt-1">
                                    <span> Total Rateble Value</span>
                                </div>

                                <div class="col-md-6 mt-1">
                                    <input type="text" readonly name="total__rateble__value" class="form-control">
                                </div>





                            </div>
                        </div>
























                        <div class="col-md-12">
                            <hr>
                        </div>


                        <div class="col-md-12">
                            <span class="section_info">Property Details <small>(Details of property where the banner is placed)</small></span>
                        </div>
                        <div class="col-md-3">
                            <label for="">Property No</label>
                            <input type="text" class="form-control" placeholder="Property number" name="property__number">
                        </div>

                      <!--   <div class="col-md-3 hoarding__media__type">
                            <label for="">Property Name</label>
                            <input type="text" class="form-control" placeholder="Property Name" name="property__name">
                        </div> -->

                        <div class="col-md-3">
                            <label for="">Property Contact Number</label>
                            <input type="number" class="form-control" placeholder="Property Contact Number" name="property_contact_number">
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>

                        <div class="col-md-12">
                            <span class="section_info">Property Address <small>(Address of property where the banner is placed)</small></span>
                        </div>

                        <div class="col-md-3">
                            <label for="">Society Name</label>
                            <input type="text" class="form-control" placeholder="Society Name" name="society__name">
                        </div>

                        <div class="col-md-3">
                            <label for="">Landmark</label>
                            <input type="text" class="form-control" placeholder="Landmark" name="landmark">
                        </div>
                        <div class="col-md-3">
                            <label for="">Proper Road Name</label>
                            <input type="text" class="form-control" placeholder="Proper Road Name" name="proper_roead_name">
                        </div>

                        <div class="col-md-3">
                            <label for="">Address</label>
                            <textarea class="form-control" id="address" name="address" placeholder="address" cols="30" rows="3"></textarea>
                        </div>



                        <div class="col-md-3">
                            <label for="">Road Status</label>
                            <input type="text" class="form-control" placeholder="Road Status" name="road_status">
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>

                        <div class="col-md-12">
                            <span class="section_info">Other Details</span>
                        </div>
                        <div class="col-md-12">
                            <label for="">Remarks</label>
                            <textarea class="form-control" id="remarks" name="remarks" placeholder="Remarks" cols="30" rows="3"></textarea>
                        </div>

                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-12">
                            <span class="section_info">Banner Images</span>
                        </div>

                        <style>
                            .banner-img {
                                height: 120px;
                                width: 250px;
                                object-fit: cover;
                            }
                        </style>

                        <div class="col-md-3">
                            <label for="">Building photo along with hoarding</label>
                            <img src="" alt="" id="image_1" onerror="this.src='https://via.placeholder.com/150'" class="form-control banner-img">
                        </div>

                        <div class="col-md-3">
                            <label for="">Entire board with stand photo</label>
                            <img src="" alt="" id="image_2" onerror="this.src='https://via.placeholder.com/150'" class="form-control banner-img">
                        </div>

                        <div class="col-md-3">
                            <label for="">Hoarding photo</label>
                            <img src="" alt="" id="image_3" onerror="this.src='https://via.placeholder.com/150'" class="form-control banner-img">
                        </div>

                        <div class="col-md-3">
                            <label for="">Additinal photo 1</label>
                            <img src="" alt="" id="image_4" onerror="this.src='https://via.placeholder.com/150'" class="form-control banner-img">
                        </div>

                        <div class="col-md-3">
                            <label for="">Hoarding plate photo</label>
                            <img src="" alt="" id="image_5" onerror="this.src='https://via.placeholder.com/150'" class="form-control banner-img">
                        </div>























                        <!-- 
                        <div class="col-md-3">
                            <label for="">Ward Name </label>
                            <select name="ward__name__id" id="ward__name__id" class="form-select"></select>
                        </div> -->







                        <div class="col-md-12">
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-success">Save changes</button>
                        </div>

                    </div>

                </form>
            </div>

        </div>
    </div>
</div>

<!-- Modal (form2) -->
<div class="modal fade zoomIn" id="form_2_modal" tabindex="-1" aria-labelledby="form_2_modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="form_2_modalLabel">Edit Data (Form 2)</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form action="<?= site_url('update-survey-data') ?>" class="banner_survey_frm__2" method="POST" onsubmit="return false">
                    <div class="row">

                        <input type="hidden" name="banner__id">
                        <input type="hidden" name="form__number" value="2">

                        <!-- <div class="col-md-3">
                            <div class="mb-1">
                                <label for="">Ward Name</label>
                                <select name="ward__id" id="ward__id" class="form-select">

                                </select>
                            </div>
                        </div> -->

                        <div class="col-md-3">
                            <label for="">Owner Name</label>
                            <input type="text" class="form-control" placeholder="Owner Name" name="owner__name">
                        </div>

                        <div class="col-md-3">
                            <label for="">Owner UID</label>
                            <input type="text" class="form-control" placeholder="Owner UID" name="owner__uid">
                        </div>

                        <div class="col-md-3">
                            <label for="">Owner Type</label>
                            <select name="owner__type__id" id="owner__type__id" class="form-select">

                            </select>
                        </div>

                        <div class="col-md-3">
                            <label for="">Govt Type</label>
                            <select name="govt__type__id" id="govt__type__id" class="form-select">

                            </select>
                        </div>

                        <div class="col-md-3">
                            <label for="">Occupier Name</label>
                            <input type="text" class="form-control" placeholder="Occupier Name" name="occupier__name">
                        </div>

                        <div class="col-md-3">
                            <label for="">Occupier UID</label>
                            <input type="number" class="form-control" placeholder="Occupier UID" name="occupier__uid">
                        </div>

                        <div class="col-md-3">
                            <label for="">Property Number</label>
                            <input type="text" class="form-control" placeholder="Property Name" name="property__name">
                        </div>

                        <div class="col-md-3">
                            <label for="">Proper Landmark</label>
                            <input type="text" class="form-control" placeholder="Proper Landmark" name="proper__landmark">
                        </div>

                        <div class="col-md-3">
                            <label for="">Proper Road Name</label>
                            <input type="text" class="form-control" placeholder="Proper road Name" name="proper__road__name">
                        </div>

                        <div class="col-md-3">
                            <label for="">More Details</label>
                            <textarea class="form-control" id="more__details" name="more__details" placeholder="More Details" cols="30" rows="3"></textarea>
                        </div>

                        <div class="col-md-3">
                            <label for="">Flat Number</label>
                            <input type="text" class="form-control" placeholder="Flat Number" name="flat__number">
                        </div>

                        <div class="col-md-3">
                            <label for="">House Number</label>
                            <input type="text" class="form-control" placeholder="House Number" name="house__number">
                        </div>

                        <div class="col-md-3">
                            <label for="">Bunglow Number</label>
                            <input type="text" class="form-control" placeholder="Bunglow Number" name="bunglow__number">
                        </div>

                        <div class="col-md-3">
                            <label for="">Pincode</label>
                            <input type="number" class="form-control" placeholder="Pincode" name="pincode">
                        </div>

                        <div class="col-md-3">
                            <label for="">Installation year</label>
                            <input type="text" data-provider="flatpickr" data-date-format="Y" class="form-control" placeholder="Installation year" name="installation_year">
                        </div>

                        <div class="col-md-3">
                            <label for="">License Renewed Till</label>
                            <input type="text" data-provider="flatpickr" data-date-format="Y-m-d" class="form-control" placeholder="License Renewed Till" name="license__renewed__till">
                        </div>

                        <div class="col-md-3">
                            <label for="">Property owner Address</label>
                            <textarea class="form-control" id="prop__owener__address" name="prop__owner__address" placeholder="Property Owner Address" cols="30" rows="3"></textarea>
                        </div>

                        <div class="col-md-3">
                            <label for="">Property owner Mobile Number</label>
                            <input type="number" class="form-control" placeholder="Proprerty Owner Mobile Number" name="property__owner__mobile__number">
                        </div>

                        <div class="col-md-3">
                            <label for="">Property Occupier Address</label>
                            <textarea class="form-control" id="prop__occupier__address" name="prop__occupier__address" placeholder="Property Occupier Address" cols="30" rows="3"></textarea>
                        </div>

                        <div class="col-md-3">
                            <label for="">Property Occupier Email</label>
                            <input type="email" class="form-control" placeholder="Proprerty Occupier Email" name="property__occupier__email">
                        </div>

                        <div class="col-md-3">
                            <label for="">Category</label>
                            <select name="category__id" id="category__id" class="form-select">

                            </select>
                        </div>

                        <div class="col-md-3">
                            <label for="">Nature Of Property</label>
                            <select name="nature__of__property__id" id="nature__of__property__id" class="form-select">

                            </select>
                        </div>

                        <div class="col-md-3">
                            <label for="">Name Of Shop</label>
                            <input type="text" class="form-control" placeholder="Name Of Shop" name="name__of__shop">
                        </div>

                        <div class="col-md-3">
                            <label for="">Manufactured Sold Item</label>
                            <input type="text" class="form-control" placeholder="Manufactured Sold Item" name="manufactured___sold__item">
                        </div>

                        <div class="col-md-3">
                            <label for="">Installed On</label>

                            <select name="installed__on__id" id="installed__on" class="form-select">

                            </select>
                        </div>

                        <div class="col-md-3">
                            <label for="">Age Of Building</label>
                            <input type="text" class="form-control" placeholder="Age Of Building" name="age__of__building">
                        </div>

                        <div class="col-md-3">
                            <label for="">Existing Length</label>
                            <input type="number" class="form-control" placeholder="Existing Length" name="existing__length">
                        </div>

                        <div class="col-md-3">
                            <label for="">Existing Breadth</label>
                            <input type="number" class="form-control" placeholder="Existing Breadth" name="existing__breadth">
                        </div>

                        <div class="col-md-3">
                            <label for="">Existing Area</label>
                            <input type="number" class="form-control" placeholder="Exixting Area" name="existing__area">
                        </div>

                        <div class="col-md-3">
                            <label for="">Actual Length</label>
                            <input type="number" class="form-control" placeholder="Actual Length" name="actual__length">
                        </div>

                        <div class="col-md-3">
                            <label for="">Actual Breadth</label>
                            <input type="number" class="form-control" placeholder="Actual Breadth" name="actual__breadth">
                        </div>

                        <div class="col-md-3">
                            <label for="">Actual Area</label>
                            <input type="number" class="form-control" placeholder="Actual Area" name="actual__area">
                        </div>

                        <div class="col-md-3">
                            <label for="">One & Half Meter from Road</label>
                            <select name="one__half__meter__road__id" id="one__half__meter__road__id" class="form-select">

                            </select>
                        </div>

                        <div class="col-md-3">
                            <label for="">Board Nature</label>
                            <select name="board__nature__id" id="board__nature__id" class="form-select">

                            </select>
                        </div>

                        <div class="col-md-3">

                            <label for="">Difference In Sqft</label>
                            <input type="text" class="form-control" placeholder="Difference In Sqft" name="difference__in__sqft">
                        </div>

                        <div class="col-md-3">
                            <label for="">Additional Ratable Value</label>
                            <input type="text" class="form-control" placeholder="Additional Ratable Value" name="additional__ratable__value">
                        </div>

                        <div class="col-md-3">
                            <label for="">New Square Feet</label>
                            <input type="text" class="form-control" placeholder="New Square Feet" name="new__square__feet">
                        </div>

                        <div class="col-md-3">
                            <label for="">New Ratable Value in Square Feet </label>
                            <input type="text" class="form-control" placeholder="New Rateble Value In Square Feet" name="new__rateble__value__squre__feet">
                        </div>

                        <div class="col-md-12">
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-success">Save changes</button>
                        </div>

                    </div>

                </form>
            </div>

        </div>
    </div>
</div>

<!-- Modal (form3) -->
<div class="modal fade zoomIn" id="form_3_modal" tabindex="-1" aria-labelledby="form_2_modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="form_2_modalLabel">Edit Data (Form 3)</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form action="<?= site_url('update-survey-data') ?>" class="banner_survey_frm__3" method="POST" onsubmit="return false">
                    <div class="row">

                        <input type="hidden" name="banner__id">
                        <input type="hidden" name="form__number" value="3">

                        <div class="col-md-3">
                            <label for="">Notice Type</label>
                            <select name="notice__type__id" id="notice__type__id" class="form-select">

                            </select>
                        </div>


                        <div class="col-md-3">
                            <label for="">Remark</label>
                            <textarea class="form-control" id="remark" name="remark" placeholder="Remark" cols="30" rows="3"></textarea>
                        </div>

                        <!-- <div class="col-md-3">
                            <label for="">Remainder One Mode</label>
                            <select name="reminder__one__mode" id="reminder__one__mode" class="form-select">

                            </select>
                        </div>

                        <div class="col-md-3">

                            <label for="">Remainder One Mode Date</label>
                            <input type="text" class="form-control flatpickr-input" data-provider="flatpickr" data-date-format="Y-m-d" readonly="readonly" placeholder="Remnainder One Mode  Date" name="reminder__one__mode__date">
                        </div>

                        <div class="col-md-3">
                            <label for="">Remainder One Mode Time</label>
                            <input readonly="text" type="number" class="form-control flatpickr-input" data-provider="timepickr" data-time-basic="true" placeholder="Remnainder One Mode  Time" name="reminder__one__mode__time">
                        </div>

                        <div class="col-md-3">
                            <label for="">Remainder Two Mode</label>
                            <select name="reminder__two__mode" id="reminder__two__mode" class="form-select">

                            </select>
                        </div>

                        <div class="col-md-3">

                            <label for="">Remainder Two Mode Date</label>
                            <input type="text" class="form-control flatpickr-input" data-provider="flatpickr" data-date-format="Y-m-d" readonly="readonly" placeholder="Remnainder Two Mode  Date" name="reminder__two__mode__date">
                        </div>

                        <div class="col-md-3">
                            <label for="">Remainder Two Mode Time</label>
                            <input readonly="text" type="number" class="form-control flatpickr-input" data-provider="timepickr" data-time-basic="true" placeholder="Remnainder Two Mode  Time" name="reminder__two__mode__time">
                        </div>




                        <div class="col-md-3">
                            <label for="">Remainder Three Mode</label>
                            <select name="reminder__three__mode" id="reminder__three__mode" class="form-select">

                            </select>
                        </div>

                        <div class="col-md-3">

                            <label for="">Remainder Three Mode Date</label>
                            <input type="text" class="form-control flatpickr-input" data-provider="flatpickr" data-date-format="Y-m-d" readonly="readonly" placeholder="Remnainder Three Mode  Date" name="reminder__three__mode__date">
                        </div>

                        <div class="col-md-3">
                            <label for="">Remainder Three Mode Time</label>
                            <input readonly="text" type="number" class="form-control flatpickr-input" data-provider="timepickr" data-time-basic="true" placeholder="Remnainder Three Mode  Time" name="reminder__three__mode__time">
                        </div>

                        <div class="col-md-3">
                            <label for="">Remark</label>
                            <textarea class="form-control" id="remark" name="remark" placeholder="Remark" cols="30" rows="3"></textarea>
                        </div>

                        <div class="col-md-3">
                            <label for="">Number Of Advertisement Count</label>
                            <input type="text" class="form-control" placeholder="Number of advertisements" name="number__of__advertisement__count">
                        </div>

                        <div class="col-md-3">
                            <label for="">Number of Plate Count</label>
                            <input type="text" class="form-control" placeholder="Number of plate counts" name="number__of__plate__count">
                        </div>
 -->


                        <style>
                            .banner-img {
                                height: 120px;
                                width: 250px;
                                object-fit: cover;
                            }
                        </style>

                        <div class="col-md-3">
                            <label for="">Photo of actual notice 1</label>
                            <img src="" alt="" onerror="this.src='https://via.placeholder.com/150'" id="notice_image_1" class="form-control banner-img">
                        </div>

                        <div class="col-md-3">
                            <label for="">Receipt of actual notice 1</label>
                            <img src="" alt="" onerror="this.src='https://via.placeholder.com/150'" id="notice_image_2" class="form-control banner-img">
                        </div>

                        <div class="col-md-3">
                            <label for="">Photo of actual notice 2</label>
                            <img src="" alt="" onerror="this.src='https://via.placeholder.com/150'" id="notice_image_3" class="form-control banner-img">
                        </div>

                        <div class="col-md-3">
                            <label for="">Receipt of actual notice 2</label>
                            <img src="" alt="" onerror="this.src='https://via.placeholder.com/150'" id="notice_image_4" class="form-control banner-img">
                        </div>

                        <div class="col-md-3">
                            <label for="">Pasted photo</label>
                            <img src="" alt="" onerror="this.src='https://via.placeholder.com/150'" id="notice_image_5" class="form-control banner-img">
                        </div>




                        <div class="col-md-12">
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-success">Save changes</button>
                        </div>

                    </div>

                </form>
            </div>

        </div>
    </div>
</div>



<!-- Modal master data (edit) -->
<div class="modal fade zoomIn" id="masterEditModal" tabindex="-1" aria-labelledby="form_2_modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="masterEditModalLabel"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form action="<?= site_url('update-master-data') ?>" class="master-data-frm" method="POST" onsubmit="return false">
                    <div class="row master__data__div">
                    </div>

                    <div class="col-md-12">
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-success">Save changes</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>




<!-- Modal master data (add) -->
<div class="modal fade zoomIn" id="masterAddModal" tabindex="-1" aria-labelledby="masterAddModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="masterAddModalLabel"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form action="<?= site_url('store-master-data') ?>" class="master-data-add-frm" method="POST" onsubmit="return false">
                    <div class="row master__data__add__div">
                        <div class="col-md-11">
                            <div class="mb-1">
                                <label>Value</label>
                                <input type="text" name="value[]" class="form-control" placeholder="Enter Value">
                            </div>
                        </div>
                        <div class="col-md-1" style="margin-top:6%">
                            <div class="mb-1">
                                <a class="btn btn-sm btn-primary" onclick="addMore()"> + </a>
                            </div>
                        </div>

                    </div>

                    <div class="new-data">

                    </div>

                    <div class="col-md-12">
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-success">Save changes</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>



<!-- Modal user__edit__add -->
<div class="modal fade zoomIn" id="userAddEditModal" tabindex="-1" aria-labelledby="userAddEditModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="userAddEditModalLabel"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form action="<?= site_url('store-user-data') ?>" class="user-frm" method="POST" onsubmit="return false">
                    <div class="row">
                        <input type="hidden" name="user__id">


                        <div class="col-md-4">
                            <div class="mb-1">
                                <label for="">First Name</label>
                                <input placeholder="Enter first name" type="text" name="first__name" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="mb-1">
                                <label for="">Last Name</label>
                                <input placeholder="Enter last name" type="text" name="last__name" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="mb-1">
                                <label for="">Email</label>
                                <input type="email" name="email" class="form-control" placeholder="Enter Email">
                            </div>
                        </div>


                        <div class="col-md-4 password">
                            <div class="mb-1">
                                <label for="">Password</label>
                                <input type="password" name="password" class="form-control" placeholder="Enter password">
                            </div>
                        </div>



                        <div class="col-md-4">
                            <div class="mb-1">
                                <label for="">Mobile Number</label>
                                <input type="text" name="mobile_number" class="form-control" placeholder="Enter Mobile number">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="mb-1">
                                <label for="">User Type</label>
                                <select name="user__type" class="form-select user_type">
                                    <option value="">Select user Type</option>
                                    <?php
                                    if (isset($user_roles) && !empty($user_roles)) :
                                        foreach ($user_roles as $key => $value) :
                                            echo '<option value="' . $value['role_type'] . '">' . $value['user_role'] . '</option>';
                                        endforeach;
                                    endif;
                                    ?>

                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="mb-1">
                                <label for="">SQID</label>
                                <input type="text" name="sqid" class="form-control" placeholder="Enter SQID">
                            </div>
                        </div>


                        <div class="col-md-4">
                            <div class="mb-1">
                                <label for="">Address</label>
                                <textarea name="address" cols="30" rows="3" class="form-control" placeholder="Enter Address"></textarea>
                            </div>
                        </div>


                        <div class="col-md-4">
                            <div class="mb-1">
                                <label for="">State</label>
                                <select name="state" id="state" class="form-select">
                                    <option value="Maharashtra">Maharashra</option>

                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="mb-1">
                                <label for="">City</label>
                                <select name="city" id="city" class="form-select">
                                    <option value="Pune">Pune</option>
                                    <option value="Mumbai">Mumbai</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="mb-1">
                                <label for="">Pincode</label>
                                <input type="number" name="pincode" class="form-control">
                            </div>
                        </div>


                    </div>



                    <div class="col-md-12">
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-success">Save changes</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!-- Modal board__edit__add -->
<div class="modal fade zoomIn" id="boardAddEditModal" tabindex="-1" aria-labelledby="boardAddEditModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="boardAddEditModalLabel"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form action="<?= site_url('Signboard/save') ?>" class="board-frm" method="POST" onsubmit="return false">
                    <div class="row">
                        <input type="hidden" name="signboard_id">
                        <div class="col-md-3">
                            <div class="mb-1">
                                <label for="">Board Type</label>
                                <select name="media_type_id" class="form-select media_type">
                                    <option value="">Select Board Type</option>
                                    <?php
                                    if (isset($media_type) && !empty($media_type)) :
                                        foreach ($media_type as $key => $value) :
                                            echo '<option value="' . $value['media_type_id'] . '">' . $value['media_type_value'] . '</option>';
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-1">
                                <label for="">Ward Name</label>
                                <select name="ward_name_id" class="form-select ward_name">
                                    <option value="">Select Ward</option>
                                    <?php
                                    if (isset($ward_name) && !empty($ward_name)) :
                                        foreach ($ward_name as $key => $value) :
                                            echo '<option value="' . $value['ward_name_id'] . '">' . $value['ward_name'] . '</option>';
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-1">
                                <label for="">Proper Road Name</label>
                                <input placeholder="Enter Proper road name" type="text" name="proper_road_name" class="form-control alphabets_dots">
                                <span class="text-danger msg"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-1">
                                <label for="">Property ID</label>
                                <input placeholder="Enter Property ID" type="text" name="property_id" class="form-control alpha_numeric_class">
                                <span class="text-danger msg"></span>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="mb-1">
                                <label for="">Building ID</label>
                                <input type="text" name="building_id" class="form-control alpha_numeric_class" placeholder="Enter Building ID">
                                <span class="text-danger msg"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-1">
                                <label for="">No of Visible Board</label>
                                <input type="text" name="no_of_visible_board" class="form-control positive_numeric" placeholder="Enter No of Visible Board">
                                <span class="text-danger msg"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-1">
                                <label for="">Legalization Possibilities</label>
                                <select name="legalization_possibilities" id="legalization_possibilities" class="form-select">
                                    <option value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-1">
                                <label for="">Property Type</label>
                                <select name="nature_of_property_id" class="form-select nature_of_property">
                                    <option value="">Select Property Type</option>
                                    <?php
                                    if (isset($nature_of_property) && !empty($nature_of_property)) :
                                        foreach ($nature_of_property as $key => $value) :
                                            echo '<option value="' . $value['nature_of_property_id'] . '">' . $value['nature_of_property_value'] . '</option>';
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 other_property_type" style="display: none;">
                            <div class="mb-1">
                                <label for="">Other Property Type</label>
                                <input type="text" name="other_property_name" class="form-control other_property_name alphabets_dots" placeholder="Enter Other Property Type here">
                                <span class="text-danger msg"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-1">
                                <label for="">Name</label>
                                <input type="text" name="p_name" class="form-control alphabets_dots" placeholder="Enter Name here">
                                <span class="text-danger msg"></span>
                            </div>
                        </div>                        
                        <div class="col-md-3">
                            <div class="mb-1">
                                <label for="">Mobile Number</label>
                                <input type="text" name="mobile_number" class="form-control mobile_class" placeholder="Enter Mobile number">
                                <span class="text-danger msg"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-1">
                                <label for="">Address</label>
                                <textarea name="p_address" cols="30" rows="3" class="form-control" placeholder="Enter Address"></textarea>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-1">
                                <label for="">Photo No</label>
                                <input type="text" name="photo_no" class="form-control positive_numeric" placeholder="Enter Photo No">
                                <span class="text-danger msg"></span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="mb-1">
                                <label for="">Approx L1</label>
                               <input type="text" name="approx_l1" class="form-control numeric_class" placeholder="Enter Approx L1 in sq. meter">
                               <span class="text-danger msg"></span>
                            </div>
                        </div>                        
                        <div class="col-md-2">
                            <div class="mb-1">
                                <label for="">Approx W1</label>
                                <input type="text" name="approx_w1" class="form-control numeric_class" placeholder="Enter Approx W1 in sq. meter">
                                <span class="text-danger msg"></span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="mb-1">
                                <label for="">Approx L2</label>
                                <input type="text" name="approx_l2" class="form-control numeric_class" placeholder="Enter Approx L2 in sq. meter">
                                <span class="text-danger msg"></span>
                            </div>
                        </div>                        
                        <div class="col-md-2">
                            <div class="mb-1">
                                <label for="">Approx W2</label>
                                <input type="text" name="approx_w2" class="form-control numeric_class" placeholder="Enter Approx W2 in sq. meter">
                                <span class="text-danger msg"></span>
                            </div>
                        </div>                        
                        <div class="col-md-2">
                            <div class="mb-1">
                                <label for="">Approx L3</label>
                                <input type="text" name="approx_l3" class="form-control numeric_class" placeholder="Enter Approx L3 in sq. meter">
                                <span class="text-danger msg"></span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="mb-1">
                                <label for="">Approx W3</label>
                                <input type="text" name="approx_w3" class="form-control numeric_class" placeholder="Enter Approx W3 in sq. meter">
                                <span class="text-danger msg"></span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="mb-1">
                                <label for="">Approx L4</label>
                                <input type="text" name="approx_l4" class="form-control numeric_class" placeholder="Enter Approx L4 in sq. meter">
                                <span class="text-danger msg"></span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="mb-1">
                                <label for="">Approx W4</label>
                                <input type="text" name="approx_w4" class="form-control numeric_class" placeholder="Enter Approx W4 in sq meter">
                                <span class="text-danger msg"></span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="mb-1">
                                <label for="">Approx L5</label>
                                <input type="text" name="approx_l5" class="form-control numeric_class" placeholder="Enter Approx L5 in sq. meter">
                                <span class="text-danger msg"></span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="mb-1">
                                <label for="">Approx W5</label>
                                <input type="text" name="approx_w5" class="form-control numeric_class" placeholder="Enter Approx W5 in sq. meter">
                                <span class="text-danger msg"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-success">Save changes</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!-- Modal board__user__count__report -->
<div class="modal fade zoomIn" id="boardReportModal" tabindex="-1" aria-labelledby="boardReportModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="boardReportModalLabel"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form action="" class="board-report-frm" method="POST" onsubmit="search_data();return false;">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="mb-1">
                                <label for="">From Date</label>
                                <input type="date" name="from_date" id="from_date" value="<?php echo date('Y-m-d',strtotime('now'));?>">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="mb-1">
                                <label for="">To Date</label>
                                <input type="date" name="to_date" id="to_date" value="<?php echo date('Y-m-d',strtotime('now'));?>">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-success">Search</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>



<!-- marker info modal -->
<div class="modal fade zoomIn" id="MarkerFormOneModal" tabindex="-1" aria-labelledby="MarkerFormOneModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="MarkerFormOneModalLabel">Banner Detail</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered" style="user-select: auto;
    display: block;
    overflow-x: auto;
    white-space: nowrap">
                            <tbody class="marker_info_table"></tbody>
                        </table>
                    </div>
                </div>



            </div>

        </div>
    </div>
</div>








<div class="modal fade zoomIn" id="loaderModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="loaderModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content" style="background: transparent;
    border: none;">
            <div class="modal-body" style="text-align: center;">
                <img src="<?= site_url('public/ajax-loader.gif') ?>">
            </div>
        </div>
    </div>
</div>












<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <script>
                    document.write(new Date().getFullYear())
                </script> © Velzon.
            </div>
            <div class="col-sm-6">
                <div class="text-sm-end d-none d-sm-block">
                    Design & Develop by Saar IT Resources
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<!-- end main content-->

</div>
<!-- END layout-wrapper -->

<!--start back-to-top-->
<button onclick="topFunction()" class="btn btn-danger btn-icon" id="back-to-top">
    <i class="ri-arrow-up-line"></i>
</button>
<!--end back-to-top-->

<div class="customizer-setting d-none d-md-block">
    <div class="btn-info btn-rounded shadow-lg btn btn-icon btn-lg p-2" data-bs-toggle="offcanvas" data-bs-target="#theme-settings-offcanvas" aria-controls="theme-settings-offcanvas">
        <i class='mdi mdi-spin mdi-cog-outline fs-22'></i>
    </div>
</div>

<!-- Theme Settings -->
<!-- <div class="offcanvas offcanvas-end border-0" tabindex="-1" id="theme-settings-offcanvas" style="user-select: auto; visibility: hidden;" area-hidden="true">
    <div class="d-flex align-items-center bg-primary bg-gradient p-3 offcanvas-header">
        <h5 class="m-0 me-2 text-white">Theme Customizer</h5>

        <button type="button" class="btn-close btn-close-white ms-auto" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body p-0">
        <div data-simplebar class="h-100">
            <div class="p-4">
                <h6 class="mb-0 fw-bold text-uppercase">Layout</h6>
                <p class="text-muted">Choose your layout</p>

                <div class="row">
                    <div class="col-4">
                        <div class="form-check card-radio">
                            <input id="customizer-layout01" name="data-layout" type="radio" value="vertical" class="form-check-input">
                            <label class="form-check-label p-0 avatar-md w-100" for="customizer-layout01">
                                <span class="d-flex gap-1 h-100">
                                    <span class="flex-shrink-0">
                                        <span class="bg-light d-flex h-100 flex-column gap-1 p-1">
                                            <span class="d-block p-1 px-2 bg-soft-primary rounded mb-2"></span>
                                            <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                            <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                            <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                        </span>
                                    </span>
                                    <span class="flex-grow-1">
                                        <span class="d-flex h-100 flex-column">
                                            <span class="bg-light d-block p-1"></span>
                                            <span class="bg-light d-block p-1 mt-auto"></span>
                                        </span>
                                    </span>
                                </span>
                            </label>
                        </div>
                        <h5 class="fs-13 text-center mt-2">Vertical</h5>
                    </div>
                    <div class="col-4">
                        <div class="form-check card-radio">
                            <input id="customizer-layout02" name="data-layout" type="radio" value="horizontal" class="form-check-input">
                            <label class="form-check-label p-0 avatar-md w-100" for="customizer-layout02">
                                <span class="d-flex h-100 flex-column gap-1">
                                    <span class="bg-light d-flex p-1 gap-1 align-items-center">
                                        <span class="d-block p-1 bg-soft-primary rounded me-1"></span>
                                        <span class="d-block p-1 pb-0 px-2 bg-soft-primary ms-auto"></span>
                                        <span class="d-block p-1 pb-0 px-2 bg-soft-primary"></span>
                                    </span>
                                    <span class="bg-light d-block p-1"></span>
                                    <span class="bg-light d-block p-1 mt-auto"></span>
                                </span>
                            </label>
                        </div>
                        <h5 class="fs-13 text-center mt-2">Horizontal</h5>
                    </div>
                    <div class="col-4">
                        <div class="form-check card-radio">
                            <input id="customizer-layout03" name="data-layout" type="radio" value="twocolumn" class="form-check-input">
                            <label class="form-check-label p-0 avatar-md w-100" for="customizer-layout03">
                                <span class="d-flex gap-1 h-100">
                                    <span class="flex-shrink-0">
                                        <span class="bg-light d-flex h-100 flex-column gap-1">
                                            <span class="d-block p-1 bg-soft-primary mb-2"></span>
                                            <span class="d-block p-1 pb-0 bg-soft-primary"></span>
                                            <span class="d-block p-1 pb-0 bg-soft-primary"></span>
                                            <span class="d-block p-1 pb-0 bg-soft-primary"></span>
                                        </span>
                                    </span>
                                    <span class="flex-shrink-0">
                                        <span class="bg-light d-flex h-100 flex-column gap-1 p-1">
                                            <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                            <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                            <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                            <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                        </span>
                                    </span>
                                    <span class="flex-grow-1">
                                        <span class="d-flex h-100 flex-column">
                                            <span class="bg-light d-block p-1"></span>
                                            <span class="bg-light d-block p-1 mt-auto"></span>
                                        </span>
                                    </span>
                                </span>
                            </label>
                        </div>
                        <h5 class="fs-13 text-center mt-2">Two Column</h5>
                    </div>
                   
                </div>

                <h6 class="mt-4 mb-0 fw-bold text-uppercase">Color Scheme</h6>
                <p class="text-muted">Choose Light or Dark Scheme.</p>

                <div class="colorscheme-cardradio">
                    <div class="row">
                        <div class="col-4">
                            <div class="form-check card-radio">
                                <input class="form-check-input" type="radio" name="data-layout-mode" id="layout-mode-light" value="light">
                                <label class="form-check-label p-0 avatar-md w-100" for="layout-mode-light">
                                    <span class="d-flex gap-1 h-100">
                                        <span class="flex-shrink-0">
                                            <span class="bg-light d-flex h-100 flex-column gap-1 p-1">
                                                <span class="d-block p-1 px-2 bg-soft-primary rounded mb-2"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                            </span>
                                        </span>
                                        <span class="flex-grow-1">
                                            <span class="d-flex h-100 flex-column">
                                                <span class="bg-light d-block p-1"></span>
                                                <span class="bg-light d-block p-1 mt-auto"></span>
                                            </span>
                                        </span>
                                    </span>
                                </label>
                            </div>
                            <h5 class="fs-13 text-center mt-2">Light</h5>
                        </div>

                        <div class="col-4">
                            <div class="form-check card-radio dark">
                                <input class="form-check-input" type="radio" name="data-layout-mode" id="layout-mode-dark" value="dark">
                                <label class="form-check-label p-0 avatar-md w-100 bg-dark" for="layout-mode-dark">
                                    <span class="d-flex gap-1 h-100">
                                        <span class="flex-shrink-0">
                                            <span class="bg-soft-light d-flex h-100 flex-column gap-1 p-1">
                                                <span class="d-block p-1 px-2 bg-soft-light rounded mb-2"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-light"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-light"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-light"></span>
                                            </span>
                                        </span>
                                        <span class="flex-grow-1">
                                            <span class="d-flex h-100 flex-column">
                                                <span class="bg-soft-light d-block p-1"></span>
                                                <span class="bg-soft-light d-block p-1 mt-auto"></span>
                                            </span>
                                        </span>
                                    </span>
                                </label>
                            </div>
                            <h5 class="fs-13 text-center mt-2">Dark</h5>
                        </div>
                    </div>
                </div>

                <div id="layout-width">
                    <h6 class="mt-4 mb-0 fw-bold text-uppercase">Layout Width</h6>
                    <p class="text-muted">Choose Fluid or Boxed layout.</p>

                    <div class="row">
                        <div class="col-4">
                            <div class="form-check card-radio">
                                <input class="form-check-input" type="radio" name="data-layout-width" id="layout-width-fluid" value="fluid">
                                <label class="form-check-label p-0 avatar-md w-100" for="layout-width-fluid">
                                    <span class="d-flex gap-1 h-100">
                                        <span class="flex-shrink-0">
                                            <span class="bg-light d-flex h-100 flex-column gap-1 p-1">
                                                <span class="d-block p-1 px-2 bg-soft-primary rounded mb-2"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                            </span>
                                        </span>
                                        <span class="flex-grow-1">
                                            <span class="d-flex h-100 flex-column">
                                                <span class="bg-light d-block p-1"></span>
                                                <span class="bg-light d-block p-1 mt-auto"></span>
                                            </span>
                                        </span>
                                    </span>
                                </label>
                            </div>
                            <h5 class="fs-13 text-center mt-2">Fluid</h5>
                        </div>
                        <div class="col-4">
                            <div class="form-check card-radio">
                                <input class="form-check-input" type="radio" name="data-layout-width" id="layout-width-boxed" value="boxed">
                                <label class="form-check-label p-0 avatar-md w-100 px-2" for="layout-width-boxed">
                                    <span class="d-flex gap-1 h-100 border-start border-end">
                                        <span class="flex-shrink-0">
                                            <span class="bg-light d-flex h-100 flex-column gap-1 p-1">
                                                <span class="d-block p-1 px-2 bg-soft-primary rounded mb-2"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                            </span>
                                        </span>
                                        <span class="flex-grow-1">
                                            <span class="d-flex h-100 flex-column">
                                                <span class="bg-light d-block p-1"></span>
                                                <span class="bg-light d-block p-1 mt-auto"></span>
                                            </span>
                                        </span>
                                    </span>
                                </label>
                            </div>
                            <h5 class="fs-13 text-center mt-2">Boxed</h5>
                        </div>
                    </div>
                </div>

                <div id="layout-position">
                    <h6 class="mt-4 mb-0 fw-bold text-uppercase">Layout Position</h6>
                    <p class="text-muted">Choose Fixed or Scrollable Layout Position.</p>

                    <div class="btn-group radio" role="group">
                        <input type="radio" class="btn-check" name="data-layout-position" id="layout-position-fixed" value="fixed">
                        <label class="btn btn-light w-sm" for="layout-position-fixed">Fixed</label>

                        <input type="radio" class="btn-check" name="data-layout-position" id="layout-position-scrollable" value="scrollable">
                        <label class="btn btn-light w-sm ms-0" for="layout-position-scrollable">Scrollable</label>
                    </div>
                </div>
                <h6 class="mt-4 mb-0 fw-bold text-uppercase">Topbar Color</h6>
                <p class="text-muted">Choose Light or Dark Topbar Color.</p>

                <div class="row">
                    <div class="col-4">
                        <div class="form-check card-radio">
                            <input class="form-check-input" type="radio" name="data-topbar" id="topbar-color-light" value="light">
                            <label class="form-check-label p-0 avatar-md w-100" for="topbar-color-light">
                                <span class="d-flex gap-1 h-100">
                                    <span class="flex-shrink-0">
                                        <span class="bg-light d-flex h-100 flex-column gap-1 p-1">
                                            <span class="d-block p-1 px-2 bg-soft-primary rounded mb-2"></span>
                                            <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                            <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                            <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                        </span>
                                    </span>
                                    <span class="flex-grow-1">
                                        <span class="d-flex h-100 flex-column">
                                            <span class="bg-light d-block p-1"></span>
                                            <span class="bg-light d-block p-1 mt-auto"></span>
                                        </span>
                                    </span>
                                </span>
                            </label>
                        </div>
                        <h5 class="fs-13 text-center mt-2">Light</h5>
                    </div>
                    <div class="col-4">
                        <div class="form-check card-radio">
                            <input class="form-check-input" type="radio" name="data-topbar" id="topbar-color-dark" value="dark">
                            <label class="form-check-label p-0 avatar-md w-100" for="topbar-color-dark">
                                <span class="d-flex gap-1 h-100">
                                    <span class="flex-shrink-0">
                                        <span class="bg-light d-flex h-100 flex-column gap-1 p-1">
                                            <span class="d-block p-1 px-2 bg-soft-primary rounded mb-2"></span>
                                            <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                            <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                            <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                        </span>
                                    </span>
                                    <span class="flex-grow-1">
                                        <span class="d-flex h-100 flex-column">
                                            <span class="bg-primary d-block p-1"></span>
                                            <span class="bg-light d-block p-1 mt-auto"></span>
                                        </span>
                                    </span>
                                </span>
                            </label>
                        </div>
                        <h5 class="fs-13 text-center mt-2">Dark</h5>
                    </div>
                </div>

                <div id="sidebar-size">
                    <h6 class="mt-4 mb-0 fw-bold text-uppercase">Sidebar Size</h6>
                    <p class="text-muted">Choose a size of Sidebar.</p>

                    <div class="row">
                        <div class="col-4">
                            <div class="form-check sidebar-setting card-radio">
                                <input class="form-check-input" type="radio" name="data-sidebar-size" id="sidebar-size-default" value="lg">
                                <label class="form-check-label p-0 avatar-md w-100" for="sidebar-size-default">
                                    <span class="d-flex gap-1 h-100">
                                        <span class="flex-shrink-0">
                                            <span class="bg-light d-flex h-100 flex-column gap-1 p-1">
                                                <span class="d-block p-1 px-2 bg-soft-primary rounded mb-2"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                            </span>
                                        </span>
                                        <span class="flex-grow-1">
                                            <span class="d-flex h-100 flex-column">
                                                <span class="bg-light d-block p-1"></span>
                                                <span class="bg-light d-block p-1 mt-auto"></span>
                                            </span>
                                        </span>
                                    </span>
                                </label>
                            </div>
                            <h5 class="fs-13 text-center mt-2">Default</h5>
                        </div>

                        <div class="col-4">
                            <div class="form-check sidebar-setting card-radio">
                                <input class="form-check-input" type="radio" name="data-sidebar-size" id="sidebar-size-compact" value="md">
                                <label class="form-check-label p-0 avatar-md w-100" for="sidebar-size-compact">
                                    <span class="d-flex gap-1 h-100">
                                        <span class="flex-shrink-0">
                                            <span class="bg-light d-flex h-100 flex-column gap-1 p-1">
                                                <span class="d-block p-1 bg-soft-primary rounded mb-2"></span>
                                                <span class="d-block p-1 pb-0 bg-soft-primary"></span>
                                                <span class="d-block p-1 pb-0 bg-soft-primary"></span>
                                                <span class="d-block p-1 pb-0 bg-soft-primary"></span>
                                            </span>
                                        </span>
                                        <span class="flex-grow-1">
                                            <span class="d-flex h-100 flex-column">
                                                <span class="bg-light d-block p-1"></span>
                                                <span class="bg-light d-block p-1 mt-auto"></span>
                                            </span>
                                        </span>
                                    </span>
                                </label>
                            </div>
                            <h5 class="fs-13 text-center mt-2">Compact</h5>
                        </div>

                        <div class="col-4">
                            <div class="form-check sidebar-setting card-radio">
                                <input class="form-check-input" type="radio" name="data-sidebar-size" id="sidebar-size-small" value="sm">
                                <label class="form-check-label p-0 avatar-md w-100" for="sidebar-size-small">
                                    <span class="d-flex gap-1 h-100">
                                        <span class="flex-shrink-0">
                                            <span class="bg-light d-flex h-100 flex-column gap-1">
                                                <span class="d-block p-1 bg-soft-primary mb-2"></span>
                                                <span class="d-block p-1 pb-0 bg-soft-primary"></span>
                                                <span class="d-block p-1 pb-0 bg-soft-primary"></span>
                                                <span class="d-block p-1 pb-0 bg-soft-primary"></span>
                                            </span>
                                        </span>
                                        <span class="flex-grow-1">
                                            <span class="d-flex h-100 flex-column">
                                                <span class="bg-light d-block p-1"></span>
                                                <span class="bg-light d-block p-1 mt-auto"></span>
                                            </span>
                                        </span>
                                    </span>
                                </label>
                            </div>
                            <h5 class="fs-13 text-center mt-2">Small (Icon View)</h5>
                        </div>

                        <div class="col-4">
                            <div class="form-check sidebar-setting card-radio">
                                <input class="form-check-input" type="radio" name="data-sidebar-size" id="sidebar-size-small-hover" value="sm-hover">
                                <label class="form-check-label p-0 avatar-md w-100" for="sidebar-size-small-hover">
                                    <span class="d-flex gap-1 h-100">
                                        <span class="flex-shrink-0">
                                            <span class="bg-light d-flex h-100 flex-column gap-1">
                                                <span class="d-block p-1 bg-soft-primary mb-2"></span>
                                                <span class="d-block p-1 pb-0 bg-soft-primary"></span>
                                                <span class="d-block p-1 pb-0 bg-soft-primary"></span>
                                                <span class="d-block p-1 pb-0 bg-soft-primary"></span>
                                            </span>
                                        </span>
                                        <span class="flex-grow-1">
                                            <span class="d-flex h-100 flex-column">
                                                <span class="bg-light d-block p-1"></span>
                                                <span class="bg-light d-block p-1 mt-auto"></span>
                                            </span>
                                        </span>
                                    </span>
                                </label>
                            </div>
                            <h5 class="fs-13 text-center mt-2">Small Hover View</h5>
                        </div>
                    </div>
                </div>

                <div id="sidebar-view">
                    <h6 class="mt-4 mb-0 fw-bold text-uppercase">Sidebar View</h6>
                    <p class="text-muted">Choose Default or Detached Sidebar view.</p>

                    <div class="row">
                        <div class="col-4">
                            <div class="form-check sidebar-setting card-radio">
                                <input class="form-check-input" type="radio" name="data-layout-style" id="sidebar-view-default" value="default">
                                <label class="form-check-label p-0 avatar-md w-100" for="sidebar-view-default">
                                    <span class="d-flex gap-1 h-100">
                                        <span class="flex-shrink-0">
                                            <span class="bg-light d-flex h-100 flex-column gap-1 p-1">
                                                <span class="d-block p-1 px-2 bg-soft-primary rounded mb-2"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                            </span>
                                        </span>
                                        <span class="flex-grow-1">
                                            <span class="d-flex h-100 flex-column">
                                                <span class="bg-light d-block p-1"></span>
                                                <span class="bg-light d-block p-1 mt-auto"></span>
                                            </span>
                                        </span>
                                    </span>
                                </label>
                            </div>
                            <h5 class="fs-13 text-center mt-2">Default</h5>
                        </div>
                        <div class="col-4">
                            <div class="form-check sidebar-setting card-radio">
                                <input class="form-check-input" type="radio" name="data-layout-style" id="sidebar-view-detached" value="detached">
                                <label class="form-check-label p-0 avatar-md w-100" for="sidebar-view-detached">
                                    <span class="d-flex h-100 flex-column">
                                        <span class="bg-light d-flex p-1 gap-1 align-items-center px-2">
                                            <span class="d-block p-1 bg-soft-primary rounded me-1"></span>
                                            <span class="d-block p-1 pb-0 px-2 bg-soft-primary ms-auto"></span>
                                            <span class="d-block p-1 pb-0 px-2 bg-soft-primary"></span>
                                        </span>
                                        <span class="d-flex gap-1 h-100 p-1 px-2">
                                            <span class="flex-shrink-0">
                                                <span class="bg-light d-flex h-100 flex-column gap-1 p-1">
                                                    <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                                    <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                                    <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                                </span>
                                            </span>
                                        </span>
                                        <span class="bg-light d-block p-1 mt-auto px-2"></span>
                                    </span>
                                </label>
                            </div>
                            <h5 class="fs-13 text-center mt-2">Detached</h5>
                        </div>
                    </div>
                </div>
                <div id="sidebar-color">
                    <h6 class="mt-4 mb-0 fw-bold text-uppercase">Sidebar Color</h6>
                    <p class="text-muted">Choose a color of Sidebar.</p>

                    <div class="row">
                        <div class="col-4">
                            <div class="form-check sidebar-setting card-radio" data-bs-toggle="collapse" data-bs-target="#collapseBgGradient.show">
                                <input class="form-check-input" type="radio" name="data-sidebar" id="sidebar-color-light" value="light">
                                <label class="form-check-label p-0 avatar-md w-100" for="sidebar-color-light">
                                    <span class="d-flex gap-1 h-100">
                                        <span class="flex-shrink-0">
                                            <span class="bg-white border-end d-flex h-100 flex-column gap-1 p-1">
                                                <span class="d-block p-1 px-2 bg-soft-primary rounded mb-2"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-primary"></span>
                                            </span>
                                        </span>
                                        <span class="flex-grow-1">
                                            <span class="d-flex h-100 flex-column">
                                                <span class="bg-light d-block p-1"></span>
                                                <span class="bg-light d-block p-1 mt-auto"></span>
                                            </span>
                                        </span>
                                    </span>
                                </label>
                            </div>
                            <h5 class="fs-13 text-center mt-2">Light</h5>
                        </div>
                        <div class="col-4">
                            <div class="form-check sidebar-setting card-radio" data-bs-toggle="collapse" data-bs-target="#collapseBgGradient.show">
                                <input class="form-check-input" type="radio" name="data-sidebar" id="sidebar-color-dark" value="dark">
                                <label class="form-check-label p-0 avatar-md w-100" for="sidebar-color-dark">
                                    <span class="d-flex gap-1 h-100">
                                        <span class="flex-shrink-0">
                                            <span class="bg-primary d-flex h-100 flex-column gap-1 p-1">
                                                <span class="d-block p-1 px-2 bg-soft-light rounded mb-2"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-light"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-light"></span>
                                                <span class="d-block p-1 px-2 pb-0 bg-soft-light"></span>
                                            </span>
                                        </span>
                                        <span class="flex-grow-1">
                                            <span class="d-flex h-100 flex-column">
                                                <span class="bg-light d-block p-1"></span>
                                                <span class="bg-light d-block p-1 mt-auto"></span>
                                            </span>
                                        </span>
                                    </span>
                                </label>
                            </div>
                            <h5 class="fs-13 text-center mt-2">Dark</h5>
                        </div>
                        <div class="col-4">
                            <button class="btn btn-link avatar-md w-100 p-0 overflow-hidden border collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseBgGradient" aria-expanded="false" aria-controls="collapseBgGradient">
                                <span class="d-flex gap-1 h-100">
                                    <span class="flex-shrink-0">
                                        <span class="bg-vertical-gradient d-flex h-100 flex-column gap-1 p-1">
                                            <span class="d-block p-1 px-2 bg-soft-light rounded mb-2"></span>
                                            <span class="d-block p-1 px-2 pb-0 bg-soft-light"></span>
                                            <span class="d-block p-1 px-2 pb-0 bg-soft-light"></span>
                                            <span class="d-block p-1 px-2 pb-0 bg-soft-light"></span>
                                        </span>
                                    </span>
                                    <span class="flex-grow-1">
                                        <span class="d-flex h-100 flex-column">
                                            <span class="bg-light d-block p-1"></span>
                                            <span class="bg-light d-block p-1 mt-auto"></span>
                                        </span>
                                    </span>
                                </span>
                            </button>
                            <h5 class="fs-13 text-center mt-2">Gradient</h5>
                        </div>
                    </div>
                   

                    <div class="collapse" id="collapseBgGradient">
                        <div class="d-flex gap-2 flex-wrap img-switch p-2 px-3 bg-light rounded">

                            <div class="form-check sidebar-setting card-radio">
                                <input class="form-check-input" type="radio" name="data-sidebar" id="sidebar-color-gradient" value="gradient">
                                <label class="form-check-label p-0 avatar-xs rounded-circle" for="sidebar-color-gradient">
                                    <span class="avatar-title rounded-circle bg-vertical-gradient"></span>
                                </label>
                            </div>
                            <div class="form-check sidebar-setting card-radio">
                                <input class="form-check-input" type="radio" name="data-sidebar" id="sidebar-color-gradient-2" value="gradient-2">
                                <label class="form-check-label p-0 avatar-xs rounded-circle" for="sidebar-color-gradient-2">
                                    <span class="avatar-title rounded-circle bg-vertical-gradient-2"></span>
                                </label>
                            </div>
                            <div class="form-check sidebar-setting card-radio">
                                <input class="form-check-input" type="radio" name="data-sidebar" id="sidebar-color-gradient-3" value="gradient-3">
                                <label class="form-check-label p-0 avatar-xs rounded-circle" for="sidebar-color-gradient-3">
                                    <span class="avatar-title rounded-circle bg-vertical-gradient-3"></span>
                                </label>
                            </div>
                            <div class="form-check sidebar-setting card-radio">
                                <input class="form-check-input" type="radio" name="data-sidebar" id="sidebar-color-gradient-4" value="gradient-4">
                                <label class="form-check-label p-0 avatar-xs rounded-circle" for="sidebar-color-gradient-4">
                                    <span class="avatar-title rounded-circle bg-vertical-gradient-4"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="sidebar-img">
                    <h6 class="mt-4 mb-0 fw-bold text-uppercase">Sidebar Images</h6>
                    <p class="text-muted">Choose a image of Sidebar.</p>

                    <div class="d-flex gap-2 flex-wrap img-switch">
                        <div class="form-check sidebar-setting card-radio">
                            <input class="form-check-input" type="radio" name="data-sidebar-image" id="sidebarimg-none" value="none">
                            <label class="form-check-label p-0 avatar-sm h-auto" for="sidebarimg-none">
                                <span class="avatar-md w-auto bg-light d-flex align-items-center justify-content-center">
                                    <i class="ri-close-fill fs-20"></i>
                                </span>
                            </label>
                        </div>

                        <div class="form-check sidebar-setting card-radio">
                            <input class="form-check-input" type="radio" name="data-sidebar-image" id="sidebarimg-01" value="img-1">
                            <label class="form-check-label p-0 avatar-sm h-auto" for="sidebarimg-01">
                                <img src="<?= site_url() ?>public/assets/images/sidebar/img-1.jpg" class="avatar-md w-auto object-cover">
                            </label>
                        </div>

                        <div class="form-check sidebar-setting card-radio">
                            <input class="form-check-input" type="radio" name="data-sidebar-image" id="sidebarimg-02" value="img-2">
                            <label class="form-check-label p-0 avatar-sm h-auto" for="sidebarimg-02">
                                <img src="<?= site_url() ?>public/assets/images/sidebar/img-2.jpg" class="avatar-md w-auto object-cover">
                            </label>
                        </div>
                        <div class="form-check sidebar-setting card-radio">
                            <input class="form-check-input" type="radio" name="data-sidebar-image" id="sidebarimg-03" value="img-3">
                            <label class="form-check-label p-0 avatar-sm h-auto" for="sidebarimg-03">
                                <img src="<?= site_url() ?>public/assets/images/sidebar/img-3.jpg" class="avatar-md w-auto object-cover">
                            </label>
                        </div>
                        <div class="form-check sidebar-setting card-radio">
                            <input class="form-check-input" type="radio" name="data-sidebar-image" id="sidebarimg-04" value="img-4">
                            <label class="form-check-label p-0 avatar-sm h-auto" for="sidebarimg-04">
                                <img src="<?= site_url() ?>public/assets/images/sidebar/img-4.jpg" class="avatar-md w-auto object-cover">
                            </label>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <div class="offcanvas-footer border-top p-3 text-center">
        <div class="row">
            <div class="col-6">
                <button type="button" class="btn btn-light w-100" id="reset-layout">Reset</button>
            </div>
            
        </div>
    </div>

</div> -->
<!-- <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script> -->
<!-- JAVASCRIPT -->
<script src="<?= site_url() ?>public/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?= site_url() ?>public/assets/libs/simplebar/simplebar.min.js"></script>
<script src="<?= site_url() ?>public/assets/libs/node-waves/waves.min.js"></script>
<script src="<?= site_url() ?>public/assets/libs/feather-icons/feather.min.js"></script>
<script src="<?= site_url() ?>public/assets/js/pages/plugins/lord-icon-2.1.0.js"></script>
<script src="<?= site_url() ?>public/assets/js/plugins.js"></script>
<!-- apexcharts -->
<script src="<?= site_url() ?>public/assets/libs/apexcharts/apexcharts.min.js"></script>
<!-- Dashboard init -->
<script src="<?= site_url() ?>public/assets/js/pages/dashboard-crm.init.js"></script>
<!--datatable js-->

<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="<?= site_url() ?>public/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<!--datatable js-->
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5Y5rIwFn8d_H5dvGlZJR1BWYKLUahlf0"></script>
<!-- notifications init -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js" integrity="sha512-zlWWyZq71UMApAjih4WkaRpikgY9Bz1oXIW5G0fED4vk14JjGlQ1UmkGM392jEULP8jbNMiwLWdM8Z87Hu88Fw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- App js -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/jquery.validate.min.js" integrity="sha512-FOhq9HThdn7ltbK8abmGn60A/EMtEzIzv1rvuh+DqzJtSGq8BRdEN0U+j0iKEIffiw/yEtVuladk6rsG4X6Uqg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<!-- echarts js -->
<script src="<?= site_url() ?>public/assets/libs/echarts/echarts.min.js"></script>

<!-- echarts init -->
<script src="<?= site_url() ?>public/assets/js/pages/echarts.init.js"></script>

<!-- init js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.13/flatpickr.min.js" integrity="sha512-K/oyQtMXpxI4+K0W7H25UopjM8pzq0yrVdFdG21Fh5dBe91I40pDd9A4lzNlHPHBIP2cwZuoxaUSX0GJSObvGA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<!--select2 cdn-->
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script src="<?= site_url() ?>public/assets/js/pages/datatables.init.js"></script>
<script src="<?= site_url() ?>public/assets/js/app.js"></script>
<script>
    const userType = '<?= isset($_SESSION['USER']['user_type']) ?  $_SESSION['USER']['user_type'] : 0 ?>';
    const site_url = "<?= site_url() ?>";

    // alert(site_url)
</script>
<script src="https://cdn.datatables.net/fixedcolumns/4.1.0/js/dataTables.fixedColumns.min.js"></script>
<script src="<?= site_url() ?>public/custom.js"></script>
</body>

</html>