<?php
class WardSummaryController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('SurveyModel');
        $this->load->model('CommonModel');
        $this->load->model('WardSummaryModel');
        $this->load->library('CustomLibrary');
        $this->user_id = isset($_SESSION['USER']['uid']) ?  $_SESSION['USER']['uid'] : 0;
        $this->user_type = isset($_SESSION['USER']['user_type']) ?  $_SESSION['USER']['user_type'] : 0;
        // echo $this->user_id;die;
        if ($this->user_id  ==  0)
            redirect('AuthController');
    }
    public function index()
    {
        $page_data['ward_data_arr'] = $this->CommonModel->getMultipleData('wards', NULL, ['ward_name_id', 'ward_name']);
        $page_data['media_type_arr'] = $this->CommonModel->getMultipleData('media_type',NULL,['media_type_id','media_type_value']);
        $this->load->view('includes/header');
        $this->load->view('ward/ward_summary', $page_data);
        $this->load->view('includes/footer');
    }
    //downloadWardSummaryCSV
    public function downloadWardSummaryCSV()
    {
        $res =  $this->wardSummaryData();
        $file_name = date('YmdHis') . '_ward_summaries.csv';
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$file_name");
        header("Content-Type: application/csv; ");
        // file creation
        $fileName = fopen('php://output', 'w');
        $hederArr = [
            'Ward Name',
            'Surveyed Count',
            'Illegal',
            'Signing In Progress',
            'Total Notice Signed',
            'Notice Delivered',
            'Notice To Be Delivered',
            'Approched For Sanction',
            'Ready For Action',
            'Legalized',
            'Demolished',
            'Waiting For Response'
        ];
        fputcsv($fileName, $hederArr);
        if (isset($res['summary']) && !empty($res['summary'])) {
            // $total_donation_amnt = 0;
            foreach ($res['summary'] as $key => $value) {
                $customArray = [
                    'Ward Name' => $value['ward_name'],
                    'Surveyed Count' => $value['surveyed_count'],
                    'Illegal' => $value['illegal_count'],
                    'Signing In Progress' => $value['signing_progress'],
                    'Total Notice Signed' => $value['notice_signed'],
                    'Notice Delivered' => $value['notice_delivered'],
                    'Notice To Be Delivered' => $value['notice_to_be_delivered'],
                    'Approched For Sanction' => $value['approched_sanction'],
                    'Ready For Action' => $value['ready_for_action'],
                    'Legalized' => $value['legalized'],
                    'Demolished' => $value['demolished'],
                    'Waiting For Response' => $value['waiting_for_response']
                ];
                fputcsv($fileName, $customArray);
            }
            $customData = [
                'Ward Name' => 'Total',
                'Surveyed Count' => $res['total'][0],
                'Illegal' => $res['total'][1],
                'Signing In Progress' => $res['total'][2],
                'Total Notice Signed' => $res['total'][3],
                'Notice Delivered' => $res['total'][4],
                'Notice To Be Delivered' => $res['total'][5],
                'Approched For Sanction' => $res['total'][6],
                'Ready For Action' => $res['total'][7],
                'Legalized' => $res['total'][8],
                'Demolished' => $res['total'][9],
                'Waiting For Response' => $res['total'][10],
            ];
            fputcsv($fileName, $customData);
            $customData = [
                'Ward Name' => 'Aprox One Year Amount',
                'Surveyed Count' => $res['amount'][0],
                'Illegal' => $res['amount'][1],
                'Signing In Progress' => $res['amount'][2],
                'Total Notice Signed' => $res['amount'][3],
                'Notice Delivered' => $res['amount'][4],
                'Notice To Be Delivered' => $res['amount'][5],
                'Approched For Sanction' => $res['amount'][6],
                'Ready For Action' => $res['amount'][7],
                'Legalized' => $res['amount'][8],
                'Demolished' => $res['amount'][9],
                'Waiting For Response' => $res['amount'][10],
            ];
            fputcsv($fileName, $customData);
        }
        fclose($fileName);
        exit;
    }
    //wardSummaryData
    public function wardSummaries()
    {
        $dJson = json_decode(file_get_contents('php://input'), true);
        $date = isset($dJson['date_range']) ? $dJson['date_range'] : NULL;
        $media_type_id =  isset($dJson['media_type_id']) ? $dJson['media_type_id'] : NULL;
        $start_date = NULL;
        $end_date = NULL;
        if ($date != NULL) {
            $dateArr = explode('to', $date);
            $start_date = $dateArr[0];
            $end_date = $dateArr[1];
        }
        $res =  $this->wardSummaryData($start_date, $end_date,$media_type_id);
        http_response_code(200);
        echo json_encode(['status' => http_response_code(), 'msg' => 'Success', 'data' => $res]);
        die;
        // echo '<pre>';
        // print_r($res);
        // echo '---------------------------------------';
        // print_r($totalArr);
        // echo "-------------------------------------";
        // print_r($approxAmountArr);
        // die;
    }
    private function wardSummaryData($start_date = NULL, $end_date = NULL,$media_type_id=NULL)
    {
        $wards = $this->CommonModel->getMultipleData('wards', '', '');
        $totalArr = [];
        $approxAmountArr = [];
        $total_survey_count = 0;
        $total_illegal = 0;
        $total_signing_progress = 0;
        $total_notice_signed = 0;
        $total_notice_delivered = 0;
        $total_notice_to_be_delivered = 0;
        $total_sanction = 0;
        $total_ready_for_action = 0;
        $total_legalized = 0;
        $total__notice_sign =0;
        $total_demolished = 0;
        $total_waiting_for_response = 0;
        foreach ($wards as $key => $value) {
            $surveyed_count = $this->WardSummaryModel->getWardSurvedCount($value['ward_name_id'], $start_date, $end_date,$media_type_id);
            $illegal = $this->WardSummaryModel->getWardBannerStatusCount($value['ward_name_id'], 'Illegal', $start_date, $end_date,$media_type_id);
            // echo $illegal;
            // die;
            // echo "<br>";
            $signing_progress = $this->WardSummaryModel->getWardBannerStatusCount($value['ward_name_id'], 'Ready For Sign', $start_date, $end_date,$media_type_id);
            // echo $signing_progress;
            $notice_signed = $this->WardSummaryModel->getWardBannerStatusCount($value['ward_name_id'], 'Notice Signed', $start_date, $end_date,$media_type_id);
            // echo "signing_progress= ".$signing_progress;
            // echo "<br>";
            // echo "notice_signed= ".$notice_signed;
            // echo "<br>";
            // echo 'illegal=='.$illegal;
            // echo '<br>';
            // echo "ward__id==".$value['ward_name_id'];
            // echo '<br>';
            // echo "---------------";
            // echo '<br>';
            $waiting_for_response = $this->WardSummaryModel->getWardBannerStatusCount($value['ward_name_id'], 'Notice delivered', $start_date, $end_date,$media_type_id);
            // $notice_delivered = $this->WardSummaryModel->getWardBannerStatusCount($value['ward_name_id'], 'Notice delivered', $start_date, $end_date,$media_type_id);
            $approched_sanction = $this->WardSummaryModel->getWardBannerStatusCount($value['ward_name_id'], 'Approached for sanction', $start_date, $end_date,$media_type_id);
            $ready_for_action = $this->WardSummaryModel->getWardBannerStatusCount($value['ward_name_id'], 'Ready For Action', $start_date, $end_date,$media_type_id);
            $legalized = $this->WardSummaryModel->getWardBannerStatusCount($value['ward_name_id'], 'Legalized', $start_date, $end_date,$media_type_id);
            $demolished = $this->WardSummaryModel->getWardBannerStatusCount($value['ward_name_id'], 'Demolished', $start_date, $end_date,$media_type_id);
           
            $notice_delivered = $approched_sanction + $ready_for_action + $waiting_for_response + $demolished + $legalized;
           
           
            // $hoardingIllegal = $this->WardSummaryModel->getWardBannerStatusCount($value['ward_name_id'], 'Illegal', $start_date,$end_date);
            // echo $hoardingIllegal;die;
            // $notice_to_be_delivered = (int)$notice_signed - (int)$notice_delivered;
            $notice_to_be_delivered = (int)$notice_signed;
            // $waiting_for_response = $notice_delivered;
            $total_notice_signed =  $notice_delivered + $notice_to_be_delivered;
            $illegalCount =  $signing_progress + $total_notice_signed + $illegal;
            // echo $illegalCount;
            // die;
            // $waiting_for_response = $notice_delivered - ($notice_to_be_delivered + $approched_sanction  + $ready_for_action + $legalized + $demolished);
            $total_survey_count = $total_survey_count + $surveyed_count;
            $total_illegal = $total_illegal + $illegalCount;
            $total_signing_progress = $total_signing_progress + $signing_progress;
            // $total_notice_signed = $total_notice_signed + $notice_signed;
            $total__notice_sign = $total__notice_sign + $total_notice_signed;
            $total_notice_delivered = $total_notice_delivered + $notice_delivered;
            $total_notice_to_be_delivered = $total_notice_to_be_delivered + $notice_to_be_delivered;
            // $total_notice_signed = $total_notice_signed + $notice_signed;

           
            


            $total_sanction = $total_sanction + $approched_sanction;
            $total_ready_for_action = $total_ready_for_action + $ready_for_action;
            $total_legalized = $total_legalized + $legalized;
            $total_demolished = $total_demolished + $demolished;
            $total_waiting_for_response =  $total_waiting_for_response + $waiting_for_response;
            // die;
            $data[] = [
                'ward_name' => $value['ward_name'],
                'surveyed_count' => $surveyed_count,
                'illegal_count' => $illegalCount,
                'signing_progress' => $signing_progress,
                'notice_signed' => $total_notice_signed,
                'notice_delivered' => $notice_delivered,
                'notice_to_be_delivered' => $notice_to_be_delivered,
                'approched_sanction' => $approched_sanction,
                'ready_for_action' => $ready_for_action,
                'legalized' => $legalized,
                'demolished' => $demolished,
                'waiting_for_response' => $waiting_for_response
            ];
        } //end foreach
        // echo $total_notice_delivered;die;
        array_push($totalArr, $total_survey_count);
        array_push($totalArr, $total_illegal);
        array_push($totalArr, $total_signing_progress);
        array_push($totalArr, $total__notice_sign);
        array_push($totalArr, $total_notice_delivered);
        array_push($totalArr, $total_notice_to_be_delivered);
        array_push($totalArr, $total_sanction);
        array_push($totalArr, $total_ready_for_action);
        array_push($totalArr, $total_legalized);
        array_push($totalArr, $total_demolished);
        array_push($totalArr, $total_waiting_for_response);
        array_push($approxAmountArr, '');
        array_push($approxAmountArr, '');
        array_push($approxAmountArr, $total_signing_progress * 222);
        array_push($approxAmountArr, $total__notice_sign * 222);
        array_push($approxAmountArr, $total_notice_delivered * 222);
        array_push($approxAmountArr, $total_notice_to_be_delivered * 222);
        array_push($approxAmountArr, $total_sanction * 222);
        array_push($approxAmountArr, $total_ready_for_action * 222);
        array_push($approxAmountArr, $total_legalized * 222);
        array_push($approxAmountArr, $total_demolished * 222);
        array_push($approxAmountArr, $total_waiting_for_response * 222);
        return $res = [
            'summary' => $data,
            'amount' => $approxAmountArr,
            'total' => $totalArr
        ];
    }
}
