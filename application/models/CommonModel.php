<?php
class CommonModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSingleData($table,$where,$select=NULL)
    {
        if($select!=NULL)
            $this->db->select($select);
        $this->db->from($table);
        $this->db->where($where);
        return $this->db->get()->row_array();
        // echo '<pre>';
        // print_r($data);die;
        
    }


    public function getMultipleData($table, $where=NULL, $select = NULL)
    {
        if ($select != NULL)
            $this->db->select($select);
        $this->db->from($table);
        if($where!=NULL)
        $this->db->where($where);
        return $this->db->get()->result_array();
        // echo '<pre>';
        // print_r($data);die;

    }

    // /updateData
    public function updateData($tableName, $condition, $updateArr)
    {
        $this->db->trans_start();
        $this->db->where($condition)
            ->update($tableName, $updateArr) ? true : false;
        $this->db->trans_complete();
        $trans_status = $this->db->trans_status();

        if ($trans_status == FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    //insertData
    public function insertData($table,$data)
    {
        $this->db->trans_start();
        $this->db->insert($table,$data) ? true : false;
        $this->db->trans_complete();
        $trans_status = $this->db->trans_status();
        if ($trans_status == FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }



    //deleteData
    public function deleteData($table, $cond)
    {
        $this->db->trans_start();
        $this->db->delete($table,$cond) ? true : false;
        $this->db->trans_complete();
        $trans_status = $this->db->trans_status();
        if ($trans_status == FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }



    //getLastId
    public function getLastId($tableName,$column)
    {
        return $this->db->select($column)
        ->from($tableName)
        ->order_by($column,'DESC')
        ->get()->row_array();
    }


    



}
