/* local server */
// const site_url = "http://172.17.47.26/project/banner_survey_v1";
// const pathArray = window.location.pathname.split("/");
// const segment = pathArray[3];
// const id = pathArray[4];
/* live server */
// const site_url = "http://103.233.79.142:90/banner_survey_web";
// const pathArray = window.location.pathname.split("/");
// const segment = pathArray[2];
// const id = pathArray[3];
//local server
// const site_url = "http://localhost/banner_survey_v1";
// const site_url = "http://172.17.46.78/banner_survey_v1";

const pathArray = window.location.pathname.split("/");
// console.log(pathArray[3]);
// const segment = pathArray[3];
const segment = pathArray[2];
const id = pathArray[3];
let surveyTableType = "allData";
// console.log(segment);



//filterMapData
const filterMapData = async (argV) =>{
	try {
		$("#loaderModal").modal("show", { backdrop: "static", keyboard: false });
		const media_id = $('.media_id').val();
		const banner_id = $('.banner_id').val();
		
		const type = $(argV).attr('data-type');
		const  value = argV.value;

		
		const rawResponse = await fetch(site_url+'filter-map-data',{
			method:"POST",
			body:JSON.stringify({media_id,banner_id})
		});
		const obj = await rawResponse.json();
		const status =await obj.status;
		const msg = await obj.msg
		const data=  await obj.data;
		if(status == 200){
			$("#loaderModal").modal("hide", { backdrop: "static", keyboard: false });







	let marker, i, myIcon;

	let locations = data;
	let map = new google.maps.Map(document.getElementById("map"), {
		zoom: 10,
		center: new google.maps.LatLng(18.5204, 73.8567),
		// mapTypeId:google.maps.MapTypeId.TERRAIN,
		mapTypeId: google.maps.MapTypeId.HYBRID,
		// mapTypeId: google.maps.MapTypeId.ROADMAP,
	});
	let infowindow = new google.maps.InfoWindow();
	// var myIcon = site_url + "/public/banner-real.png";
	for (i = 0; i < locations.length; i++) {
		let banner_status = locations[i][3];
		switch (banner_status) {
			case "Approached for sanction":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Court Case":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Demolished":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Existing Id Found":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Illegal":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Legalized":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Not Useful":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Notice delivered":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Notice Signed":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Ready For Action":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Ready For Sign":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Signboard":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "To be verified":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			default:
				myIcon = site_url + `public/banner-real.png`;
				// myIcon = site_url + `/public/banner_markers/notice_delivered_3.png`;
				break;
		}
		if (userType == "Admin" || userType == "admin") {
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(locations[i][1], locations[i][2]),
				map: map,
				icon: myIcon,
				animation: google.maps.Animation.DROP,
				title: locations[i][5] + " | " + locations[i][0],
				optimized: true,
				draggable: true,
			});
		} else {
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(locations[i][1], locations[i][2]),
				map: map,
				title: locations[i][5] + " | " + locations[i][0],
				animation: google.maps.Animation.DROP,
				icon: myIcon,
				optimized: true,
				draggable: false,
			});
		}
		google.maps.event.addListener(
			marker,
			"click",
			async function (event, marker, i) {
				// $("#loaderModal").modal("show", {
				// 	backdrop: "static",
				// 	keyboard: false,
				// });
				const lat = event.latLng.lat();
				const long = event.latLng.lng();
				const rawResponse = await fetch(site_url + "get-marker-data", {
					method: "POST",
					body: JSON.stringify({ lat, long }),
				});
				const obj = await rawResponse.json();
				const status = await obj.status;
				const data = await obj.data;
				const msg = await obj.msg;
				if (status == 200) {
					// $("#loaderModal").modal("hide", {
					// 	backdrop: "static",
					// 	keyboard: false,
					// });
					$(".marker_info_table").empty();
					$(".marker_info_table").append(
						`
							<tr>
							<th>Banner ID</th>
							<td>${data.banner_id}</td>


							<th>Banner Owner Name</th>
							<td>${data.banner_owner_name}</td>

							<th>Actual Length (PMC)</th>
							<td>${data.actual_length}</td>
							</tr>


							<tr>
							<th>Banner Location Long</th>
							<td>${data.banner_location_lng}</td>
							<th>Banner Contact Number</th>
							<td>${data.property_contact_no}</td>
							<th>Actual Breadth (PMC)</th>
							<td>${data.actual_breadth}</td>
							</tr>


							<tr>
							<th>Banner Location Lat</th>
							<td>${data.banner_location_lat}</td>
							<th>Property No</th>
							<td>${data.property_no}</td>
							<th>Actual Area(PMC)</th>
							<td>${data.actual_area}</td>
							
							</tr>


							<tr>
							<th>Plate Available</th>
							<td>${data.plate_available_value}</td>
							<th>Advertisor Name</th>
							<td>${data.property_name}</td>
							<th>Existing Length(saar)</th>
							<td>${data.existing_length}</td>
							
							</tr>


							<tr>
							<th>Installed On</th>
							<td>${data.installed_on_value}</td>
							<th>Property Contact Number</th>
							<td>${data.property_contact_no}</td>
							<th>Existing Breadth(saar)</th>
							<td>${data.existing_breadth}</td>
							</tr>


							<tr>
							<th>Banner Status</th>
							<td>${data.banner_status_value}</td>
							<th>Address</th>
							<td>${data.address}</td>
							<th>Existing Area</th>
							<td>${data.existing_area}</td>
							</tr>

							<tr>
							<th>Old Id</th>
							<td>${data.old_id}</td>
							<th>Landmark</th>
							<td>${data.landmark}</td>
							<th>Remarks</th>
							<td>${data.remarks}</td>
							</tr>

							<tr>
							<th>Survey Date Time</th>
							<td>${data.survey_date} ${data.survey_time}</td>
							<th>Ward Name</th>
							<td>${data.ward_name}</td>
							</tr>










						

							<tr>
							<td>
							<img onerror="this.src='https://via.placeholder.com/150'" style="width:250px;height:120px" class="form-control" src="http://103.233.79.142:90/images_banner/banner_images/${data.banner_id}_BPWH.jpg">
							</td>

							<td>
							<img onerror="this.src='https://via.placeholder.com/150'" style="width:250px;height:120px" class="form-control" src="http://103.233.79.142:90/images_banner/banner_images/${data.banner_id}_HP.jpg">
							</td>


							<td>
							<img onerror="this.src='https://via.placeholder.com/150'" style="width:250px;height:120px" class="form-control" src="http://103.233.79.142:90/images_banner/banner_images/${data.banner_id}_AP1.jpg">
							</td>



							<td>
							<img onerror="this.src='https://via.placeholder.com/150'" style="width:250px;height:120px" class="form-control" src="http://103.233.79.142:90/images_banner/banner_images/${data.banner_id}_AP2.jpg">
							</td>


							<td>
							<img onerror="this.src='https://via.placeholder.com/150'" style="width:250px;height:120px" class="form-control" src="http://103.233.79.142:90/images_banner/banner_images/${data.banner_id}_EBWSP.jpg">
							</td>

							</tr>
							`
					);
					$("#MarkerFormOneModal").modal("show");
				}
			}
		);
	
		if (userType == "Admin" || userType == "admin") {
			google.maps.event.addListener(
				marker,
				"dragend",
				(function (marker, i) {
					return async function (event) {
						try {
							const id = locations[i][5];
							const conf = confirm("Are you sure want to update location ?");
							if (conf == true) {
								// $("#loaderModal").modal("show", { backdrop: "static", keyboard: false });
								const lat = event.latLng.lat();
								const long = event.latLng.lng();
								// console.log(id);
								// return
								const rawResponse = await fetch(
									site_url + "update-map-lat-long",
									{
										method: "POST",
										body: JSON.stringify({ id, lat, long }),
									}
								);
								const obj = await rawResponse.json();
								const status = await obj.status;
								const msg = await obj.msg;
								// $("#loaderModal").css('display','none');
								// $("#loaderModal").modal("hide", { backdrop: "static", keyboard: true });
								if (status == 200) {
									$.toast({
										heading: "Success",
										text: msg,
										position: "top-right",
										stack: false,
									});
								}
							} else {
								const rawResponse = await fetch(
									site_url + "MapController/getMerkerDataByID",
									{
										method: "POST",
										body: JSON.stringify({ id }),
									}
								);
								const obj = await rawResponse.json();
								const status = await obj.status;
								const data = await obj.data;
								// const msg = await obj.msg;
								if (status == 200) {
									console.log(data);
									var latlng = new google.maps.LatLng(
										data.banner_location_lat,
										data.banner_location_lng
									);
									marker.setPosition(latlng);
									// $("#latitude").val(data.banner_location_lat);
									// $("#longitude").val(data.banner_location_lng);
								}
							}
						} catch (error) {
							$.toast({
								heading: "Error",
								text: error,
								position: "top-right",
								stack: false,
							});
						}
					};
				})(marker, i)
			);
		}
	} // end for loop
	




		}
		else
		{
			$.toast({
				heading: "Error",
				text: msg,
				position: "top-right",
				stack: false,
			});
		}
	} catch (error) {
		$.toast({
			heading: "Error",
			text: error,
			position: "top-right",
			stack: false,
		});
	}
}



$(() => {
	$("select").select2();
	$(".simplebar-content li").click(function () {
		$(".simplebar-content li").removeClass("active");
		$(this).addClass("active");
	});
	loadMasterDataTable();
	console.log(segment)
	switch (segment) {
		case "dashboard":
			floatSurveyGraph();
			mediaTypeGraph();
			availablePlateData();
			break;
		case "users-list":
			// alert('users list');
			getUserList();
			break;
		case "survey-count":
			// alert('survey count')
			getBannerSurveyourCountList();
			break;
		case "survey-list":
			getBannerSurveyList();
			break;
		case "map":
			ploatMapMarker();
			break;
		case "ward-summary-list":
			wardSummaries();
			break;
		case "signboard-list":
			getSignBoardList();
			break;
		case "users-report":
			var d = new Date();
			var strDate = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate();
			console.warn(strDate+" " +strDate)
			getSignBoardreprotList(strDate,strDate);
			break;
	}
	$(".survey_count_date_range").flatpickr({
		altInput: true,
		altFormat: "F j, Y",
		mode: "range",
		onClose: function () {
			console.log(surveyTableType);
			if (surveyTableType == "allData") getSurveyourFilterData();
			if (surveyTableType == "countData") showFilteredCount();
		},
	});
	$(".ward__summary__date__range").flatpickr({
		altInput: true,
		altFormat: "F j, Y",
		mode: "range",
		onClose: function () {
			wardSummaries();
			// console.log(surveyTableType);
			// if (surveyTableType == "allData") getSurveyourFilterData();
			// if (surveyTableType == "countData") showFilteredCount();
		},
	});
}); // end on ready function
//changeMarker
const changeMarker = async (argV) => {
	const id = $(argV).attr("data-id");
	const value = argV.value;
	const rawResponse = await fetch(site_url + "change-map-marker", {
		method: "POST",
		body: JSON.stringify({ id, value }),
	});
	const obj = await rawResponse.json();
	const status = await obj.status;
	const msg = await obj.msg;
	console.log(value + "||" + id);
};
$(".user-frm").validate({
	rules: {},
	submitHandler: async (frm) => {
		try {
			const rawResponse = await fetch(frm.action, {
				method: frm.method,
				body: new FormData(frm),
			});
			const obj = await rawResponse.json();
			const status = await obj.status;
			if (status == 200 || status == 201) {
				$(".user-frm")[0].reset();
				$("#userAddEditModal").modal("hide");
				$.toast({
					heading: "Success",
					text: obj.msg,
					position: "top-right",
					stack: false,
				});
			}
		} catch (error) {
			$.toast({
				heading: "Error",
				text: error,
				position: "top-right",
				stack: false,
			});
		} //end catch
	},
});
//addUser
const addUser = async () => {
	try {
		$("#userAddEditModal").modal("show");
		const rawResponse = await fetch(site_url + "get-user-type", {
			method: "GET",
		});
		const obj = await rawResponse.json();
		$('.user_type').empty();
		obj.user_role.map((item) => {
			$(".user_type").append(
				`<option  value="${item.role_type}">${item.user_role}</option>`
			);
		});

		$(".user_type").prepend(
			`<option  value="" selected disabled>Please select user role type</option>`
		);
	} catch (error) {
		$.toast({
			heading: "Error",
			text: error,
			position: "top-right",
			stack: false,
		});
	}
};
//editUser
const editUser = async (argV) => {
	try {
		// $(".password").slideUp();
		const id = $(argV).attr("data-id");
		const rawResponse = await fetch(site_url + "get-user-data", {
			method: "POST",
			body: JSON.stringify({ id }),
		});
		const obj = await rawResponse.json();
		const status = await obj.status;
		const data = await obj.data;
		const msg = await obj.msg;
		const user_role = await obj.user_role;
		if (status === 200) {
			$("#userAddEditModal").modal("show");
			// $.toast({
			// 	heading: "Success",
			// 	text: msg,
			// 	position: "top-right",
			// 	stack: false,
			// });
			$(".user_type").empty();
			$('input[name="user__id"]').val(data.uid);
			$('input[name="first__name"]').val(data.first_name);
			$('input[name="last__name"]').val(data.last_name);
			$('input[name="email"]').val(data.email);
			$('input[name="mobile_number"]').val(data.mobile_no);
			$('input[name="user_type"]').val(data.abc);
			$('input[name="sqid"]').val(data.sqid);
			$('textarea[name="address"]').val(data.address);
			$('input[name="pincode"]').val(data.pincode);
			$("#city").val(data.city);
			user_role.map((obj) => {
				if (data.user_type == obj.role_type)
					$(".user_type").append(
						`<option selected value="${obj.role_type}">${obj.user_role}</option>`
					);
				else
					$(".user_type").append(
						`<option  value="${obj.role_type}">${obj.user_role}</option>`
					);
			});
		}
	} catch (error) {
		$.toast({
			heading: "Error",
			text: error,
			position: "top-right",
			stack: false,
		});
	}
};
//addUser
const addBoard = async () => {
	try {
		$("#boardAddEditModal").modal("show");
		const rawResponse = await fetch(site_url + "Signboard/type_list?table_name="+"media_type", {
			method: "GET",
		});
		const obj = await rawResponse.json();
		obj.media_type.map((item) => {
			$(".media_type").append(
				`<option  value="${item.media_type_id}">${item.media_type_value}</option>`
			);
		});
		const rawResponse1 = await fetch(site_url + "Signboard/type_list?table_name="+"nature_of_property", {
			method: "GET",
		});
		const obj1 = await rawResponse1.json();
		obj1.nature_of_property.map((item) => {
			$(".nature_of_property").append(
				`<option  value="${item.nature_of_property_id}">${item.nature_of_property_value}</option>`
			);
		});
		const rawResponse2 = await fetch(site_url + "Signboard/type_list?table_name="+"wards", {
			method: "GET",
		});
		const obj2 = await rawResponse2.json();
		obj2.wards.map((item) => {
			$(".ward_name").append(
				`<option  value="${item.ward_name_id}">${item.ward_name}</option>`
			);
		});
	} catch (error) {
		$.toast({
			heading: "Error",
			text: error,
			position: "top-right",
			stack: false,
		});
	}
};
const search_report = async () => {
	try {
		$("#boardReportModal").modal("show");		
	} catch (error) {
		$.toast({
			heading: "Error",
			text: error,
			position: "top-right",
			stack: false,
		});
	}
};
const search_data = async () => {
	// alert($('#from_date').val()+" "+$('#to_date').val());
	from_date=$('#from_date').val();
	to_date=$('#to_date').val();
	$("#boardReportModal").modal("hide");
	tables = $(".report_list_datatable").DataTable({
		pageLength: 10,
		bDestroy: true,
		lengthMenu: [10, 25, 50, 100, 200, 500],
		language: {
			processing: `<img src="${site_url}public/ajax-loader.gif"></img>`,
		},		
		ajax:site_url+"Report/list_data?from_date="+from_date+"&to_date="+to_date,
	});
}
//editUser
const editBoard = async (argV) => {
	try {
		// $(".password").slideUp();
		const id = $(argV).attr("data-id");
		const rawResponse = await fetch(site_url + "Signboard/signboard_data", {
			method: "POST",
			body: JSON.stringify({ id }),
		});
		const obj = await rawResponse.json();
		const status = await obj.status;
		const data = await obj.data;
		console.warn(data);
		const msg = await obj.msg;
		const media_type = await obj.media_type;
		const nature_of_property = await obj.nature_of_property;
		const wards = await obj.wards;
		if (status === 200) {
			$("#boardAddEditModal").modal("show");
			// $.toast({
			// 	heading: "Success",
			// 	text: msg,
			// 	position: "top-right",
			// 	stack: false,
			// });
			$(".media_type").empty();
			$(".nature_of_property").empty();
			$(".wards").empty();
			$('input[name="signboard_id"]').val(data.signboard_id);
			$('input[name="media_type_id"]').val(data.media_type_id);
			$('input[name="ward_name_id"]').val(data.ward_name_id);
			$('input[name="proper_road_name"]').val(data.proper_road_name);
			$('input[name="property_id"]').val(data.property_id);
			$('input[name="building_id"]').val(data.building_id);
			$('input[name="no_of_visible_board"]').val(data.no_of_visible_board);
			$('#legalization_possibilities').val(data.legalization_possibilities);
			$('textarea[name="p_address"]').val(data.p_address);
			$('input[name="nature_of_property_id"]').val(data.nature_of_property_id);
			$('input[name="other_property_name"]').val(data.other_property_name);
			$('input[name="mobile_number"]').val(data.mobile_number);
			$('input[name="p_name"]').val(data.p_name);
			$('input[name="photo_no"]').val(data.photo_no);
			$('input[name="approx_l1"]').val(data.approx_l1);
			$('input[name="approx_w1"]').val(data.approx_w1);
			$('input[name="approx_l2"]').val(data.approx_l2);
			$('input[name="approx_w2"]').val(data.approx_w2);
			$('input[name="approx_l3"]').val(data.approx_l3);
			$('input[name="approx_w3"]').val(data.approx_w3);
			$('input[name="approx_l4"]').val(data.approx_l4);
			$('input[name="approx_w4"]').val(data.approx_w4);
			$('input[name="approx_l5"]').val(data.approx_l5);
			$('input[name="approx_w5"]').val(data.approx_w5);
			$('select').select2()
			media_type.map((obj) => {
				if (data.media_type_id == obj.media_type_id)
					$(".media_type").append(
						`<option selected value="${obj.media_type_id}">${obj.media_type_value}</option>`
					);
				else
					$(".media_type").append(
						`<option  value="${obj.media_type_id}">${obj.media_type_value}</option>`
					);
			});
			nature_of_property.map((obj) => {
				if (data.nature_of_property_id == obj.nature_of_property_id)
					$(".nature_of_property").append(
						`<option selected value="${obj.nature_of_property_id}">${obj.nature_of_property_value}</option>`
					);
				else
					$(".nature_of_property").append(
						`<option  value="${obj.nature_of_property_id}">${obj.nature_of_property_value}</option>`
					);
			});
			wards.map((obj) => {
				if (data.ward_name_id == obj.ward_name_id)
					$(".ward_name").append(
						`<option selected value="${obj.ward_name_id}">${obj.ward_name}</option>`
					);
				else
					$(".ward_name").append(
						`<option  value="${obj.ward_name_id}">${obj.ward_name}</option>`
					);
			});
		}
	} catch (error) {
		$.toast({
			heading: "Error",
			text: error,
			position: "top-right",
			stack: false,
		});
	}
};
$(document).on("change", ".nature_of_property", function () {
	$('.other_property_type').css("display", "none");
	$('.other_property_type input').attr("required", false);
	if ($(this).val() ==6)
	{
		$('.other_property_type').css("display", "block");
		$('.other_property_type input').attr("required", true);
	}	
});
const loadMasterDataTable = () => {
	$(".master_datatable").DataTable({
		pageLength: 10,
		lengthMenu: [10, 25, 50, 100, 200, 500],
		language: {
			processing: `<img src="${site_url}public/ajax-loader.gif"></img>`,
		},
		scrollX: true,
		bDestroy: true,
		scrollY: "50vh",
		scrollX: true,
		aaSorting: [],
	});
};
$(".board-frm").validate({
	rules: {
		"media_type_id":"required",	
		"ward_name_id":"required",	
		"proper_road_name":"required",	
		"property_id":"required",	
		"building_id":"required",	
		"no_of_visible_board":"required",	
		"legalization_possibilities":"required",	
		"nature_of_property_id":"required",	
		"mobile_number":"required",	
		"p_name":"required",	
		"p_address":"required",	
		"photo_no":"required",	
		"approx_l1":"required",	
		"approx_w1":"required",	
		"approx_l2":"required",	
		"approx_w2":"required",	
		"approx_l3":"required",	
		"approx_w3":"required",	
		"approx_l4":"required",	
		"approx_w4":"required",	
		"approx_l5":"required",	
		"approx_w5":"required",	
	},
	submitHandler: async (frm) => {
		try {
			const rawResponse = await fetch(frm.action, {
				method: frm.method,
				body: new FormData(frm),
			});
			const content = await rawResponse.json();
			const status = await content.status;
			const msg = await content.msg;
			if (status === 200) {
				$.toast({
					heading: "Success",
					text: msg,
					position: "top-right",
					stack: false,
				});
				$(".board-frm")[0].reset();
				$("#boardAddEditModal").modal("hide");
				setTimeout(() => {
					location.reload();
				}, 1500);
			}
		} catch (error) {
			$.toast({
				heading: "Error",
				text: error,
				position: "top-right",
				stack: false,
			});
		}
	},
});
$('.numeric_class').on("change",function(){
    var field_value = $(this).val();
//    alert(field_value);
    if (/^[0-9.]{1,}$/.test(field_value)==false) {
        $(this).val("");
        $(this).focus();
        $(this).parent('div').find('.msg').html("Enter digit only");
    }else{
        $(this).parent('div').find('.msg').html("");                
    }
});
$('.positive_numeric').on("change",function(){
    var field_value = $(this).val();
//    alert(field_value);
    if (/^[0-9]{1,}$/.test(field_value)==false) {
        $(this).val("");
        $(this).focus();
        $(this).parent('div').find('.msg').html("Enter positive digit only");
    }else{
        $(this).parent('div').find('.msg').html("");                
    }
});
$('.alphabets_dots').on("change",function(){
    var field_value = $(this).val();
    if (/^[a-zA-Z. ]{0,}$/.test(field_value)==false) {
        $(this).val("");
        $(this).focus();
        $(this).parent('div').find('.msg').html("Use only Alphabets and space");
    }else{
       $(this).parent('div').find('.msg').html("");
    }
}); 
$('.mobile_class').on("change",function(){
    var field_value = $(this).val();
    if (/^[6-9]{1}[0-9]{9}$/.test(field_value)==false) {
        $(this).val("");
        $(this).focus();
        $(this).parent('div').find('.msg').html("Enter valid mobile number");
    }else{
       $(this).parent('div').find('.msg').html("");
    }
});
$('.alpha_numeric_class').on("change",function(){
    var field_value = $(this).val();
    if (/^[a-zA-Z0-9.]{1,}$/.test(field_value)==false) {
        $(this).val("");
        $(this).focus();
        $(this).parent('div').find('.msg').html("Enter only alpha-numeric digit");
    }else{
        $(this).parent('div').find('.msg').html("");                
    }
}); 
$(".master-data-frm").validate({
	submitHandler: async (frm) => {
		try {
			const rawResponse = await fetch(frm.action, {
				method: frm.method,
				body: new FormData(frm),
			});
			const content = await rawResponse.json();
			const status = await content.status;
			const msg = await content.msg;
			// console.log(content);
			if (status === 200) {
				$.toast({
					heading: "Success",
					text: msg,
					position: "top-right",
					stack: false,
				});
				$("#masterEditModal").modal("hide");
				setTimeout(() => {
					location.reload();
				}, 1500);
			}
		} catch (error) {
			$.toast({
				heading: "Error",
				text: error,
				position: "top-right",
				stack: false,
			});
		}
	},
});
const getMasterData = async (argV) => {
	const id = $(argV).attr("data-id");
	const table_column_name = $(argV).attr("data-column-name");
	const table_name = $(argV).attr("data-table-name");
	console.log(id + "|" + table_column_name + "|" + table_name);
	const rawResponse = await fetch(site_url + "get-master-data", {
		method: "POST",
		body: JSON.stringify({
			id,
			table_name,
			table_column_name,
		}),
	});
	const obj = await rawResponse.json();
	const data = await obj.data;
	const status = await obj.status;
	const msg = await obj.msg;
	if (status === 200) {
		$("#masterEditModal").modal("show");
		$.toast({
			heading: "Success",
			text: msg,
			position: "top-right",
			stack: false,
		});
		switch (table_name) {
			case "category":
				$("#masterEditModalLabel").text("Edit Category");
				$(".master__data__div").empty();
				var appendData = `<div class="col-md-4">
				<div class="mb-1">
				<label>Category Name</label>
				<input class="form-control" name="category_value" value="${data.category_value}">
				</div>
				</div>
				<input name="category_id" type="hidden" value="${id}">
				<input type="hidden" name="table_name" value="${table_name}">
				<input type="hidden" name="column_name" value="${table_column_name}">
				<input type="hidden" name="update_column" value="category_value">
				`;
				var newData = $(appendData);
				newData.hide();
				$(".master__data__div").append(newData);
				newData.slideDown();
				break;
			case "banner_status":
				$("#masterEditModalLabel").text("Edit Banner Status");
				$(".master__data__div").empty();
				var appendData = `<div class="col-md-4">
				<div class="mb-1">
				<label>Banner Status </label>
				<input class="form-control" name="banner_status_value" value="${data.banner_status_value}">
				</div>
				</div>
				<input name="id" type="hidden" value="${id}">
				<input type="hidden" name="table_name" value="${table_name}">
				<input type="hidden" name="column_name" value="${table_column_name}">
				<input type="hidden" name="update_column" value="banner_status_value">
				`;
				var newData = $(appendData);
				newData.hide();
				$(".master__data__div").append(newData);
				newData.slideDown();
				break;
			case "board_nature":
				$("#masterEditModalLabel").text("Edit Board Nature");
				$(".master__data__div").empty();
				var appendData = `<div class="col-md-4">
						<div class="mb-1">
						<label>Banner Status </label>
						<input class="form-control" name="board_nature_value" value="${data.board_nature_value}">
						</div>
						</div>
						<input name="board_nature_id" type="hidden" value="${id}">
						<input type="hidden" name="table_name" value="${table_name}">
						<input type="hidden" name="column_name" value="${table_column_name}">
						<input type="hidden" name="update_column" value="board_nature_value">
						`;
				var newData = $(appendData);
				newData.hide();
				$(".master__data__div").append(newData);
				newData.slideDown();
				break;
			case "govt_type":
				$("#masterEditModalLabel").text("Edit Government Type");
				$(".master__data__div").empty();
				var appendData = `<div class="col-md-4">
						<div class="mb-1">
						<label>Government Type</label>
						<input class="form-control" name="govt_type_value" value="${data.govt_type_value}">
						</div>
						</div>
						<input name="govt_type_id" type="hidden" value="${id}">
						<input type="hidden" name="table_name" value="${table_name}">
						<input type="hidden" name="column_name" value="${table_column_name}">
						<input type="hidden" name="update_column" value="govt_type_value">
						`;
				var newData = $(appendData);
				newData.hide();
				$(".master__data__div").append(newData);
				newData.slideDown();
				break;
			case "installed_on":
				$("#masterEditModalLabel").text("Edit Installed On");
				$(".master__data__div").empty();
				var appendData = `<div class="col-md-4">
						<div class="mb-1">
						<label>Installed On</label>
						<input class="form-control" name="installed_on_value" value="${data.installed_on_value}">
						</div>
						</div>
						<input name="installed_on_id" type="hidden" value="${id}">
						<input type="hidden" name="table_name" value="${table_name}">
						<input type="hidden" name="column_name" value="${table_column_name}">
						<input type="hidden" name="update_column" value="installed_on_value">
						`;
				var newData = $(appendData);
				newData.hide();
				$(".master__data__div").append(newData);
				newData.slideDown();
				break;
			case "media_type":
				$("#masterEditModalLabel").text("Edit Media Type");
				$(".master__data__div").empty();
				var appendData = `<div class="col-md-4">
						<div class="mb-1">
						<label>Media Type</label>
						<input class="form-control" name="media_type_value" value="${data.media_type_value}">
						</div>
						</div>
						<input name="media_type_id" type="hidden" value="${id}">
						<input type="hidden" name="table_name" value="${table_name}">
						<input type="hidden" name="column_name" value="${table_column_name}">
						<input type="hidden" name="update_column" value="media_type_value">
						`;
				var newData = $(appendData);
				newData.hide();
				$(".master__data__div").append(newData);
				newData.slideDown();
				break;
			case "nature_of_property":
				$("#masterEditModalLabel").text("Edit Nature Of Property");
				$(".master__data__div").empty();
				var appendData = `<div class="col-md-4">
						<div class="mb-1">
						<label>Nature Of Property</label>
						<input class="form-control" name="nature_of_property_value" value="${data.nature_of_property_value}">
						</div>
						</div>
						<input name="nature_of_property_id" type="hidden" value="${id}">
						<input type="hidden" name="table_name" value="${table_name}">
						<input type="hidden" name="column_name" value="${table_column_name}">
						<input type="hidden" name="update_column" value="nature_of_property_value">
						`;
				var newData = $(appendData);
				newData.hide();
				$(".master__data__div").append(newData);
				newData.slideDown();
				break;
			case "notice_type":
				$("#masterEditModalLabel").text("Edit Notice Type");
				$(".master__data__div").empty();
				var appendData = `<div class="col-md-4">
						<div class="mb-1">
						<label>Notice Type</label>
						<input class="form-control" name="notice_type_value" value="${data.notice_type_value}">
						</div>
						</div>
						<input name="notice_type_id" type="hidden" value="${id}">
						<input type="hidden" name="table_name" value="${table_name}">
						<input type="hidden" name="column_name" value="${table_column_name}">
						<input type="hidden" name="update_column" value="notice_type_value">
						`;
				var newData = $(appendData);
				newData.hide();
				$(".master__data__div").append(newData);
				newData.slideDown();
				break;
			case "one_half_meters_from_road":
				$("#masterEditModalLabel").text("Edit One And Half Meter From Road ");
				$(".master__data__div").empty();
				var appendData = `<div class="col-md-4">
						<div class="mb-1">
						<label>One And Half Meter From Road</label>
						<input class="form-control" name="one_and_a_half_meters_from_road_value" value="${data.one_and_a_half_meters_from_road_value}">
						</div>
						</div>
						<input name="one_and_a_half_meters_from_road_id" type="hidden" value="${id}">
						<input type="hidden" name="table_name" value="${table_name}">
						<input type="hidden" name="column_name" value="${table_column_name}">
						<input type="hidden" name="update_column" value="one_and_a_half_meters_from_road_value">
						`;
				var newData = $(appendData);
				newData.hide();
				$(".master__data__div").append(newData);
				newData.slideDown();
				break;
			case "owner_type":
				$("#masterEditModalLabel").text("Edit Owner Type");
				$(".master__data__div").empty();
				var appendData = `<div class="col-md-4">
						<div class="mb-1">
						<label>One And Half Meter From Road</label>
						<input class="form-control" name="owner_type_value" value="${data.owner_type_value}">
						</div>
						</div>
						<input name="owner_type_id" type="hidden" value="${id}">
						<input type="hidden" name="table_name" value="${table_name}">
						<input type="hidden" name="column_name" value="${table_column_name}">
						<input type="hidden" name="update_column" value="owner_type_value">
						`;
				var newData = $(appendData);
				newData.hide();
				$(".master__data__div").append(newData);
				newData.slideDown();
				break;
			case "plate_available":
				$("#masterEditModalLabel").text("Edit Plate Available");
				$(".master__data__div").empty();
				var appendData = `<div class="col-md-4">
						<div class="mb-1">
						<label>Plate Available</label>
						<input class="form-control" name="plate_available_value" value="${data.plate_available_value}">
						</div>
						</div>
						<input name="plate_available_id" type="hidden" value="${id}">
						<input type="hidden" name="table_name" value="${table_name}">
						<input type="hidden" name="column_name" value="${table_column_name}">
						<input type="hidden" name="update_column" value="plate_available_value">
						`;
				var newData = $(appendData);
				newData.hide();
				$(".master__data__div").append(newData);
				newData.slideDown();
				break;
			case "wards":
				$("#masterEditModalLabel").text("Edit Wards");
				$(".master__data__div").empty();
				var appendData = `<div class="col-md-4">
						<div class="mb-1">
						<label>Ward Name</label>
						<input class="form-control" name="ward_name" value="${data.ward_name}">
						</div>
						</div>
						<input name="ward_name_id" type="hidden" value="${id}">
						<input type="hidden" name="table_name" value="${table_name}">
						<input type="hidden" name="column_name" value="${table_column_name}">
						<input type="hidden" name="update_column" value="ward_name">
						`;
				var newData = $(appendData);
				newData.hide();
				$(".master__data__div").append(newData);
				newData.slideDown();
				break;
		} //end switch
	} //end if
	console.log(obj);
};
const ploatMapMarker = async () => {
	let marker, i, myIcon;
	const rawResponse = await fetch(site_url + "get-map-lat-long", {
		method: "GET",
	});
	const obj = await rawResponse.json();
	const status = await obj.status;
	const msg = await obj.msg;
	const data = await obj.data;
	let locations = data;
	let map = new google.maps.Map(document.getElementById("map"), {
		zoom: 10,
		center: new google.maps.LatLng(18.5204, 73.8567),
		// mapTypeId:google.maps.MapTypeId.TERRAIN,
		mapTypeId: google.maps.MapTypeId.HYBRID,
		// mapTypeId: google.maps.MapTypeId.ROADMAP,
	});
	let infowindow = new google.maps.InfoWindow();
	// var myIcon = site_url + "/public/banner-real.png";
	for (i = 0; i < locations.length; i++) {
		let banner_status = locations[i][3];
		switch (banner_status) {
			case "Approached for sanction":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Court Case":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Demolished":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Existing Id Found":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Illegal":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Legalized":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Not Useful":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Notice delivered":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Notice Signed":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Ready For Action":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Ready For Sign":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "Signboard":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			case "To be verified":
				myIcon = site_url + `public/banner_markers/${locations[i][4]}`;
				break;
			default:
				myIcon = site_url + `public/banner-real.png`;
				// myIcon = site_url + `/public/banner_markers/notice_delivered_3.png`;
				break;
		}
		if (userType == "Admin" || userType == "admin") {
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(locations[i][1], locations[i][2]),
				map: map,
				icon: myIcon,
				animation: google.maps.Animation.DROP,
				title: locations[i][5] + " | " + locations[i][0],
				optimized: true,
				draggable: true,
			});
		} else {
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(locations[i][1], locations[i][2]),
				map: map,
				title: locations[i][5] + " | " + locations[i][0],
				animation: google.maps.Animation.DROP,
				icon: myIcon,
				optimized: true,
				draggable: false,
			});
		}
		google.maps.event.addListener(
			marker,
			"click",
			async function (event, marker, i) {
				// $("#loaderModal").modal("show", {
				// 	backdrop: "static",
				// 	keyboard: false,
				// });
				const lat = event.latLng.lat();
				const long = event.latLng.lng();
				const rawResponse = await fetch(site_url + "get-marker-data", {
					method: "POST",
					body: JSON.stringify({ lat, long }),
				});
				const obj = await rawResponse.json();
				const status = await obj.status;
				const data = await obj.data;
				const msg = await obj.msg;
				if (status == 200) {
					// $("#loaderModal").modal("hide", {
					// 	backdrop: "static",
					// 	keyboard: false,
					// });
					$(".marker_info_table").empty();
					$(".marker_info_table").append(
						`
							<tr>
							<th>Banner ID</th>
							<td>${data.banner_id}</td>


							<th>Banner Owner Name</th>
							<td>${data.banner_owner_name}</td>

							<th>Actual Length (PMC)</th>
							<td>${data.actual_length}</td>
							</tr>


							<tr>
							<th>Banner Location Long</th>
							<td>${data.banner_location_lng}</td>
							<th>Banner Contact Number</th>
							<td>${data.property_contact_no}</td>
							<th>Actual Breadth (PMC)</th>
							<td>${data.actual_breadth}</td>
							</tr>


							<tr>
							<th>Banner Location Lat</th>
							<td>${data.banner_location_lat}</td>
							<th>Property No</th>
							<td>${data.property_no}</td>
							<th>Actual Area(PMC)</th>
							<td>${data.actual_area}</td>
							
							</tr>


							<tr>
							<th>Plate Available</th>
							<td>${data.plate_available_value}</td>
							<th>Advertisor Name</th>
							<td>${data.property_name}</td>
							<th>Existing Length(saar)</th>
							<td>${data.existing_length}</td>
							
							</tr>


							<tr>
							<th>Installed On</th>
							<td>${data.installed_on_value}</td>
							<th>Property Contact Number</th>
							<td>${data.property_contact_no}</td>
							<th>Existing Breadth(saar)</th>
							<td>${data.existing_breadth}</td>
							</tr>


							<tr>
							<th>Banner Status</th>
							<td>${data.banner_status_value}</td>
							<th>Address</th>
							<td>${data.address}</td>
							<th>Existing Area</th>
							<td>${data.existing_area}</td>
							</tr>

							<tr>
							<th>Old Id</th>
							<td>${data.old_id}</td>
							<th>Landmark</th>
							<td>${data.landmark}</td>
							<th>Remarks</th>
							<td>${data.remarks}</td>
							</tr>

							<tr>
							<th>Survey Date Time</th>
							<td>${data.survey_date} ${data.survey_time}</td>
							<th>Ward Name</th>
							<td>${data.ward_name}</td>
							</tr>










						

							<tr>
							<td>
							<img onerror="this.src='https://via.placeholder.com/150'" style="width:250px;height:120px" class="form-control" src="http://103.233.79.142:90/images_banner/banner_images/${data.banner_id}_BPWH.jpg">
							</td>

							<td>
							<img onerror="this.src='https://via.placeholder.com/150'" style="width:250px;height:120px" class="form-control" src="http://103.233.79.142:90/images_banner/banner_images/${data.banner_id}_HP.jpg">
							</td>


							<td>
							<img onerror="this.src='https://via.placeholder.com/150'" style="width:250px;height:120px" class="form-control" src="http://103.233.79.142:90/images_banner/banner_images/${data.banner_id}_AP1.jpg">
							</td>



							<td>
							<img onerror="this.src='https://via.placeholder.com/150'" style="width:250px;height:120px" class="form-control" src="http://103.233.79.142:90/images_banner/banner_images/${data.banner_id}_AP2.jpg">
							</td>


							<td>
							<img onerror="this.src='https://via.placeholder.com/150'" style="width:250px;height:120px" class="form-control" src="http://103.233.79.142:90/images_banner/banner_images/${data.banner_id}_EBWSP.jpg">
							</td>

							</tr>
							`
					);
					$("#MarkerFormOneModal").modal("show");
				}
			}
		);
	
		if (userType == "Admin" || userType == "admin") {
			google.maps.event.addListener(
				marker,
				"dragend",
				(function (marker, i) {
					return async function (event) {
						try {
							const id = locations[i][5];
							const conf = confirm("Are you sure want to update location ?");
							if (conf == true) {
								// $("#loaderModal").modal("show", { backdrop: "static", keyboard: false });
								const lat = event.latLng.lat();
								const long = event.latLng.lng();
								// console.log(id);
								// return
								const rawResponse = await fetch(
									site_url + "update-map-lat-long",
									{
										method: "POST",
										body: JSON.stringify({ id, lat, long }),
									}
								);
								const obj = await rawResponse.json();
								const status = await obj.status;
								const msg = await obj.msg;
								// $("#loaderModal").css('display','none');
								// $("#loaderModal").modal("hide", { backdrop: "static", keyboard: true });
								if (status == 200) {
									$.toast({
										heading: "Success",
										text: msg,
										position: "top-right",
										stack: false,
									});
								}
							} else {
								const rawResponse = await fetch(
									site_url + "MapController/getMerkerDataByID",
									{
										method: "POST",
										body: JSON.stringify({ id }),
									}
								);
								const obj = await rawResponse.json();
								const status = await obj.status;
								const data = await obj.data;
								// const msg = await obj.msg;
								if (status == 200) {
									console.log(data);
									var latlng = new google.maps.LatLng(
										data.banner_location_lat,
										data.banner_location_lng
									);
									marker.setPosition(latlng);
									// $("#latitude").val(data.banner_location_lat);
									// $("#longitude").val(data.banner_location_lng);
								}
							}
						} catch (error) {
							$.toast({
								heading: "Error",
								text: error,
								position: "top-right",
								stack: false,
							});
						}
					};
				})(marker, i)
			);
		}
	} // end for loop
	
};
Date.prototype.monthNames = [
	"January",
	"February",
	"March",
	"April",
	"May",
	"June",
	"July",
	"August",
	"September",
	"October",
	"November",
	"December",
];
Date.prototype.getMonthName = function () {
	return this.monthNames[this.getMonth()];
};
Date.prototype.getShortMonthName = function () {
	return this.getMonthName().substr(0, 3);
};
const getSurveyourFilterData = async () => {
	try {
		let new_date;
		const date = $("#date_filter_count").val();
		// console.log("🚀 ~ file: custom.js ~ line 870 ~ getSurveyourFilterData ~ date", date)
		// return
		const userIds = $("#surveyour__id").val();
		const rawResponse = await fetch(site_url + "get-surveyour-counts", {
			method: "POST",
			body: JSON.stringify({ userIds, date }),
		});
		const obj = await rawResponse.json();
		const status = await obj.status;
		const msg = await obj.msg;
		const data = await obj.data;
		const count_data_arr = data.count_data_arr;
		const date_arr = data.date_arr;
		const table_headging_arr = data.table_headging_arr;
		// console.log(data);
		// return
		// $(".banner__surveyour__count__datatable tbody").empty();
		// $(".survey__count__tbody").empty();
		var table = $(".banner__surveyour__count__datatable").DataTable();
		table.clear().draw();
		table.destroy();
		if (status == 200) {
			$.toast({
				heading: "Success",
				text: msg,
				position: "top-right",
				stack: false,
			});
			// if(date!=undefined)
			// new_date = new Date(date);
			// else
			// new_date = new Date();
			// const year = new_date.getFullYear();
			// const month = new_date.getShortMonthName();
			// const day = new_date.getDate();
			if (date != "") {
				$(".banner__surveyour__count__datatable thead").empty();
				$(".banner__surveyour__count__datatable tfoot").empty();
				let tableHead = "<tr>";
				table_headging_arr.map((item) => {
					tableHead += `<th>${item}</th>`;
				});
				tableHead += "</tr>";
				$(".banner__surveyour__count__datatable thead").append(tableHead);
				$(".banner__surveyour__count__datatable tfoot").append(tableHead);
			}
			// if(count_data_arr.length > 0)
			// {
			let tBody;
			count_data_arr.map((uD) => {
				tBody = `<tr>
					<td>${uD.name}</td>`;
				for (let j = 0; j < uD.formDataArr.length; j++) {
					console.log(uD.formDataArr[j]);
					tBody += `<td>${uD.formDataArr[j]}</td>`;
				}
				tBody += "</tr>";
				const newData = $(tBody);
				newData.hide();
				$(".banner__surveyour__count__datatable tbody").append(newData);
				newData.slideDown();
			});
		}
		getBannerSurveyourCountList();
	} catch (error) {
		console.log(
			"🚀 ~ file: custom.js ~ line 940 ~ getSurveyourFilterData ~ error",
			error
		);
		$.toast({
			heading: "Error",
			text: error,
			position: "top-right",
			stack: false,
		});
	}
};
//getSurveyCountList
const getSurveyCountList = async () => {
	// location.reload();
	$(".ward__name__div").slideUp();
	surveyTableType = "allData";
	var table = $(".banner__survey__count__datatable").DataTable();
	table.clear().draw();
	table.destroy();
	const countTable = $("#survey_count_datatable");
	countTable.removeClass("banner__survey__count__datatable");
	countTable.addClass("banner__surveyour__count__datatable");
	$(".banner__surveyour__count__datatable thead,tfoot").empty();
	const date = $("#date_filter_count").val();
	const userIds = $("#surveyour__id").val();
	const rawResponse = await fetch(site_url + "get-surveyour-counts", {
		method: "POST",
		body: JSON.stringify({ userIds, date }),
	});
	const obj = await rawResponse.json();
	const status = await obj.status;
	const msg = await obj.msg;
	const data = await obj.data;
	const count_data_arr = data.count_data_arr;
	const date_arr = data.date_arr;
	const table_headging_arr = data.table_headging_arr;
	// console.log(data);
	// return
	// $(".banner__surveyour__count__datatable tbody").empty();
	// $(".survey__count__tbody").empty();
	// var table = $(".banner__surveyour__count__datatable").DataTable();
	// table.clear().draw();
	// table.destroy();
	if (status == 200) {
		$.toast({
			heading: "Success",
			text: msg,
			position: "top-right",
			stack: false,
		});
		$(".banner__surveyour__count__datatable thead").empty();
		$(".banner__surveyour__count__datatable tfoot").empty();
		let tableHead = "<tr>";
		table_headging_arr.map((item) => {
			tableHead += `<th>${item}</th>`;
		});
		tableHead += "</tr>";
		$(".banner__surveyour__count__datatable thead").append(tableHead);
		$(".banner__surveyour__count__datatable tfoot").append(tableHead);
		// if(count_data_arr.length > 0)
		// {
		let tBody;
		count_data_arr.map((uD) => {
			tBody = `<tr>
				<td>${uD.name}</td>`;
			for (let j = 0; j < uD.formDataArr.length; j++) {
				console.log(uD.formDataArr[j]);
				tBody += `<td>${uD.formDataArr[j]}</td>`;
			}
			tBody += "</tr>";
			const newData = $(tBody);
			newData.hide();
			$(".banner__surveyour__count__datatable tbody").append(newData);
			newData.slideDown();
		});
		$("#btn__count").attr("onclick", "showFilteredCount()");
		$("#btn__count").text("Counts");
		getBannerSurveyourCountList();
	}
};
$("#ward__ids").on("change", async (e) => {
	if (segment == "survey-list") getBannerSurveyList();
	if (surveyTableType == "countData") showFilteredCount();
});
//showFilteredCount
const showFilteredCount = async () => {
	// alert('inside functuion')
	$(".ward__name__div").slideDown();
	surveyTableType = "countData";
	var table = $("#survey_count_datatable").DataTable();
	table.clear().draw();
	table.destroy();
	const countTable = $("#survey_count_datatable");
	countTable.removeClass("banner__surveyour__count__datatable");
	countTable.addClass("banner__survey__count__datatable");
	const date = $(".survey_count_date_range").val();
	const surveyour__id = $("#surveyour__id").val();
	const ward__id = $("#ward__ids").val();
	$("#survey_count_datatable thead,tfoot").empty();
	try {
		const rawResponse = await fetch(
			site_url + "SurveyController/getCurrentMonthSurveyCount",
			{
				method: "POST",
				body: JSON.stringify({ date, surveyour__id, ward__id }),
			}
		);
		const obj = await rawResponse.json();
		const status = await obj.status;
		const data = await obj.data;
		if (status == 200) {
			let emp = "--";
			$("#btn__count").attr("onclick", "getSurveyCountList()");
			$("#btn__count").text("List");
			$("#survey_count_datatable thead,tfoot").append(`<tr>
			<th>Survey Name</th>
			<th>Month</th>
			<th>Ward Name</th>
			<th>New Count</th>
			</tr>`);
			data.map((c) => {
				let d = `<tr>
				<td>${c.first_name} ${c.last_name}</th>
				<td>${c.month}</th>
				<td>`;
				if (c.ward_name != null) {
					d += c.ward_name;
				} else {
					d += emp;
				}

				d += `</th>
				<td>${c.count}</th>
			</tr>`;

				$("#survey_count_datatable tbody").append(d);
			});
		}
		getBannerSurveyourCountListV2();
	} catch (error) {
		$.toast({
			heading: "Error",
			text: error,
			position: "top-right",
			stack: false,
		});
	}
};
const getBannerSurveyourCountListV2 = () => {
	tables = $(".banner__survey__count__datatable").DataTable({
		pageLength: 10,
		lengthMenu: [10, 25, 50, 100, 200, 500],
		// // language: {
		processing: `<img src="${site_url}public/ajax-loader.gif"></img>`,
		// // },
		scrollX: true,
		// // "scrollCollapse": true,
		bSort: true,
		bDestroy: true,
		// bProcessing: true,
		scrollY: "50vh",
		scrollCollapse: true,
		// bServerSide: true,
		aaSorting: [0, "desc"],
		// autoWidth: false,
	});
};
$("#surveyour__id").on("change", async (e) => {
	// const usreids = $("#surveyour__id").val();
	// if (usreids.length == 0) location.reload();
	// else
	if (surveyTableType == "allData") getSurveyourFilterData();
	if (surveyTableType == "countData") showFilteredCount();
});
$(".banner_survey_frm__2").validate({
	submitHandler: async (frm) => {
		submitFormData(frm);
		
	},
});
$(".banner_survey_frm__3").validate({
	submitHandler: async (frm) => {
		submitFormData(frm);
		
	},
});
$(".banner_survey_frm").validate({
	rules:{
		property_contact_number:{
			digits:true,
			maxlength:10,
			minlength:10
		},
	},
	submitHandler: async (frm) => {
		submitFormData(frm);
 setTimeout(() => {
			window.location.reload(true)
		}, 1200);  
		
	},
});
const submitFormData = async (frm) => {
	try {
		const rawResponse = await fetch(frm.action, {
			method: frm.method,
			body: new FormData(frm),
		});
		const content = await rawResponse.json();
		const status = await content.status;
		const msg = await content.msg;
		// console.log(content);
		if (status === 200) {
	 clearFormFields()
			$.toast({
				heading: "Success",
				text: msg,
				position: "top-right",
				stack: false,
			});
			setTimeout(() => {
				$("#exampleModal").modal("hide");
				$("#form_2_modal").modal("hide");
				$("#form_3_modal").modal("hide");
				
				getBannerSurveyList();
			}, 1500);
		}
	} catch (error) {
		$.toast({
			heading: "Error",
			text: error,
			position: "top-right",
			stack: false,
		});
	}
};
$(".date_range_filter").change(function () {
	setTimeout(() => {
		const date = $(".date_range_filter").val();
		$(".csv_download_btn").attr(
			"href",
			`${site_url}download-survey-list-csv/${date}`
		);
		getBannerSurveyList();
	}, 1500);
});
const getUserList = () => {
	tables = $(".user_list_datatable").DataTable({
		pageLength: 10,
		lengthMenu: [10, 25, 50, 100, 200, 500],
		language: {
			processing: `<img src="${site_url}public/ajax-loader.gif"></img>`,
		},
		scrollY: "50vh",
		// "scrollCollapse": true,
		bDestroy: true,
		bProcessing: true,
		scrollY: "50vh",
		scrollX: true,
		// "scrollCollapse": true,
		bServerSide: true,
		aaSorting: [],
		// "order": [
		//          [1, 'desc']
		//     ],
		autoWidth: false,
		ajax: {
			url: site_url + "AuthController/getUserList",
			// "dataType": "json",
			type: "POST",
			data: function (data) {
				data.date_range = $("#date-range-filter").val();
			},
		},
		columnDefs: [
			{
				targets: [4, 5], // exeptions des ordres de triage
				orderable: false,
			},
		],
				
	});
};
const getSignBoardList = () => {
	tables = $(".list_datatable").DataTable({
		pageLength: 10,
		lengthMenu: [10, 25, 50, 100, 200, 500],
		language: {
			processing: `<img src="${site_url}public/ajax-loader.gif"></img>`,
		},		
		ajax:site_url+"Signboard/list_data",
	});
};
const getSignBoardreprotList = (from_date, to_date) => {
	tables = $(".report_list_datatable").DataTable({
		pageLength: 10,
		lengthMenu: [10, 25, 50, 100, 200, 500],
		language: {
			processing: `<img src="${site_url}public/ajax-loader.gif"></img>`,
		},		
		ajax:site_url+"Report/list_data?from_date="+from_date+"&to_date="+to_date,
	});
};
const getBannerSurveyList = () => {
	tables = $(".banner__survey_datatable").DataTable({
		pageLength: 10,
		lengthMenu: [10, 25, 50, 100, 200, 500],
		language: {
			processing: `<img src="${site_url}public/ajax-loader.gif"></img>`,
		},
		scrollY: "50vh",
		// "scrollCollapse": true,
		bDestroy: true,
		bProcessing: true,
		scrollY: "50vh",
		scrollX: true,
		// "scrollCollapse": true,
		bServerSide: true,
		aaSorting: [],
		// "order": [
		//          [1, 'desc']
		//     ],
		autoWidth: false,
		ajax: {
			url: site_url + "/SurveyController/getBannerSurveyList",
			// "dataType": "json",
			type: "POST",
			data: function (data) {
				data.date_range = $("#date-range-filter").val();
				data.ward_ids = $("#ward__ids").val();
				data.banner_id = $('#banner_id').val()			
			},
		},
		columnDefs: [
			{
				targets: [5], // exeptions des ordres de triage
				orderable: false,
			},
		],
	});
};
//getBannerSurveyourCountList
const getBannerSurveyourCountList = () => {
	tables = $(".banner__surveyour__count__datatable").DataTable({
		pageLength: 10,
		lengthMenu: [10, 25, 50, 100, 200, 500],
		// // language: {
		processing: `<img src="${site_url}public/ajax-loader.gif"></img>`,
		// // },
		scrollX: true,
		// // "scrollCollapse": true,
		bSort: true,
		bDestroy: true,
		// bProcessing: true,
		scrollY: "50vh",
		scrollCollapse: true,
		// bServerSide: true,
		// aaSorting: [3, "desc"],
		// autoWidth: false,
	});
};
//openModal
const openModal = async (argV) => {
	const form_number = $(argV).attr("data-form-number");
	const banner_id = $(argV).attr("data-id");
	// console.log(form_number);
	switch (form_number) {
		case "1":
			form1Data(banner_id);
			break;
		case "2":
			form2Data(banner_id);
			break;
		case "3":
			form3Data(banner_id);
			break;
	}
};
const form3Data = async (banner_id) => {
	const rawResponse = await fetch(site_url + "get-banner-data", {
		method: "POST",
		body: JSON.stringify({ banner_id, formNo: 3 }),
	});
	const content = await rawResponse.json();
	const status = await content.status;
	const banner_data = await content.banner_data;
	const noticeTypeData = await content.noticeTypeData;
	const reminderMode = await content.reminderMode;
	if (status === 200) {
		if (banner_data == null || banner_data == "") {
			Swal.fire({
				title: "Oops...",
				text: "Data not found!",
				icon: "error",
				confirmButtonClass: "btn btn-primary w-xs mt-2",
				buttonsStyling: !1,
				// footer: '<a href="">Why do I have this issue?</a>',
				showCloseButton: !0,
			});
			return;
			// $(".banner_survey_frm").remove();
			// $(".banner_survey_frm").html(`<h4>Data not found!</h4>`);
		}
		$("#form_3_modal").modal("show");
		$("#notice__type__id").empty();
		$('input[name="banner__id"]').val(banner_data.banner_id);
		//1. Notice type
		if (noticeTypeData.length > 0) {
			for (let nT of noticeTypeData) {
				if (banner_data.notice_type_id == nT.notice_type_id)
					$("#notice__type__id").append(
						`<option selected value="${nT.notice_type_id}">${nT.notice_type_value}</option>`
					);
				else
					$("#notice__type__id").append(
						`<option  value="${nT.notice_type_id}">${nT.notice_type_value}</option>`
					);
			} //end for
			if (
				banner_data.notice_type_id == null ||
				banner_data.notice_type_id == ""
			)
				$("#notice__type__id").prepend(
					`<option value="0" selected >Please select ward name</option>`
				);
		}
		if (reminderMode.length > 0) {
			for (let rM of reminderMode) {
				if (banner_data.remainder_one_mode == rM.reminder_mode_id)
					$("#reminder__one__mode").append(
						`<option selected value="${rM.reminder_mode_id}">${rM.reminder_mode_value}</option>`
					);
				else
					$("#reminder__one__mode").append(
						`<option  value="${rM.reminder_mode_id}">${rM.reminder_mode_value}</option>`
					);
				if (banner_data.remainder_two_mode == rM.reminder_mode_id)
					$("#reminder__two__mode").append(
						`<option selected value="${rM.reminder_mode_id}">${rM.reminder_mode_value}</option>`
					);
				else
					$("#reminder__two__mode").append(
						`<option  value="${rM.reminder_mode_id}">${rM.reminder_mode_value}</option>`
					);
				if (banner_data.remainder_three_mode == rM.reminder_mode_id)
					$("#reminder__three__mode").append(
						`<option selected value="${rM.reminder_mode_id}">${rM.reminder_mode_value}</option>`
					);
				else
					$("#reminder__three__mode").append(
						`<option  value="${rM.reminder_mode_id}">${rM.reminder_mode_value}</option>`
					);
			} //end for
			if (
				banner_data.remainder_one_mode == null ||
				banner_data.remainder_one_mode == ""
			)
				$("#reminder__one__mode").prepend(
					`<option value="0" selected >Please select remainder one</option>`
				);
			if (
				banner_data.remainder_two_mode == null ||
				banner_data.remainder_two_mode == ""
			)
				$("#reminder__two__mode").prepend(
					`<option value="0" selected >Please select remainder two</option>`
				);
			if (
				banner_data.remainder_three_mode == null ||
				banner_data.remainder_three_mode == ""
			)
				$("#reminder__three__mode").prepend(
					`<option value="0" selected >Please select remainder three</option>`
				);
		}
		$('input[name="reminder__one__mode__date"]').val(
			banner_data.remainder_one_mode_date
		);
		$('input[name="reminder__one__mode__time"]').val(
			banner_data.remainder_one_mode_time
		);
		$('input[name="reminder__two__mode__date"]').val(
			banner_data.remainder_two_mode_date
		);
		$('input[name="reminder__two__mode__time"]').val(
			banner_data.remainder_two_mode_time
		);
		$('input[name="reminder__three__mode__date"]').val(
			banner_data.remainder_three_mode_date
		);
		$('input[name="reminder__three__mode__time"]').val(
			banner_data.remainder_three_mode_time
		);
		// number__of__advertisement__count;
		// number__of__plate__count;
		$("#remark").val(banner_data.remark);
		$('input[name="number__of__advertisement__count"]').val(
			banner_data.no_advertisement_count
		);
		$('input[name="number__of__plate__count"]').val(banner_data.no_plate_count);
	} //end if

	$("#notice_image_1").attr(
		"src",
		`http://103.233.79.142:90/images_banner/notice_images/${banner_data.banner_id}_actual_notice_1.jpg`
	);
	$("#notice_image_2").attr(
		"src",
		`http://103.233.79.142:90/images_banner/notice_images/${banner_data.banner_id}_actual_notice_2.jpg`
	);
	$("#notice_image_3").attr(
		"src",
		`http://103.233.79.142:90/images_banner/notice_images/${banner_data.banner_id}_actual_notice_3.jpg`
	);
	$("#notice_image_4").attr(
		"src",
		`http://103.233.79.142:90/images_banner/notice_images/${banner_data.banner_id}_actual_notice_4.jpg`
	);
	$("#notice_image_5").attr(
		"src",
		`http://103.233.79.142:90/images_banner/notice_images/${banner_data.banner_id}_actual_notice_5.jpg`
	);
};
const form2Data = async (banner_id) => {
	const rawResponse = await fetch(site_url + "get-banner-data", {
		method: "POST",
		body: JSON.stringify({ banner_id, formNo: 2 }),
	});
	const content = await rawResponse.json();
	const status = await content.status;
	const banner_data = await content.banner_data;
	const media_data = await content.media_data;
	const plate_data = await content.plate_data;
	const noticeTypeData = await content.noticeTypeData;
	const categoryData = await content.categoryData;
	const govtTypeData = await content.govtTypeData;
	const installedOnData = await content.installedOnData;
	const naturePropData = await content.naturePropData;
	const ownerTypeData = await content.ownerTypeData;
	const boardNatureData = await content.boardNatureData;
	const wardData = await content.wardData;
	const oneHalfMeterRoadData = await content.oneHalfMeterRoadData;
	if (status === 200) {
		if (banner_data == null || banner_data == "") {
			Swal.fire({
				title: "Oops...",
				text: "Data not found!",
				icon: "error",
				confirmButtonClass: "btn btn-primary w-xs mt-2",
				buttonsStyling: !1,
				// footer: '<a href="">Why do I have this issue?</a>',
				showCloseButton: !0,
			});
			return;
			// $(".banner_survey_frm").remove();
			// $(".banner_survey_frm").html(`<h4>Data not found!</h4>`);
		}
		$("#form_2_modal").modal("show");
		$("#ward__id").empty();
		$("#owner__type__id").empty();
		$("#category__id").empty();
		$("#nature__of__property__id").empty();
		$("#installed__on").empty();
		$("#one__half__meter__road__id").empty();
		$("#board__nature__id").empty();
		//1. ward name
		// if (wardData.length > 0) {
		// 	for (let ward of wardData) {
		// 		if (banner_data.ward_name_id == ward.ward_name_id)
		// 			$("#ward__id").append(
		// 				`<option selected value="${ward.ward_name_id}">${ward.ward_name}</option>`
		// 			);
		// 		else
		// 			$("#ward__id").append(
		// 				`<option  value="${ward.ward_name_id}">${ward.ward_name}</option>`
		// 			);
		// 	}
		// 	if (banner_data.ward_name_id == null || banner_data.ward_name_id == "")
		// 		$("#ward__id").prepend(
		// 			`<option value="0" selected >Please select ward name</option>`
		// 		);
		// }
		//2. owner__type__id
		if (ownerTypeData.length > 0) {
			// console.warn('owner type')
			for (let ownType of ownerTypeData) {
				if (banner_data.owner_type_id == ownType.owner_type_id)
					$("#owner__type__id").append(
						`<option selected value="${ownType.owner_type_id}">${ownType.owner_type_value}</option>`
					);
				else
					$("#owner__type__id").append(
						`<option  value="${ownType.owner_type_id}">${ownType.owner_type_value}</option>`
					);
			}
			if (banner_data.owner_type_id == null || banner_data.owner_type_id == "")
				$("#owner__type__id").prepend(
					`<option value="0"  selected >Please select owner type</option>`
				);
		}
		//3. govt__type__id
		if (govtTypeData.length > 0) {
			for (let gvtT of govtTypeData) {
				if (banner_data.govt_type_id == gvtT.govt_type_id)
					$("#govt__type__id").append(
						`<option selected value="${gvtT.govt_type_id}">${gvtT.govt_type_value}</option>`
					);
				else
					$("#govt__type__id").append(
						`<option  value="${gvtT.govt_type_id}">${gvtT.govt_type_value}</option>`
					);
			}
			if (banner_data.govt_type_id == null || banner_data.govt_type_id == "")
				$("#govt__type__id").prepend(
					`<option value="0"  selected >Please select owner type</option>`
				);
		}
		//4. category__id
		if (categoryData.length > 0) {
			for (let cat of categoryData) {
				if (banner_data.category_id == cat.category_id)
					$("#category__id").append(
						`<option selected value="${cat.category_id}">${cat.category_value}</option>`
					);
				else
					$("#category__id").append(
						`<option  value="${cat.category_id}">${cat.category_value}</option>`
					);
			}
			if (banner_data.category_id == null || banner_data.category_id == "")
				$("#category__id").prepend(
					`<option value="0"  selected >Please select owner type</option>`
				);
		}
		//5. nature__of__property__id
		if (naturePropData.length > 0) {
			for (let natProp of naturePropData) {
				if (banner_data.nature_of_property_id == natProp.nature_of_property_id)
					$("#nature__of__property__id").append(
						`<option selected value="${natProp.nature_of_property_id}">${natProp.nature_of_property_value}</option>`
					);
				else
					$("#nature__of__property__id").append(
						`<option  value="${natProp.nature_of_property_id}">${natProp.nature_of_property_value}</option>`
					);
			}
			if (
				banner_data.nature_of_property_id == null ||
				banner_data.nature_of_property_id == ""
			)
				$("#nature__of__property__id").prepend(
					`<option value="0" selected >Please select nature of property</option>`
				);
		}
		//6. installed__on
		if (installedOnData.length > 0) {
			for (let instlD of installedOnData) {
				if (banner_data.installed_on_id == instlD.installed_on_id)
					$("#installed__on").append(
						`<option selected value="${instlD.installed_on_id}">${instlD.installed_on_value}</option>`
					);
				else
					$("#installed__on").append(
						`<option  value="${instlD.installed_on_id}">${instlD.installed_on_value}</option>`
					);
			} //end for
			if (
				banner_data.installed_on_id == null ||
				banner_data.installed_on_id == ""
			)
				$("#installed__on").prepend(
					`<option value="0" selected >Please select installed on value</option>`
				);
		}
		//7. one__half__meter__road__id
		if (oneHalfMeterRoadData.length > 0) {
			for (let oneHalf of oneHalfMeterRoadData) {
				if (
					banner_data.one_and_a_half_meters_from_road_id ==
					oneHalf.one_and_a_half_meters_from_road_id
				)
					$("#one__half__meter__road__id").append(
						`<option selected value="${oneHalf.one_and_a_half_meters_from_road_id}">${oneHalf.one_and_a_half_meters_from_road_value}</option>`
					);
				else
					$("#one__half__meter__road__id").append(
						`<option  value="${oneHalf.one_and_a_half_meters_from_road_id}">${oneHalf.one_and_a_half_meters_from_road_value}</option>`
					);
			}
			if (
				banner_data.one_and_a_half_meters_from_road_id == null ||
				banner_data.one_and_a_half_meters_from_road_id == ""
			)
				$("#one__half__meter__road__id").prepend(
					`<option value="0" selected >Please select one & half meter from road	</option>`
				);
		}
		//8.board__nature__id
		if (boardNatureData.length > 0) {
			for (let boarNature of boardNatureData) {
				if (banner_data.board_nature_id == boarNature.board_nature_id)
					$("#board__nature__id").append(
						`<option selected value="${boarNature.board_nature_id}">${boarNature.board_nature_value}</option>`
					);
				else
					$("#board__nature__id").append(
						`<option  value="${boarNature.board_nature_id}">${boarNature.board_nature_value}</option>`
					);
			}
			if (
				banner_data.board_nature_id == null ||
				banner_data.board_nature_id == ""
			)
				$("#board__nature__id").prepend(
					`<option value="0" selected >Please select board nature</option>`
				);
		}
		$('input[name="banner__id"]').val(banner_data.banner_id);
		$('input[name="owner__name"]').val(banner_data.owner_name);
		$('input[name="owner__uid"]').val(banner_data.owner_uid);
		$('input[name="occupier__name"]').val(banner_data.occupier_name);
		$('input[name="occupier__uid"]').val(banner_data.occupier_uid);
		$('input[name="property__name"]').val(banner_data.property_name);
		$('input[name="proper__landmark"]').val(banner_data.proper_landmark);
		$('input[name="proper__road__name"]').val(banner_data.proper_road_name);
		$("#more__details").val(banner_data.more_details);
		$('input[name="flat__number"]').val(banner_data.flat_no);
		$('input[name="house__number"]').val(banner_data.house_no);
		$('input[name="bunglow__number"]').val(banner_data.bungalow_no);
		$('input[name="Pincode"]').val(banner_data.pincode);
		$('input[name="installation_year"]').val(banner_data.installation_year);
		$('input[name="license__renewed__till"]').val(
			banner_data.license_renewed_upto
		);
		$('input[name="prop__owener__address"]').val(
			banner_data.property_owner_address
		);
		$('input[name="property__owner__mobile__number"]').val(
			banner_data.property_owner_mobile_no
		);
		$('input[name="prop__occupier__address"]').val(
			banner_data.property_occupier_address
		);
		$('input[name="property__occupier__email"]').val(
			banner_data.property_occupier_email
		);
		$('input[name="name__of__shop"]').val(banner_data.name_of_shop);
		$('input[name="manufactured___sold__item"]').val(
			banner_data.manufactured_sold_item
		);
		$('input[name="age__of__building"]').val(banner_data.age_of_building);
		$('input[name="existing__length"]').val(banner_data.existing_length);
		$('input[name="existing__breadth"]').val(banner_data.existing_breadth);
		$('input[name="existing__area"]').val(banner_data.existing_area);
		$('input[name="actual__length"]').val(banner_data.actual_length);
		$('input[name="actual__breadth"]').val(banner_data.actual_breadth);
		$('input[name="actual__area"]').val(banner_data.actual_area);
		$('input[name="difference__in__sqft"]').val(banner_data.difference_in_sqft);
		$('input[name="additional__ratable__value"]').val(
			banner_data.additional_ratable_value
		);
		$('input[name="new__square__feet"]').val(banner_data.new_square_feet);
		$('input[name="new__rateble__value__squre__feet"]').val(
			banner_data.new_ratable_value_in_square_feet
		);
		// $('input[name="banner__id"]').val(banner_data.banner_id);
		// $('input[name="old__id"]').val(banner_data.old_id);
		// $('input[name="property__name"]').val(banner_data.property_name);
		// $('input[name="society__name"]').val(banner_data.society_name);
		// $('input[name="landmark"]').val(banner_data.landmark);
		// $("#address").val(banner_data.address);
		// $('input[name="proper_roead_name"]').val(banner_data.proper_road_name);
		// $('input[name="road_status"]').val(banner_data.road_status);
		// $('input[name="property_contact_number"]').val(
		// 	banner_data.property_contact_no
		// );
		// $('input[name="approx_banner_length"]').val(
		// 	banner_data.approx_banner_length
		// );
		// $('input[name="approx_banner_width"]').val(banner_data.approx_banner_width);
		// $('input[name="approx_banner_height"]').val(
		// 	banner_data.approx_banner_height
		// );
		// $("#remarks").val(banner_data.remarks);
		// $("#image_1").attr(
		// 	"src",
		// 	`http://103.233.79.142:90/images_banner/banner_images/${banner_data.banner_id}_BPWH.jpg`
		// );
		// $("#image_2").attr(
		// 	"src",
		// 	`http://103.233.79.142:90/images_banner/banner_images/${banner_data.banner_id}_EBWSP.jpg`
		// );
		// $("#image_3").attr(
		// 	"src",
		// 	`http://103.233.79.142:90/images_banner/banner_images/${banner_data.banner_id}_HP.jpg`
		// );
		// $("#image_4").attr(
		// 	"src",
		// 	`http://103.233.79.142:90/images_banner/banner_images/${banner_data.banner_id}_AP1.jpg`
		// );
		// $("#image_5").attr(
		// 	"src",
		// 	`http://103.233.79.142:90/images_banner/banner_images/${banner_data.banner_id}_AP2.jpg`
		// );
	} //end if
};

const calculateExistingArea = () => {
	const length = $(".exisiting__length").val();
	const breadth = $(".existing__breadth").val();
	if (parseFloat(length) > 0 && parseFloat(breadth) > 0) {
		const area = parseFloat(length) * parseFloat(breadth);
		$("#existing__area").val(area);
		const actual = $("#actual__area").val();
		if(parseFloat(actual) > 0){
		const diff = parseFloat(area) - parseFloat(actual);
		$('.diff__square__feet').val(diff)
		$('.additional__ratable__value').val(parseFloat(diff) * 222);
		}
		else{
			$('.diff__square__feet').val(0.00)
			$('.additional__ratable__value').val(0.00);

			$('.new__square__feet').val(area)
			$('.new__ratable__value__square__feet').val(parseFloat(area) * 222)
		}
		
	}
	else
	$("#existing__area").val(0.00);
};

//pmc
const calculateActualArea = () => {
	const length = $(".actual__length").val();
	const breadth = $(".actual__breadth").val();
	if (parseFloat(length) > 0 && parseFloat(breadth) > 0) {
		const area = parseFloat(length) * parseFloat(breadth);
		$("#actual__area").val(area);

		const existing = $("#existing__area").val();
		const diff = parseFloat(existing) - parseFloat(area);
		$('.diff__square__feet').val(diff)
		$('.additional__ratable__value').val(parseFloat(diff) * 222);

		
		$('.new__square__feet').val(0.00)
		$('.new__ratable__value__square__feet').val(0.00)

	}
	else{
		const exArea = $("#existing__area").val();
		$('.new__square__feet').val(exArea)
		$('.new__ratable__value__square__feet').val(parseFloat(exArea) * 222)



		$('.diff__square__feet').val(0.00)
		$('.additional__ratable__value').val(0.00);
		$("#actual__area").val(0.00);
		const value = $('.new__square__feet').val();

		
		$('.new__ratable__value__square__feet').val(parseFloat(value) * 222)
			}
};

$('.new__square__feet').on('keyup',(e) =>{
	const value = $('.new__square__feet').val();
	console.log(value);
	$('.new__ratable__value__square__feet').val(parseFloat(value) * 222)
})
// $('.diff__square__feet').on('keyup',(e) =>{
// 	const value = e.value;
// 	if(parseFloat(value) > 0)
// 		$('.additional__ratable__value').val(parseFloat(value) * 222);
	
// })




//deleteSignboardAdditionalData
const deleteSignboardAdditionalData = async (id) => {
	
	try {
		
			
	
		const banner_id = 	$('input[name="banner__id"]').val();

		const additional_area = 	$('input[name="total__additional__area"]').val();
		const additional_rateble = 	$('input[name="total__additional__rateble__value"]').val();
		const total_area = 	$('input[name="total__area"]').val();
		const total_rateble = 	$('input[name="total__rateble__value"]').val();
		const addSqFT = $('input[name="signboard__add__sqft[]"]').map(function () {
			return this.value;
		}).get();


		const count = addSqFT.length;
		




		const rawResponse = await fetch(site_url+'delete-signboard-data',{
			method:"POST",
			body:JSON.stringify({id,
				count,
				additional_area,
				additional_rateble,
				total_area,
				total_rateble,
				banner_id

			
			})
		});


		const obj = await rawResponse.json();
		const status = await obj.status;
		const msg = await obj.msg;
		
		if(status == 200){
			$.toast({
				heading: "Success",
				text: msg,
				position: "top-right",
				stack: false,
			});

			calculatedTotalAdditionalValue();
		}
	
	} catch (error) {
		// $.toast({
		// 	heading: "Error",
		// 	text: error,
		// 	position: "top-right",
		// 	stack: false,
		// });
	}

}



function fileExists(url) {
    if(url){
        var req = new XMLHttpRequest();
        req.open('GET', url, false);
        req.send();
        return req.status==200;
    } else {
        return false;
    }
}





const form1Data = async (banner_id) => {
	$("#exampleModal").modal("show");
	$("#image_1").attr("src", "");
	$("#image_2").attr("src", "");
	$("#image_3").attr("src", "");
	$("#image_4").attr("src", "");
	$("#image_5").attr("src", "");
	const rawResponse = await fetch(site_url + "get-banner-data", {
		method: "POST",
		body: JSON.stringify({ banner_id, formNo: 1 }),
	});
	const content = await rawResponse.json();
	const status = await content.status;
	const banner_data = await content.banner_data;
	const media_data = await content.media_data;
	const plate_data = await content.plate_data;
	const ward_data = await content.wardData;
	const banner_status = await content.bannerStatus;
	const installedOnData = await content.installedOnData;
	const boardNatureData = await content.boardNatureData;
	const oneHalfMeterRoadData = await content.oneHalfMeterRoadData;
	const signboardAdditional = await content.signboardAdditional;
	if (status === 200) {
		$("#media__type").empty();
		let  options= '';

		
$('textarea[name="advertisor__address"]').val(banner_data.advertiser_address)



		$('select[name="ward_name_id"]').empty();
		ward_data.map((wd)=>{
		
			if(banner_data.ward_name_id == wd.ward_name_id)
			options += `<option selected value="${wd.ward_name_id}">${wd.ward_name}</option>`;
			else
			options += `<option value="${wd.ward_name_id}">${wd.ward_name}</option>`;
			$('select[name="ward_name_id"]').append(options);
		})
		
		$('select[name="ward_name_id"]').select2();
		
		$("#plate__available").empty();
		$("#ward__name__id").empty();
		$("#banner__status__id").empty();
		$("#installed__on").empty();
		$("#board__nature").empty();
		$("#1_5_meater_from_road").empty();

		$(".banner_id_heading").text(`#${banner_data.banner_id}`);
		showHideSignBoard(banner_data.media_type_id)
		if (oneHalfMeterRoadData.length > 0) {
			for (let ion of oneHalfMeterRoadData) {
				if (
					banner_data.one_and_a_half_meters_from_road_id ==
					ion.one_and_a_half_meters_from_road_id
				)
					$("#1_5_meater_from_road").append(
						`<option selected value="${ion.one_and_a_half_meters_from_road_id}">${ion.one_and_a_half_meters_from_road_value}</option>`
					);
				else
					$("#1_5_meater_from_road").append(
						`<option  value="${ion.one_and_a_half_meters_from_road_id}">${ion.one_and_a_half_meters_from_road_value}</option>`
					);
			}
			if (
				banner_data.one_and_a_half_meters_from_road_id == null ||
				banner_data.one_and_a_half_meters_from_road_id == ""
			)
				$("#1_5_meater_from_road").prepend(
					`<option value="0" selected >Please select 1.5 meter from road </option>`
				);
		}

		if (boardNatureData.length > 0) {
			for (let ion of boardNatureData) {
				if (banner_data.board_nature_id == ion.board_nature_id)
					$("#board__nature").append(
						`<option selected value="${ion.board_nature_id}">${ion.board_nature_value}</option>`
					);
				else
					$("#board__nature").append(
						`<option  value="${ion.board_nature_id}">${ion.board_nature_value}</option>`
					);
			}
			if (
				banner_data.board_nature_id == null ||
				banner_data.board_nature_id == ""
			)
				$("#board__nature").prepend(
					`<option value="0" selected >Please select board nature </option>`
				);
		}

		if (installedOnData.length > 0) {
			for (let ion of installedOnData) {
				if (banner_data.installed_on_id == ion.installed_on_id)
					$("#installed__on").append(
						`<option selected value="${ion.installed_on_id}">${ion.installed_on_value}</option>`
					);
				else
					$("#installed__on").append(
						`<option  value="${ion.installed_on_id}">${ion.installed_on_value}</option>`
					);
			}
			if (
				banner_data.installed_on_id == null ||
				banner_data.installed_on_id == ""
			)
				$("#installed__on").prepend(
					`<option value="0" selected >Please select installed on </option>`
				);
		}

		if (media_data.length > 0) {
			for (let med of media_data) {
				if (banner_data.media_type_id == med.media_type_id)
					$("#media__type").append(
						`<option selected value="${med.media_type_id}">${med.media_type_value}</option>`
					);
				else
					$("#media__type").append(
						`<option  value="${med.media_type_id}">${med.media_type_value}</option>`
					);
			}
			if (banner_data.media_type_id == null || banner_data.media_type_id == "")
				$("#media__type").prepend(
					`<option value="0" selected >Please select media type</option>`
				);
		}
		//banner_status
		if (banner_status.length > 0) {
			for (let status of banner_status) {
				if (banner_data.banner_status_id == status.id)
					$("#banner__status__id").append(
						`<option selected value="${status.id}">${status.banner_status_value}</option>`
					);
				else
					$("#banner__status__id").append(
						`<option  value="${status.id}">${status.banner_status_value}</option>`
					);
			} //end for loop
			if (
				banner_data.banner_status_id == null ||
				banner_data.banner_status_id == ""
			)
				$("#banner__status__id").prepend(
					`<option value="0" selected >Please select banner status</option>`
				);
		} //end if'
		if (ward_data.length > 0) {
			for (let ward of ward_data) {
				if (banner_data.ward_name_id == ward.ward_name_id)
					$("#ward__name__id").append(
						`<option selected value="${ward.ward_name_id}">${ward.ward_name}</option>`
					);
				else
					$("#ward__name__id").append(
						`<option  value="${ward.ward_name_id}">${ward.ward_name}</option>`
					);
			} //end for loop
			if (banner_data.ward_name_id == null || banner_data.ward_name_id == "")
				$("#ward__name__id").prepend(
					`<option value="0" selected >Please select ward name</option>`
				);
		} //end if'
		if (plate_data.length > 0) {
			for (let plt of plate_data) {
				if (banner_data.plate_available_id == plt.plate_available_id)
					$("#plate__available").append(
						`<option selected value="${plt.plate_available_id}">${plt.plate_available_value}</option>`
					);
				else
					$("#plate__available").append(
						`<option  value="${plt.plate_available_id}">${plt.plate_available_value}</option>`
					);
			}
			if (
				banner_data.plate_available_id == null ||
				banner_data.plate_available_id == ""
			)
				$("#plate__available").prepend(
					`<option value="0" selected >Please select plate available</option>`
				);
		}
		$('input[name="banner__id"]').val(banner_data.banner_id);
		$('input[name="old__id"]').val(banner_data.old_id);
		
		$('input[name="property__name"]').val(banner_data.property_name);
		$('input[name="advertiser__name"]').val(banner_data.property_name);
		
		$('input[name="society__name"]').val(banner_data.society_name);
		$('input[name="landmark"]').val(banner_data.landmark);
		$("#address").val(banner_data.address);




		// $('.advertiser__name').val(banner_data.property_name);
		$('textarea[name="advertisor__address"]').val(banner_data.advertiser_address);
		// $('input[name="landmark"]').val(banner_data.landmark);
		// $("#address").val(banner_data.address);


		$('select[name="signboard__type__id"]').val(banner_data.signboard_type_id)
		$('select[name="legalization__possibility__id"]').val(banner_data.legalization_possibility_id)

		$('select').select2();


		$('.signboard__length__0').val(banner_data.sn_length)
		$('.signboard__breadth__0').val(banner_data.sn_breadth)
		$('.signboard__sqft__0').val(banner_data.sn_area)
		$('.signboard__additional_sqft__0').val(banner_data.sn_additional_sq_ft)
		$('.signboard__additional__rateble__value__0').val(banner_data.sn_additional_ratable_value)


		$('input[name="total__additional__area"]').val(banner_data.sn_total_area_of_additional_signboard);
		$('input[name="total__additional__rateble__value"]').val(banner_data.sn_total_rateable_value_of_additional_signboard);
		$('input[name="total__area"]').val(banner_data.sn_total_area_of_signboard);
		$('input[name="total__rateble__value"]').val(banner_data.sn_total_rateable_value_of_signboard);



		if(banner_data.media_type_id == 2){
			const image1Url = site_url+'images_banner/signboard_images/'+banner_data.banner_id+'_image1.jpg';
			const image1 = fileExists(image1Url);

			if(image1 == true)
			{
				$('.signboard__image1__upload').slideUp()
				$('.signboard__image1__preview').slideDown()
				$('#signboard__image1__preview').attr('src',image1Url)
			}

			const image2Url = site_url+'images_banner/signboard_images/'+banner_data.banner_id+'_image2.jpg';
			const image2 = fileExists(image1Url);

			if(image2 == true)
			{
				$('.signboard__image2__upload').slideUp()
				$('.signboard__image2__preview').slideDown();
				$('#signboard__image2__preview').attr('src',image2Url)

			}
// signboard__image2__upload
// signboard__image2__preview



		}
		let signboardCount = 1;
		if(signboardAdditional.length > 0)
		{
			$('input[name="total__additional__board__count"]').val(signboardAdditional.length)
			let imgUrl = '';
			console.log(signboardAdditional.length)
			signboardAdditional.map(item =>{
				let appendData = `

	<div class="row new__row"
	style="
	

		border: 1px solid #dbd1d1;
		padding: 1rem;
		margin-top: 1rem;
		border-radius: .2rem;

	
	"
	>
	<input type="hidden" value="${item.sign_board_id}"
	name="signboard__id[]"
	>

	<div class="col-md-2 ">
			<label for="">Length</label>
			<input value="${item.sn_length}" onkeyup="calculateAdditionalArea(${signboardCount})" type="number" class="signboard__additional__length__${signboardCount} form-control" placeholder="Length" name="signboard__additional__length[]">
		</div>

		<div class="col-md-2">
			<label for="">Breadth</label>
			<input  value="${item.sn_breadth}" onkeyup="calculateAdditionalArea(${signboardCount})" type="number" class="signboard__additional__breadth__${signboardCount} form-control" placeholder="Breadth" name="signboard__additional__breadth[]">
		</div>


		<div class="col-md-2">
			<label for="">Area (in SQ FT)</label>
			<input value="${item.sn_area}" readonly type="number" class="signboard__add__sqft__${signboardCount} form-control" placeholder="Signboard Area " name="signboard__add__sqft[]" readonly>
		</div>

	
		<div class="col-md-3">
			<label for="">Additional rateble value</label>
			<input value="${item.sn_additional_ratable_value}" type="number" class="signboard__add__additional__rateble__value__${signboardCount} form-control" placeholder="Additional rateble value" name="signboard__add__additional__rateble__value[]">
		</div>`;

		



		imgUrl = site_url+'images_banner/signboard_images/additional_images/'+item.sign_board_id+'_image1.jpg';
			let image1 = fileExists(imgUrl);
			if(image1 ==  true)
			{
				 appendData +=`	<div class="col-md-4">
				 <label for="">Additional Signboard Image 1</label>
				 <img src="${imgUrl}" alt="" onerror="this.src='https://via.placeholder.com/150'" class="form-control banner-img">
			 </div>`
			}
			else
			{
				 appendData +=`<div class="col-md-4">
				 <label for="">Additional Signboard Image 1</label>
				 <input class="form-control" type="file" name="signboard__additional__image1[]">
			 </div>`;
			}



		


			imgUrl = site_url+'images_banner/signboard_images/additional_images/'+item.sign_board_id+'_image2.jpg';
			let image2 = fileExists(imgUrl);
			if(image2 ==  true)
			{
				 appendData +=`	<div class="col-md-4">
				 <label for="">Additional Signboard Image 2</label>
				 <img src="${imgUrl}" alt="" onerror="this.src='https://via.placeholder.com/150'" class="form-control banner-img">
			 </div>`
			}
			else
			{
				 appendData +=`<div class="col-md-4">
				 <label for="">Additional Signboard Image 2</label>
				 <input class="form-control" type="file" name="signboard__additional__image2[]">
			 </div>`;
			}


		
	


			appendData +=`
		<div class="col-md-4" style="margin-top:28px">
		  
		   <a onclick="removeDiv(this)" data-signboard-id="${item.sign_board_id}" class="btn btn-sm btn-danger">Remove</a>
		</div>
	</div>
	
	`;
	signboardCount++;
	let newData = $(appendData);
	newData.hide();
	$('.signboard__div').append(newData)
	newData.slideDown();
	signboardCount++;
			})
		}

		// signboard__image1__preview
		// signboard__image2__preview

		// signboard__image1__upload
		// signboard__image2__upload





		$('input[name="proper_roead_name"]').val(banner_data.proper_road_name);
		$('input[name="road_status"]').val(banner_data.road_status);

		$('input[name="survey__location"]').val(
			banner_data.location_lat + "," + banner_data.location_lng
		);
		$('input[name="banner__location"]').val(
			banner_data.banner_location_lat + "," + banner_data.banner_location_lng
		);
		// $('input[name="plate__available"]').val(banner_data.abc);
		// $('input[name="installed__on"]').val(banner_data.abc);
		// $('input[name="banner__status__id"]').val(banner_data.abc);
		$('input[name="1_5_meater_from_road"]').val(
			banner_data.one_and_a_half_meters_from_road_id
		);
		// $('input[name="board__nature"]').val(banner_data.abc);
		$('input[name="banner__owner__name"]').val(banner_data.banner_owner_name);
		$('input[name="banner__contact__number"]').val(
			banner_data.banner_owner_contact_no
		);
		$('input[name="exisiting__length"]').val(banner_data.existing_length);
		$('input[name="existing__breadth"]').val(banner_data.existing_breadth);
		$('input[name="existing__area"]').val(banner_data.existing_area);
		$('input[name="actual__length"]').val(banner_data.actual_length);
		$('input[name="actual__breadth"]').val(banner_data.actual_breadth);
		$('input[name="actual__area"]').val(banner_data.actual_area);
		$('input[name="diff__square__feet"]').val(banner_data.difference_in_sqft);
		$('input[name="additional__ratable__value"]').val(
			banner_data.additional_ratable_value
		);
		$('input[name="new__square__feet"]').val(banner_data.new_square_feet);
		$('input[name="new__ratable__value__square__feet"]').val(
			banner_data.new_ratable_value_in_square_feet
		);
		$('input[name="property__number"]').val(banner_data.property_no);

		$('input[name="property_contact_number"]').val(
			banner_data.property_contact_no
		);
		$('input[name="approx_banner_length"]').val(
			banner_data.approx_banner_length
		);
		$('input[name="approx_banner_width"]').val(banner_data.approx_banner_width);
		$('input[name="approx_banner_height"]').val(
			banner_data.approx_banner_height
		);

		$("#remarks").val(banner_data.remarks);
		$("#image_1").attr(
			"src",
			`http://103.233.79.142:90/images_banner/banner_images/${banner_data.banner_id}_BPWH.jpg`
		);
		$("#image_2").attr(
			"src",
			`http://103.233.79.142:90/images_banner/banner_images/${banner_data.banner_id}_EBWSP.jpg`
		);
		$("#image_3").attr(
			"src",
			`http://103.233.79.142:90/images_banner/banner_images/${banner_data.banner_id}_HP.jpg`
		);
		$("#image_4").attr(
			"src",
			`http://103.233.79.142:90/images_banner/banner_images/${banner_data.banner_id}_AP1.jpg`
		);
		$("#image_5").attr(
			"src",
			`http://103.233.79.142:90/images_banner/banner_images/${banner_data.banner_id}_AP2.jpg`
		);
	} //end if
};




const floatSurveyGraph = async () => {
	const rawResponse = await fetch(site_url + "get-survey-data", {
		method: "GET",
	});
	const content = await rawResponse.json();
	const status = await content.status;
	const msg = await content.msg;
	const data = await content.data;
	// console.log(data);
	var areachartSalesColors = getChartColorsArray("survey-chart");
	areachartSalesColors &&
		((options = {
			series: [
				{
					name: "Hoarding",
					data: data.hoarding,
				},
				{
					name: "Signboard",
					data: data.signboard,
				},
			],
			chart: {
				type: "bar",
				height: 350,
			},
			plotOptions: {
				bar: {
					horizontal: false,
					columnWidth: "35%",
					endingShape: "rounded",
				},
			},
			dataLabels: {
				enabled: false,
			},
			stroke: {
				show: true,
				width: 2,
				colors: ["transparent"],
			},
			xaxis: {
				categories: data.dateArr,
			},
			yaxis: {
				title: {
					text: "Count",
				},
			},
			fill: {
				opacity: 1,
			},
			tooltip: {
				y: {
					formatter: function (val) {
						return val;
					},
				},
			},
		}),
		// ((options = {
		// 	series: data,
		// 	chart: {
		// 		type: "bar",
		// 		height: 341,
		// 		toolbar: {
		// 			show: !1,
		// 		},
		// 	},
		// 	plotOptions: {
		// 		bar: {
		// 			horizontal: !1,
		// 			columnWidth: "30%",
		// 		},
		// 	},
		// 	stroke: {
		// 		show: !0,
		// 		width: 3,
		// 		colors: ["transparent"],
		// 	},
		// 	xaxis: {
		// 		categories: [""],
		// 		axisTicks: {
		// 			show: !1,
		// 			borderType: "solid",
		// 			color: "#78909C",
		// 			height: 6,
		// 			offsetX: 0,
		// 		},
		// 		title: {
		// 			text: "last fifteen days Survey Count",
		// 			offsetX: 0,
		// 			style: {
		// 				color: "#78909C",
		// 				fontSize: "12px",
		// 				fontWeight: 400,
		// 			},
		// 		},
		// 	},
		// 	colors: areachartSalesColors,
		// }),
		(chart = new ApexCharts(
			document.querySelector("#survey-chart"),
			options
		)).render());
};
const mediaTypeGraph = async () => {
	const rawResponse = await fetch(site_url + "get-media-type-data", {
		method: "GET",
	});
	const content = await rawResponse.json();
	const status = await content.status;
	const msg = await content.msg;
	const data = await content.data;
	// console.log(data);
	var chartPieColors = getChartColorsArray("media-type-chart");
	chartPieColors &&
		((chartDom = document.getElementById("media-type-chart")),
		(myChart = echarts.init(chartDom)),
		(option = {
			tooltip: {
				trigger: "item",
			},
			legend: {
				orient: "vertical",
				left: "left",
				textStyle: {
					color: "#858d98",
				},
			},
			color: chartPieColors,
			series: [
				{
					name: "Media Type",
					type: "pie",
					radius: "50%",
					data: data,
					emphasis: {
						itemStyle: {
							shadowBlur: 10,
							shadowOffsetX: 0,
							shadowColor: "rgba(0, 0, 0, 0.5)",
						},
					},
				},
			],
			textStyle: {
				fontFamily: "Poppins, sans-serif",
			},
		}) && myChart.setOption(option));
};
const availablePlateData = async () => {
	// const rawResponse = await fetch(site_url + "/get-available-plate-data", {
	const rawResponse = await fetch(site_url + "get-existing-illegal-data", {
		method: "GET",
	});
	const content = await rawResponse.json();
	const status = await content.status;
	const msg = await content.msg;
	const data = await content.data;
	console.log(data);
	var chartDoughnutColors = getChartColorsArray("available-plate-data");
	chartDoughnutColors &&
		((chartDom = document.getElementById("available-plate-data")),
		(myChart = echarts.init(chartDom)),
		(option = {
			tooltip: {
				trigger: "item",
			},
			legend: {
				top: "5%",
				left: "center",
				textStyle: {
					color: "#858d98",
				},
			},
			color: chartDoughnutColors,
			series: [
				{
					name: "Banner Status",
					type: "pie",
					radius: ["40%", "70%"],
					avoidLabelOverlap: !1,
					label: {
						show: !1,
						position: "center",
					},
					emphasis: {
						label: {
							show: !0,
							fontSize: "16",
							fontWeight: "bold",
						},
					},
					labelLine: {
						show: !1,
					},
					data: [
						{
							name: "Existing Id Found(H)",
							value: data.existingCount_hoarding,
						},
						{
							name: "To Be Verified(H)",
							value: data.toBeVerifiedCount_hoarding,
						},
						{
							name: "Illegal(H)",
							value: data.illegalCount_hoarding,
						},
						{
							name: "Existing Id Found(S)",
							value: data.existingCount_signboard,
						},
						{
							name: "To Be Verified(S)",
							value: data.toBeVerifiedCount_signboard,
						},
						{
							name: "Illegal(S)",
							value: data.illegalCount_signboard,
						},
					],
				},
			],
			textStyle: {
				fontFamily: "Poppins, sans-serif",
			},
		}) && myChart.setOption(option));
};
//addNewInMaster
const addNewInMaster = (argV) => {
	const name = $(argV).attr("data-name");
	$(".new-data").append(
		`<input type="hidden" value="${name}" name="table-name">`
	);
	$("#masterAddModal").modal("show");
	console.log(name);
};
//addMore
const addMore = () => {
	const newRow = `
	<div class="row new__row">
	<div class="col-md-11">
		<div class="mb-1">
			<label>Value</label>
			<input type="text" name="value[]" class="form-control" placeholder="Enter Value">
		</div>
	</div>
	<div class="col-md-1" style="margin-top:6%">
		<div class="mb-1">
			<a class="btn btn-sm btn-danger" onclick="removeDiv(this)"> - </a>
		</div>
	</div>
	</div>
	`;
	const newData = $(newRow);
	newData.hide();
	$(".new-data").append(newData);
	newData.slideDown();
};
//removeDiv
const removeDiv = (argV) => {
	$(argV).parents(".new__row").slideUp();
	const signedId = $(argV).attr('data-signboard-id');
	setTimeout(() => {
		$(argV).parents(".new__row").remove();
		calculatedTotalAdditionalValue();
		deleteSignboardAdditionalData(signedId)
		
	}, 500);
};
$(".master-data-add-frm").validate({
	submitHandler: async (frm) => {
		try {
			const rawResponse = await fetch(frm.action, {
				method: frm.method,
				body: new FormData(frm),
			});
			const obj = await rawResponse.json();
			const msg = await obj.msg;
			$.toast({
				heading: "Success",
				text: msg,
				position: "top-right",
				stack: false,
			});
			$("#masterAddModal").modal("hide");
			setTimeout(() => {
				location.reload();
			}, 700);
		} catch (error) {
			$.toast({
				heading: "Error",
				text: error,
				position: "top-right",
				stack: false,
			});
		}
	},
});
//activateUser
const activateUser = async (argV) => {
	try {
		const table_name = $(argV).attr("data-table-name");
		const column = $(argV).attr("data-column-name");
		const id = $(argV).attr("data-id");
		const conf = confirm("Are you sure want to activate user ?");
		if (conf == true) {
			$("#loaderModal").modal("show", { backdrop: "static", keyboard: false });
			// return
			const rawResponse = await fetch(site_url + "activate-user", {
				method: "POST",
				body: JSON.stringify({
					table_name,
					column,
					id,
				}),
			});
			const obj = await rawResponse.json();
			const msg = await obj.msg;
			$.toast({
				heading: "Success",
				text: msg,
				position: "top-right",
				stack: false,
			});
			setTimeout(() => {
				$("#loaderModal").modal("hide", {
					backdrop: "static",
					keyboard: false,
				});
				getUserList();
			}, 700);
		}
	} catch (error) {
		$.toast({
			heading: "Error",
			text: error,
			position: "top-right",
			stack: false,
		});
	}
};
// /deleteMasterData
const deleteMasterData = async (argV) => {
	try {
		const table_name = $(argV).attr("data-table-name");
		const column = $(argV).attr("data-column-name");
		const id = $(argV).attr("data-id");
		const conf = confirm("Are you sure want to delete ?");
		if (conf == true) {
			$("#loaderModal").modal("show", { backdrop: "static", keyboard: false });
			// return
			const rawResponse = await fetch(site_url + "delete-data", {
				method: "POST",
				body: JSON.stringify({
					table_name,
					column,
					id,
				}),
			});
			const obj = await rawResponse.json();
			const msg = await obj.msg;
			$.toast({
				heading: "Success",
				text: msg,
				position: "top-right",
				stack: false,
			});
			setTimeout(() => {
				location.reload();
			}, 700);
		}
	} catch (error) {
		$.toast({
			heading: "Error",
			text: error,
			position: "top-right",
			stack: false,
		});
	}
};
const wardSummaries = async () => {
	try {
		$('#loaderModal').modal('show');
		const date_range = $(".ward__summary__date__range").val();
		const media_type_id = $('.media__type__id').val();

		const rawResponse = await fetch(
			site_url + "WardSummaryController/wardSummaries",
			{
				method: "POST",
				body: JSON.stringify({ date_range,media_type_id }),
			}
		);
		const obj = await rawResponse.json();
		const status = await obj.status;
		const data = await obj.data;
		const summary = await data.summary;
		const total = await data.total;
		const msg = await data.msg;
		const amount = await data.amount;
		if (status == 200) {
		
			$.toast({
				heading: "Success",
				text: msg,
				position: "top-right",
				stack: false,
			});
			// alert('werwer')
			// $('#loaderModal').hide();
			var table = $(".ward_summary_datatable").DataTable();
			table.clear().draw();
			table.destroy();
			summary.map((s) => {
				$(".ward_summary_datatable tbody").append(`
					<tr>
					<td>${s.ward_name}</td>
					<td>${s.surveyed_count}</td>
					<td>${s.illegal_count}</td>
					<td>${s.signing_progress}</td>
					<td>${s.notice_signed}</td>
					<td>${s.notice_delivered}</td>
					<td>${s.notice_to_be_delivered}</td>
					<td>${s.approched_sanction}</td>
					<td>${s.ready_for_action}</td>
					<td>${s.legalized}</td>
					<td>${s.demolished}</td>
					<td>${s.waiting_for_response}</td>
					</tr>
				`);
			});
			let totalCount = `<tr
			style="background: #000;
			color: white;
			font-weight: 700;"
			><th>Total</th>`;
			total.map((item) => {
				totalCount += `<th>${item}</th>`;
			});
			totalCount += "</tr>";
			$(".ward_summary_datatable tbody").append(totalCount);
			let aprxAmount = `<tr style="background: #297f99;
			color: white;
			font-weight: 700;"><th>Aprox Amount One Year</th>`;
			amount.map((item) => {
				aprxAmount += `<th>${item}</th>`;
			});
			aprxAmount += "</tr>";
			$(".ward_summary_datatable tbody").append(aprxAmount);
			tables = $(".ward_summary_datatable").DataTable({
				pageLength: 20,
				lengthMenu: [10, 25, 50, 100, 200, 500],
				processing: `<img src="${site_url}public/ajax-loader.gif"></img>`,
				scrollX: true,
				bSort: false,
				bDestroy: true,
				scrollY: "100vh",
				scrollCollapse: true,
				fixedColumns:   {
					left: 1,
				
				}
			});
		}
		setTimeout(() => {
			$('#loaderModal').modal('hide');
		}, 200);
		
	} catch (error) {
		$.toast({
			heading: "Error",
			text: error,
			position: "top-right",
			stack: false,
		});
	}
};


$('#media__type').on('change',(e)  =>{
	const mediaType = e.target.value;
	showHideSignBoard(mediaType)
})

const showHideSignBoard = (mediaType) =>{
	switch (mediaType) {
		case "1":
			$('.signboard__details').slideUp();	
			$('.hoarding__media__type').slideDown();
		break;

		case "2":
			$('.hoarding__media__type').slideUp();
			$('.signboard__details').slideDown();
		break;


		case "3":
			
		break;

	
	}
}

//addMoreSignboards
let signboardCount = 50;
const addMoreSignboards = () =>{
	let appendData = (`
	
	<div class="row new__row"
	
	style="
	

	border: 1px solid #dbd1d1;
	padding: 1rem;
	margin-top: 1rem;
	border-radius: .2rem;


"
	>
	<input type="hidden" 
	name="signboard__id[]"
	>
	<div class="col-md-2 ">
			<label for="">Length</label>
			<input onkeyup="calculateAdditionalArea(${signboardCount})" type="number" class="signboard__additional__length__${signboardCount} form-control" placeholder="Length" name="signboard__additional__length[]">
		</div>

		<div class="col-md-2">
			<label for="">Breadth</label>
			<input onkeyup="calculateAdditionalArea(${signboardCount})" type="number" class="signboard__additional__breadth__${signboardCount} form-control" placeholder="Breadth" name="signboard__additional__breadth[]">
		</div>


		<div class="col-md-2">
			<label for="">Area (in SQ FT)</label>
			<input readonly type="number" class="signboard__add__sqft__${signboardCount} form-control" placeholder="Signboard Area " name="signboard__add__sqft[]" readonly>
		</div>

	
		<div class="col-md-3">
			<label for="">Additional rateble value</label>
			<input type="number" class="signboard__add__additional__rateble__value__${signboardCount} form-control" placeholder="Additional rateble value" name="signboard__add__additional__rateble__value[]">
		</div>



		<div class="col-md-4">
			<label for="">Additional Signboard Image 1</label>
			<input class="form-control" type="file" name="signboard__additional__image1[]">
		</div>


		<div class="col-md-4">
			<label for="">Additional Signboard Image 2</label>
			<input class="form-control" type="file" name="signboard__additional__image2[]">
		</div>

		<div class="col-md-4" style="margin-top:28px">
		  
		   <a onclick="removeDiv(this)" class="btn btn-sm btn-danger">Remove</a>
		</div>
	</div>
	
	`);
	signboardCount++;
	let newData = $(appendData);
	newData.hide();
	$('.signboard__div').append(newData)
	newData.slideDown();
}

//calculateArea
const calculateArea = (count) =>{
	const len = $('.signboard__length__'+count).val();
	const bred = $('.signboard__breadth__'+count).val();
	const area = parseFloat(len) * parseFloat(bred);

	$('.signboard__additional_sqft__'+count).val('');
	$('.signboard__additional__rateble__value__'+count).val('');

	$('.signboard__sqft__'+count).val(area);
	let addSqft = 0;
	//if length >=3  
	if(parseFloat(len) >= 3)
	{
		addSqft = (parseFloat(len) - 3) * parseFloat(bred)
		$('.signboard__additional_sqft__'+count).val(addSqft);
		const ratebleValue = parseFloat(addSqft * 222).toFixed(2);
		$('.signboard__additional__rateble__value__'+count).val(ratebleValue);
	}

	calculatedTotalAdditionalValue()
}

//calculateAdditionalArea
const calculateAdditionalArea = async (count) =>{
	const len = $('.signboard__additional__length__'+count).val();
	const bred = $('.signboard__additional__breadth__'+count).val();
	const area = parseFloat(len) * parseFloat(bred);
	$('.signboard__add__sqft__'+count).val(area);
	const ratebleValue = parseFloat(area * 222).toFixed(2);
	$('.signboard__add__additional__rateble__value__'+count).val(ratebleValue);
	calculatedTotalAdditionalValue();
}

//calculatedTotalAdditionalValue
const calculatedTotalAdditionalValue = () =>{

	const addSqFT = $('input[name="signboard__add__sqft[]"]').map(function () {
		return this.value;
	}).get();
	const addSqFTNumArr = addSqFT.map(Number);
	let totalAdditionalArea = 0.00;
	totalAdditionalArea =  addSqFTNumArr.reduce((a,b) => a+b,0);


	const addRatebleVal = $('input[name="signboard__add__additional__rateble__value[]"]').map(function () {
		return this.value;
	}).get();
	const addRatebleValNum = addRatebleVal.map(Number);
	const totalAdditionalRatebleVal =  addRatebleValNum.reduce((a,b) => a+b,0);


	//actual value
	const signArea = $('input[name="signboard__sqft"').val();
	const signAdiotionalVal = $('input[name="signboard__additional_sqft"]').val();
	const acRatebleValue = $('input[name="signboard__additional__rateble__value"]').val();


	let acArea = 0;
	let totalRatebleVal = 0;
	if(parseFloat(signAdiotionalVal) > 0.00)
		acArea = signAdiotionalVal;
	else
		acArea = signArea;


	

	//totalArea
	const totalArea = parseFloat(acArea) + parseFloat(totalAdditionalArea);
	if(parseFloat(acRatebleValue) > 0.00)
		totalRatebleVal = parseFloat(acRatebleValue) + parseFloat(totalAdditionalRatebleVal)
	else
		totalRatebleVal = totalAdditionalRatebleVal;


		console.log(totalArea+'||'+totalRatebleVal)



		$('input[name="total__additional__area"]').val(totalAdditionalArea);
		$('input[name="total__additional__rateble__value"]').val(totalAdditionalRatebleVal);

		$('input[name="total__area"]').val(totalArea)
		$('input[name="total__rateble__value"]').val(totalRatebleVal)
		$('input[name="total__additional__board__count"]').val(addRatebleVal.length)





	// signboard__add__additional__rateble__value
}


const clearFormFields = (frm_name) => {
    $(":input", `#${frm_name}`)
        .not(":button, :submit")
        .val("")
        .prop("checked", false)
        .prop("selected", false);
    $(".occupancy_certificate").slideDown();
    $(".occupancy__certificate__preview").slideUp();
    $(".occupancy__certificate__preview_btn").slideUp();
    $(".other_related_certificate").slideDown();
    $(".other_related_certificate__preview").slideUp();
    $(".building_front_image").slideDown();
    $(".building_front_image__preview").slideUp();
    $(".building_left_image").slideDown();
    $(".building_left_image__preview").slideUp();
    $(".building_right_image").slideDown();
    $(".building_right_image__preview").slideUp();
    $(".building_sky_view_image").slideDown();
    $(".building_sky_view_image__preview").slideUp();
    $(":input", `.${frm_name}`)
        .not(":button, :submit")
        .val("")
        .prop("checked", false)
        .prop("selected", false);
    $(".occupancy_certificate").slideDown();
    $(".occupancy__certificate__preview").slideUp();
    $(".occupancy__certificate__preview_btn").slideUp();
    $(".other_related_certificate").slideDown();
    $(".other_related_certificate__preview").slideUp();
    $(".building_front_image").slideDown();
    $(".building_front_image__preview").slideUp();
    $(".building_left_image").slideDown();
    $(".building_left_image__preview").slideUp();
    $(".building_right_image").slideDown();
    $(".building_right_image__preview").slideUp();
    $(".building_sky_view_image").slideDown();
    $(".building_sky_view_image__preview").slideUp();
};


$('#banner_id').on('change',() =>{
	getBannerSurveyList();
})


