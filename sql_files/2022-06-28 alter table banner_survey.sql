alter table banner_survey 
add ward_name_id BIGINT DEFAULT NULL,
add FOREIGN KEY (ward_name_id) REFERENCES wards(ward_name_id)
add banner_status_id BIGINT,
add FOREIGN KEY (banner_status_id) REFERENCES banner_status(id) 


create table banner_status
(
    id SERIAL PRIMARY KEY ,
    banner_status_value VARCHAR(255),
    is_active int DEFAULT 1,
    created_at TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)

INSERT INTO banner_status (
    banner_status_value,
    created_at
  )
VALUES (
    'New Hoardings Indetified',
    '2022-06-28 18:54:55'
  ),
  (
    'Total Notice Signed',
    '2022-06-28 18:54:55'
  ),

  (
    'Signing In Progress',
    '2022-06-28 18:54:55'
  ),

  (
    'Notice delivered',
    '2022-06-28 18:54:55'
  ),


  (
    'Notice to be delivered',
    '2022-06-28 18:54:55'
  ),

  (
    'Approached for sanction',
    '2022-06-28 18:54:55'
  ),


  (
    'Ready For Action',
    '2022-06-28 18:54:55'
  ),

  (
    'Legalized',
    '2022-06-28 18:54:55'
  ),

  (
    'Demolished',
    '2022-06-28 18:54:55'
  );

