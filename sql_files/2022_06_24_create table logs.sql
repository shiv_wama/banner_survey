DROP TABLE IF EXISTS logs;
create table logs
(
id  serial primary key,
table_name VARCHAR(50),
old_data text,
new_data text,
updated_by BIGINT,
FOREIGN KEY (updated_by) REFERENCES banner_user(uid),
created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP

)