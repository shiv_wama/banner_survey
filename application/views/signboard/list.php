        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0">SignBoard Google Street List</h4>
                            </div>
                        </div>
                    </div>
                    <!-- end page title -->



                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <button type="button" class="btn btn-primary btn-sm" onclick="addBoard()" >Add New</button>
                                        </div>
                                        <!-- <div class="col-md-3">
                                            <div class="mb-1">
                                                <label for="">Date Range</label>

                                                <input type="text" name="date-range-filter" data-provider="flatpickr" data-date-format="Y-m-d" data-range-date="true" id="date-range-filter" class="form-control date_range_filter">
                                            </div>
                                        </div> -->
                                    </div>
                                </div>

                                <div class="card-body">
                                    <table class="list_datatable table nowrap align-middle" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Unique ID</th>
                                                <th>Employee</th>
                                                <th>Board Type</th>
                                                <th>Ward Name</th>
                                                <th>Road Name</th>
                                                <th>Property Type</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>



                                        </tbody>

                                        <tfoot>
                                            <tr>
                                                <th>Unique ID</th>
                                                <th>Employee</th>
                                                <th>Board Type</th>
                                                <th>Ward Name</th>
                                                <th>Road Name</th>
                                                <th>Property Type</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>