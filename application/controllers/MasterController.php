<?php
class MasterController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('MasterModel');
        $this->load->model('CommonModel');
        $this->user_id = isset($_SESSION['USER']['uid']) ?  $_SESSION['USER']['uid'] : 0;
        $this->user_type = isset($_SESSION['USER']['user_type']) ?  $_SESSION['USER']['user_type'] : 0;
        // echo $this->user_id;die;
        if ($this->user_id  ==  0)
            redirect('AuthController');
        if ($this->user_type != 'Admin') {
            json_encode(['status' => 401, 'msg' => "You don't have permission to access this page."]);
            die;
        }
    }
    /*
    Category
    Bannner Status 
    Board Nature
    Government Types
    Installed On
    Media Types
    Nature Of Property
    Notice Types
    One Half Meters From Road
    Owner Types
    Plate Available
    Wards 
    */

    public function getMasterData()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $id = base64_decode($data['id']);
        $column = $data['table_column_name'];
        $table_name = $data['table_name'];
        $data =  $this->CommonModel->getSingleData($table_name, [$column => $id], '');
        http_response_code(200);
        echo json_encode(['status' => http_response_code(), 'msg' => 'Success', 'data' => $data]);
        die;
    }

    //updateData
    public function updateData()
    {
        // echo '<pre>';
        // print_r($_POST);die;
        $column = $_POST['column_name'];
        $id = base64_decode($_POST[$column]);
        $table = $_POST['table_name'];
        $update_column = $_POST['update_column'];
        $value = $_POST[$update_column];
        $oldData = $this->CommonModel->getSingleData($table,[$column=>$id],NULL);
        $oldJsonData = json_encode($oldData);
        $res = $this->CommonModel->updateData($table, [$column => $id], [$update_column => $value]);
    
        

        if ($res) {
            $newData = $this->CommonModel->getSingleData($table, [$column => $id], NULL);
            $newJsonData = json_encode($newData);
            $insertArr = [

                'table_name'=> $table, 
                'old_data'=> $oldJsonData, 
                'new_data'=> $newJsonData, 
                'updated_by'=>$this->user_id, 
                'created_at'=>date('Y-m-d H:i:s')
            ];

            $res = $this->CommonModel->insertData('logs',$insertArr);
            http_response_code(200);
            echo json_encode(['status' => http_response_code(), 'msg' => 'success']);
            die;
        } else {

            http_response_code(400);
            echo json_encode(['status' => http_response_code(), 'msg' => 'Something went wrong.']);
            die;
        }
    }

    //store
    public function store()
    {

        $table_name = $_POST['table-name'];
        $data = $_POST['value'];
        switch ($table_name) {
            case 'wards':
                if (!empty($data)) {
                    $existArr = [];
                    for ($i = 0; $i < count($data); $i++) {
                        $lastId = $this->CommonModel->getLastId('wards', 'ward_name_id');
                        $checkExist = $this->CommonModel->getSingleData('wards', ['ward_name' => $data[$i]], '');
                        if (empty($checkExist))
                            $res = $this->CommonModel->insertData('wards', ['ward_name' => $data[$i], 'ward_name_id' => $lastId['ward_name_id'] + 1]);
                        else
                            array_push($existArr, $data[$i]);
                    }
                    $str = '';
                    if (!empty($existArr))
                        $str = implode(',', $existArr);
                    http_response_code(201);
                    if ($str != '')
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Data added , following data already exist ' . $str]);
                    else
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Success']);

                    die;
                }
                break;

            case 'categories':
                if (!empty($data)) {
                    $existArr = [];
                    for ($i = 0; $i < count($data); $i++) {
                        $lastId = $this->CommonModel->getLastId('category', 'category_id');
                        $checkExist = $this->CommonModel->getSingleData('category', ['category_value' => $data[$i]], '');
                        if (empty($checkExist))
                            $res = $this->CommonModel->insertData('category', ['category_value' => $data[$i], 'category_id' => $lastId['category_id'] + 1]);
                        else
                            array_push($existArr, $data[$i]);
                    }
                    $str = '';
                    if (!empty($existArr))
                        $str = implode(',', $existArr);
                    http_response_code(201);
                    if ($str != '')
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Data added , following data already exist ' . $str]);
                    else
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Success']);

                    die;
                }
                break;

            case "banner-status":
                if (!empty($data)) {
                    $existArr = [];
                    for ($i = 0; $i < count($data); $i++) {
                        $lastId = $this->CommonModel->getLastId('banner_status', 'id');
                        $checkExist = $this->CommonModel->getSingleData('banner_status', ['banner_status_value' => $data[$i]], '');
                        if (empty($checkExist))
                            $res = $this->CommonModel->insertData('banner_status', ['banner_status_value' => $data[$i]]);
                        else
                            array_push($existArr, $data[$i]);
                    }
                    $str = '';
                    if (!empty($existArr))
                        $str = implode(',', $existArr);
                    http_response_code(201);
                    if ($str != '')
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Data added , following data already exist ' . $str]);
                    else
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Success']);

                    die;
                }
                break;

            case "board-nature":
                if (!empty($data)) {
                    $existArr = [];
                    for ($i = 0; $i < count($data); $i++) {
                        $lastId = $this->CommonModel->getLastId('board_nature', 'board_nature_id');
                        $checkExist = $this->CommonModel->getSingleData('board_nature', ['board_nature_value' => $data[$i]], '');
                        if (empty($checkExist))
                            $res = $this->CommonModel->insertData('board_nature', ['board_nature_value' => $data[$i], 'board_nature_id' => $lastId['board_nature_id'] + 1]);
                        else
                            array_push($existArr, $data[$i]);
                    }
                    $str = '';
                    if (!empty($existArr))
                        $str = implode(',', $existArr);
                    http_response_code(201);
                    if ($str != '')
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Data added , following data already exist ' . $str]);
                    else
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Success']);

                    die;
                }
                break;

            case "government-type":
                if (!empty($data)) {
                    $existArr = [];
                    for ($i = 0; $i < count($data); $i++) {
                        $lastId = $this->CommonModel->getLastId('govt_type', 'govt_type_id');
                        $checkExist = $this->CommonModel->getSingleData('govt_type', [' ' => $data[$i]], '');
                        if (empty($checkExist))
                            $res = $this->CommonModel->insertData('govt_type', ['govt_type_value' => $data[$i], 'govt_type_id' => $lastId['govt_type_id'] + 1]);
                        else
                            array_push($existArr, $data[$i]);
                    }
                    $str = '';
                    if (!empty($existArr))
                        $str = implode(',', $existArr);
                    http_response_code(201);
                    if ($str != '')
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Data added , following data already exist ' . $str]);
                    else
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Success']);

                    die;
                }
                break;

            case "installed-on":
                if (!empty($data)) {
                    $existArr = [];
                    for ($i = 0; $i < count($data); $i++) {
                        $lastId = $this->CommonModel->getLastId('installed_on', 'installed_on_id');
                        $checkExist = $this->CommonModel->getSingleData('installed_on', ['installed_on_value' => $data[$i]], '');
                        if (empty($checkExist))
                            $res = $this->CommonModel->insertData('installed_on', ['installed_on_value' => $data[$i], 'installed_on_id' => $lastId['installed_on_id'] + 1]);
                        else
                            array_push($existArr, $data[$i]);
                    }
                    $str = '';
                    if (!empty($existArr))
                        $str = implode(',', $existArr);
                    http_response_code(201);
                    if ($str != '')
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Data added , following data already exist ' . $str]);
                    else
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Success']);

                    die;
                }
                break;

            case "media-type":
                if (!empty($data)) {
                    $existArr = [];
                    for ($i = 0; $i < count($data); $i++) {
                        $lastId = $this->CommonModel->getLastId('media_type', 'media_type_id');
                        $checkExist = $this->CommonModel->getSingleData('media_type', ['media_type_value' => $data[$i]], '');
                        if (empty($checkExist))
                            $res = $this->CommonModel->insertData('media_type', ['media_type_value' => $data[$i], 'media_type_id' => $lastId['media_type_id'] + 1]);
                        else
                            array_push($existArr, $data[$i]);
                    }
                    $str = '';
                    if (!empty($existArr))
                        $str = implode(',', $existArr);
                    http_response_code(201);
                    if ($str != '')
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Data added , following data already exist ' . $str]);
                    else
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Success']);

                    die;
                }
                break;
            case "user-type":
                if (!empty($data)) {
                    $existArr = [];
                    for ($i = 0; $i < count($data); $i++) {
                        // $lastId = $this->CommonModel->getLastId('user_roles', 'role_type');
                        $checkExist = $this->CommonModel->getSingleData('user_roles', ['user_role' => $data[$i]], '');
                        if (empty($checkExist))
                            $res = $this->CommonModel->insertData('user_roles', ['user_role' => $data[$i], 'role_type' => str_replace(" ","_",strtolower($data[$i]))]);
                        else
                            array_push($existArr, $data[$i]);
                    }
                    $str = '';
                    if (!empty($existArr))
                        $str = implode(',', $existArr);
                    http_response_code(201);
                    if ($str != '')
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Data added , following data already exist ' . $str]);
                    else
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Success']);

                    die;
                }
                break;

            case "nature-of-property":
                if (!empty($data)) {
                    $existArr = [];
                    for ($i = 0; $i < count($data); $i++) {
                        $lastId = $this->CommonModel->getLastId('nature_of_property', 'nature_of_property_id');
                        $checkExist = $this->CommonModel->getSingleData('nature_of_property', ['nature_of_property_value' => $data[$i]], '');
                        if (empty($checkExist))
                            $res = $this->CommonModel->insertData('nature_of_property', ['nature_of_property_value' => $data[$i], 'nature_of_property_id' => $lastId['nature_of_property_id'] + 1]);
                        else
                            array_push($existArr, $data[$i]);
                    }
                    $str = '';
                    if (!empty($existArr))
                        $str = implode(',', $existArr);
                    http_response_code(201);
                    if ($str != '')
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Data added , following data already exist ' . $str]);
                    else
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Success']);

                    die;
                }
                break;

            case "notice-type":
                if (!empty($data)) {
                    $existArr = [];
                    for ($i = 0; $i < count($data); $i++) {
                        $lastId = $this->CommonModel->getLastId('notice_type', 'notice_type_id');
                        $checkExist = $this->CommonModel->getSingleData('notice_type', ['notice_type_value' => $data[$i]], '');
                        if (empty($checkExist))
                            $res = $this->CommonModel->insertData('notice_type', ['notice_type_value' => $data[$i], 'notice_type_id' => $lastId['notice_type_id'] + 1]);
                        else
                            array_push($existArr, $data[$i]);
                    }
                    $str = '';
                    if (!empty($existArr))
                        $str = implode(',', $existArr);
                    http_response_code(201);
                    if ($str != '')
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Data added , following data already exist ' . $str]);
                    else
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Success']);

                    die;
                }
                break;

            case "one-half-meter":
                if (!empty($data)) {
                    $existArr = [];
                    for ($i = 0; $i < count($data); $i++) {
                        $lastId = $this->CommonModel->getLastId('one_half_meters_from_road', 'one_and_a_half_meters_from_road_id');
                        $checkExist = $this->CommonModel->getSingleData('one_half_meters_from_road', ['one_and_a_half_meters_from_road_value' => $data[$i]], '');
                        if (empty($checkExist))
                            $res = $this->CommonModel->insertData('one_half_meters_from_road', ['one_and_a_half_meters_from_road_value' => $data[$i], 'one_and_a_half_meters_from_road_id' => $lastId['one_and_a_half_meters_from_road_id'] + 1]);
                        else
                            array_push($existArr, $data[$i]);
                    }
                    $str = '';
                    if (!empty($existArr))
                        $str = implode(',', $existArr);
                    http_response_code(201);
                    if ($str != '')
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Data added , following data already exist ' . $str]);
                    else
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Success']);

                    die;
                }
                break;

            case "owner-type":
                if (!empty($data)) {
                    $existArr = [];
                    for ($i = 0; $i < count($data); $i++) {
                        $lastId = $this->CommonModel->getLastId('owner_type', 'owner_type_id');
                        $checkExist = $this->CommonModel->getSingleData('owner_type', ['owner_type_value' => $data[$i]], '');
                        if (empty($checkExist))
                            $res = $this->CommonModel->insertData('owner_type', ['owner_type_value' => $data[$i], 'owner_type_id' => $lastId['owner_type_id'] + 1]);
                        else
                            array_push($existArr, $data[$i]);
                    }
                    $str = '';
                    if (!empty($existArr))
                        $str = implode(',', $existArr);
                    http_response_code(201);
                    if ($str != '')
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Data added , following data already exist ' . $str]);
                    else
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Success']);

                    die;
                }
                break;

            case "plate-available":
                if (!empty($data)) {
                    $existArr = [];
                    for ($i = 0; $i < count($data); $i++) {
                        $lastId = $this->CommonModel->getLastId('plate_available', 'plate_available_id');
                        $checkExist = $this->CommonModel->getSingleData('plate_available', ['plate_available_value' => $data[$i]], '');
                        if (empty($checkExist))
                            $res = $this->CommonModel->insertData('plate_available', ['plate_available_value' => $data[$i], 'plate_available_id' => $lastId['plate_available_id'] + 1]);
                        else
                            array_push($existArr, $data[$i]);
                    }
                    $str = '';
                    if (!empty($existArr))
                        $str = implode(',', $existArr);
                    http_response_code(201);
                    if ($str != '')
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Data added , following data already exist ' . $str]);
                    else
                        echo json_encode(['status' => http_response_code(), 'msg' => 'Success']);

                    die;
                }
                break;
        } //end switch

    }

    // /deleteData
    public function deleteData()
    {
        $data = json_decode(file_get_contents("php://input"),true);
        $tableName = $data['table_name'];
        $column = $data['column'];
        $id = base64_decode($data['id']);
        $tableData = $this->CommonModel->getSingleData($tableName,[$column=>$id],NULL);
        $oldJsonData = json_encode($tableData); 
        if($tableName!='banner_user')
        $res = $this->CommonModel->deleteData($tableName,[$column=>$id]);
        else
        $res = $this->CommonModel->updateData($tableName, [$column => $id],['enabled'=>0]);
        //deleted log
        $insertArr =[
            'table_name'=> $tableName, 
            'old_data'=> $oldJsonData, 
            'new_data'=>'data deleted', 
            'updated_by'=>$this->user_id, 
            'created_at'=>date('Y-m-d H:i:s')
        ];
        $this->CommonModel->insertData('logs',$insertArr);
        http_response_code(200);
        echo json_encode(['status'=>http_response_code(),'msg'=>'Success']);
        die;
    }

    public function editMaster()
    {
        $type = $this->uri->segment(2);
        switch ($type) {
            case 'categories':
                $page_data['title'] = "Categories | Banner Status";
                $page_data['tbody_data'] = $this->CommonModel->getMultipleData('category', NULL, ['category_id', 'category_value']);
                $page_data['type'] = 'category';
                $page_data['data'] = '<thead>
                    <tr>
                    <th>Category Name</th>
                    <th>Action</th>
                    </tr>
                    </thead>';
                $page_data['page_title'] = 'Categories List';
                break;

            case 'banner-status':
                $page_data['title'] = "Banner Status | Banner Status";
                $page_data['tbody_data'] = $this->CommonModel->getMultipleData('banner_status', ['is_active' => 1], ['id', 'banner_status_value','banner_marker']);

                $page_data['type'] = 'banner_status';
                $page_data['data'] = '<thead>
                    <tr>
                    <th>#</th>
                    <th>Banner Status</th>
                    <th>Marker</th>
                    <th>Action</th>
                    </tr>
                    </thead>';
                $page_data['page_title'] = 'Banner Status List';
                break;

            case 'board-nature':
                $page_data['title'] = "Board Nature | Banner Status";
                $page_data['tbody_data'] = $this->CommonModel->getMultipleData('board_nature', NULL, ['board_nature_id', 'board_nature_value']);

                $page_data['type'] = 'board_nature';
                $page_data['data'] = '<thead>
                    <tr>
                    <th>Board Nature</th>
                    <th>Action</th>
                    </tr>
                    </thead>';
                $page_data['page_title'] = 'Board Nature List';
                break;

            case 'government-type':
                $page_data['title'] = "Government Types | Banner Status";
                $page_data['tbody_data'] = $this->CommonModel->getMultipleData('govt_type', NULL, ['govt_type_id', 'govt_type_value']);

                $page_data['type'] = 'govt_type';
                $page_data['data'] = '<thead>
                    <tr>
                    <th>Government Type</th>
                    <th>Action</th>
                    </tr>
                    </thead>';
                $page_data['page_title'] = 'Government Type List';
                break;

            case 'installed-on':
                $page_data['title'] = "Installed On | Banner Status";
                $page_data['tbody_data'] = $this->CommonModel->getMultipleData('installed_on', NULL, ['installed_on_id', 'installed_on_value']);

                $page_data['type'] = 'installed_on';
                $page_data['data'] = '<thead>
                    <tr>
                    <th>Installed On Value</th>
                    <th>Action</th>
                    </tr>
                    </thead>';
                $page_data['page_title'] = 'Installed On List';
                break;

            case 'media-type':
                $page_data['title'] = "Media Type | Banner Status";
                $page_data['tbody_data'] = $this->CommonModel->getMultipleData('media_type', NULL, ['media_type_id', 'media_type_value']);

                $page_data['type'] = 'media_type';
                $page_data['data'] = '<thead>
                    <tr>
                    <th>Media Type</th>
                    <th>Action</th>
                    </tr>
                    </thead>';
                $page_data['page_title'] = 'Media Type List';
                break;

            case 'user-type':
                $page_data['title'] = "User Type | Banner Status";
                $page_data['tbody_data'] = $this->CommonModel->getMultipleData('user_roles', NULL, ['role_type', 'user_role']);
                // print_r($page_data['tbody_data']);
                // die;
                $page_data['type'] = 'user_type';
                $page_data['data'] = '<thead>
                    <tr>
                    <th>User Type</th>
                    <th>Action</th>
                    </tr>
                    </thead>';
                $page_data['page_title'] = 'User Type List';
                break;

            case 'nature-of-property':
                $page_data['title'] = "Nature Of Property | Banner Status";
                $page_data['tbody_data'] = $this->CommonModel->getMultipleData('nature_of_property', NULL, ['nature_of_property_id', 'nature_of_property_value']);

                $page_data['type'] = 'nature_of_property';
                $page_data['data'] = '<thead>
                    <tr>
                    <th>Nature Of Property</th>
                    <th>Action</th>
                    </tr>
                    </thead>';
                $page_data['page_title'] = 'Nature Of Property List';
                break;

            case 'notice-type':
                $page_data['title'] = "Notice Type | Banner Status";
                $page_data['tbody_data'] = $this->CommonModel->getMultipleData('notice_type', NULL, ['notice_type_id', 'notice_type_value']);

                $page_data['type'] = 'notice_type';
                $page_data['data'] = '<thead>
                    <tr>
                    <th>Notice Type</th>
                    <th>Action</th>
                    </tr>
                    </thead>';
                $page_data['page_title'] = 'Notice Type List';
                break;

            case 'one-half-meter':
                $page_data['title'] = "One Half Meter From Road | Banner Status";
                $page_data['tbody_data'] = $this->CommonModel->getMultipleData('one_half_meters_from_road', NULL, ['one_and_a_half_meters_from_road_id', 'one_and_a_half_meters_from_road_value    ']);

                $page_data['type'] = 'one_half_meters_from_road';
                $page_data['data'] = '<thead>
                    <tr>
                    <th>One And Half Meter From Road</th>
                    <th>Action</th>
                    </tr>
                    </thead>';
                $page_data['page_title'] = 'One And A Half Meter From Road List';

                break;

            case 'owner-type':
                $page_data['title'] = "Owner Type | Banner Status";
                $page_data['tbody_data'] = $this->CommonModel->getMultipleData('owner_type', NULL, ['owner_type_id', 'owner_type_value']);

                $page_data['type'] = 'owner_type';
                $page_data['data'] = '<thead>
                    <tr>
                    <th>Owner Type</th>
                    <th>Action</th>
                    </tr>
                    </thead>';
                $page_data['page_title'] = 'Owner Type List';
                break;

            case 'plate-available':
                $page_data['title'] = "Plate Available | Banner Status";
                $page_data['tbody_data'] = $this->CommonModel->getMultipleData('plate_available', NULL, ['plate_available_id', 'plate_available_value']);

                $page_data['type'] = 'plate_available';
                $page_data['data'] = '<thead>
                    <tr>
                    <th>Plate Available</th>
                    <th>Action</th>
                    </tr>
                    </thead>';
                $page_data['page_title'] = 'Plate Available List';
                break;

            case 'wards':
                $page_data['title'] = "Wards | Banner Status";
                $page_data['tbody_data'] = $this->CommonModel->getMultipleData('wards', NULL, ['ward_name_id', 'ward_name']);

                $page_data['type'] = 'wards';
                $page_data['data'] = '<thead>
                    <tr>
					<th>Ward Id</th>
                    <th>Ward Name</th>
                    <th>Action</th>
                    </tr>
                    </thead>';
                $page_data['page_title'] = 'Ward List';
                break;
        }

        $this->load->view('includes/header', $page_data);
        $this->load->view('master/manage_master', $page_data);
        $this->load->view('includes/footer');
    }


    //updateMapMarker
    public function updateMapMarker()
    {
        $data = json_decode(file_get_contents("php://input"),true);
        $id = $data['id'];
        $value =$data['value'];

        $statusData = $this->CommonModel->getSingleData('banner_status',['id'=>$id],['id']);
        if(empty($statusData))
        {
            http_response_code(400);
            echo json_encode(['status'=>http_response_code(),'msg'=>'Banner status does not exist']);
            die;
        }

        $res = $this->CommonModel->updateData('banner_status',['id'=>$id],['banner_marker'=>$value]);
        if($res)
            {
                http_response_code(200);
                echo json_encode(['status'=>http_response_code(),'msg'=>'Success']);
                die;
            }
        else
        {

            http_response_code(400);
            echo json_encode(['status'=>http_response_code(),'msg'=>'Something went wrong, please try again.']);
            die;
        }
        
    }
   
}
