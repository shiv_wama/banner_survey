<?php
class MapController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->user_id = isset($_SESSION['USER']['uid']) ?  $_SESSION['USER']['uid'] : 0;
        $this->user_type = isset($_SESSION['USER']['user_type']) ?  $_SESSION['USER']['user_type'] : 0;
        // echo $this->user_id;die;
        $this->load->model('CommonModel');
        if ($this->user_id  ==  0)
            redirect('AuthController');
    }
    public function index()
    {
        $page_data['title'] = 'Map | Banner Survey';
        $page_data['banner_type'] = $this->CommonModel->getMultipleData('media_type', NULL, ['media_type_id', 'media_type_value']);
        $page_data['banner_id'] = $this->CommonModel->getMultipleData('banner_survey', NULL, ['banner_id']);
        $this->load->view('includes/header');
        $this->load->view('map/map', $page_data);
        $this->load->view('includes/footer');
    }
    //getMapLatLong
    public function getMapLatLong()
    {
        $res = [];
        $data = $this->SurveyModel->getLatLongForMap();
        foreach ($data as $key => $value) {
            $res[] = [
                $value['media_type_value'] . ' | ' . $value['address'],
                (float)$value['banner_location_lat'],
                (float)$value['banner_location_lng'],
                $value['banner_status_value'],
                $value['banner_marker'],
                $value['banner_id']
            ];
        }
        //    if(!empty($data))
        //    {
        //     $currArr = array();
        //     $i = 0;
        //     foreach ($data as $key => $value) {
        //             array_push($currArr,$value['address']);
        //             array_push($currArr, $value['banner_location_lat']);
        //             array_push($currArr, $value['banner_location_lng']);
        //             if($i == 1)
        //             {
        //                 $str = implode(",",$currArr);
        //                 $res[] = $str;
        //                 $currArr = array();
        //                 $str = "";
        //                 $i = 0;
        //             //   echo $str;die;
        //             }
        //             // echo $i . '<br>';
        //             $i++;
        //     }
        //         // echo '<pre>';
        //         // print_r($res);
        //         // die;
        // }
        http_response_code(200);
        // echo $res;die;
        echo json_encode(['status' => http_response_code(), 'msg' => 'success', 'data' => $res]);
        die;
    }
    //getMarkerFormOneData
    public function getMarkerFormOneData()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $lat = $data['lat'];
        $long = $data['long'];
        if ($lat != '' && $long != '') {
            $banner_form_one_data = $this->SurveyModel->getFormOneData($lat, $long);
            // echo '<pre>';
            // print_r($banner_form_one_data);die;
            http_response_code(200);
            echo json_encode(['status' => http_response_code(), 'msg' => 'Success', 'data' => $banner_form_one_data]);
            die;
        } else {
            http_response_code(400);
            echo json_encode(['status' => http_response_code(), 'msg' => 'Required data missing.']);
            die;
        }
    }
    //updateMapLatLong
    public function updateMapLatLong()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $id = $data['id'];
        $lat = $data['lat'];
        $long = $data['long'];
        $old_data = $this->CommonModel->getSingleData('banner_survey', ['banner_id' => $id], '');
        $updateArr = [
            'banner_location_lat' => $lat,
            'banner_location_lng' => $long
        ];
        $res = $this->CommonModel->updateData('banner_survey', ['banner_id' => $id], $updateArr);
        $new_data = $this->CommonModel->getSingleData('banner_survey', ['banner_id' => $id], '');

        $logArr = [

            'table_name' => 'banner_survey',
            'old_data' => json_encode($old_data),
            'new_data' => json_encode($new_data),
            'updated_by' => $this->user_id,
            'created_at' => date('Y-m-d H:i:s')
        ];
        $res = $this->CommonModel->insertData('logs', $logArr);

        if ($res) {
            http_response_code(200);
            echo json_encode(['status' => http_response_code(), 'msg' => 'Success']);
            die;
        } else {
            http_response_code(400);
            echo json_encode(['status' => http_response_code(), 'msg' => 'Something went wrong.']);
            die;
        }
        // echo '<pre>';
        // print_r($data);die;
    }
    //getMerkerDataByID
    public function getMerkerDataByID()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $id = $data['id'];
        $marker_data = $this->CommonModel->getSingleData('banner_survey', ['banner_id' => $id], ['banner_location_lat', 'banner_location_lng']);
        http_response_code(200);
        echo json_encode(['status' => http_response_code(), 'msg' => 'Success', 'data' => $marker_data]);
        die;
    }

    // /filterMapData
    public function filterMapData()
    {
        $data = json_decode(file_get_contents('php://input'), true);

        $banner_id = $data['banner_id'];
        $media_id = $data['media_id'];
   
                    $res = [];
                    $data = $this->SurveyModel->getLatLongForMap($media_id, $banner_id);
                    foreach ($data as $key => $value) {
                        $res[] = [
                            $value['media_type_value'] . ' | ' . $value['address'],
                            (float)$value['banner_location_lat'],
                            (float)$value['banner_location_lng'],
                            $value['banner_status_value'],
                            $value['banner_marker'],
                            $value['banner_id']
                        ];
                    }
                    http_response_code(200);
                    // echo $res;die;
                    echo json_encode(['status' => http_response_code(), 'msg' => 'success', 'data' => $res]);
                    die;
   
              
        
    }
}
