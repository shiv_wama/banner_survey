<!doctype html>
<html lang="en" data-layout="vertical" data-topbar="light" data-sidebar="dark" data-sidebar-size="sm-hover" data-sidebar-image="none">

<head>

    <meta charset="utf-8" />
    <title>Sign In | Banner Survey </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- Layout config Js -->
    <script src="<?= site_url() ?>public/assets/js/layout.js"></script>
    <!-- Bootstrap Css -->
    <link href="<?= site_url() ?>public/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="<?= site_url() ?>public/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="<?= site_url() ?>public/assets/css/app.min.css" rel="stylesheet" type="text/css" />
    <!-- custom Css-->
    <link href="<?= site_url() ?>public/assets/css/custom.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" integrity="sha512-wJgJNTBBkLit7ymC6vvzM1EcSWeM9mmOu+1USHaRBbHkm6W9EgM0HY27+UtUaprntaYQJF75rc8gjxllKs5OIQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

</head>

<body>

    <div class="auth-page-wrapper pt-5">
        <!-- auth page bg -->
        <div class="auth-one-bg-position auth-one-bg" id="auth-particles">
            <div class="bg-overlay"></div>

            <div class="shape">
                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1440 120">
                    <path d="M 0,36 C 144,53.6 432,123.2 720,124 C 1008,124.8 1296,56.8 1440,40L1440 140L0 140z"></path>
                </svg>
            </div>
        </div>

        <!-- auth page content -->
        <div class="auth-page-content">
            <div class="container">
                <!-- end row -->
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card mt-4">

                            <div class="card-body p-4">
                                <div class="text-center mt-2">
                                    <h5 class="text-primary">Welcome Back !</h5>
                                    <!-- <p class="text-muted">Sign in to continue   to Velzon.</p> -->
                                </div>
                                <div class="p-2 mt-4">
                                    <form action="<?= site_url('sign-in') ?>" method="POST" class="signin-frm" onsubmit="return false">

                                        <div class="mb-3">
                                            <label for="username" class="form-label">Username</label>
                                            <input type="text" class="form-control" name="user_name" id="username" placeholder="Enter username">
                                        </div>

                                        <div class="mb-3">
                                           
                                            <label class="form-label" for="password-input">Password</label>
                                            <div class="position-relative auth-pass-inputgroup mb-3">
                                                <input type="password" class="form-control pe-5" name="password" placeholder="Enter password" id="password-input">
                                                <a onclick="togglePassIcon(this)" class="btn btn-link position-absolute end-0 top-0 text-decoration-none text-muted"><i id="pass-icon" class="ri-eye-fill align-middle"></i></a>
                                            </div>
                                        </div>

                                        <div class="mt-4">
                                            <!-- <a href="<?= site_url('dashboard') ?>" class="btn btn-success w-100" > Sign In </a> -->
                                            <button class="btn btn-success w-100" type="submit">Sign In</button>
                                        </div>

                                      
                                    </form>
                                </div>
                            </div>
                            <!-- end card body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end auth page content -->

        <!-- footer -->
        <footer class="footer start-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <p class="mb-0 text-muted">&copy;
                                <script>
                                    document.write(new Date().getFullYear())
                                </script> Saar IT
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->
    </div>
    <!-- end auth-page-wrapper -->

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <!-- JAVASCRIPT -->
    <script src="<?= site_url() ?>public/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= site_url() ?>public/assets/libs/simplebar/simplebar.min.js"></script>
    <script src="<?= site_url() ?>public/assets/libs/node-waves/waves.min.js"></script>
    <script src="<?= site_url() ?>public/assets/libs/feather-icons/feather.min.js"></script>
    <script src="<?= site_url() ?>public/assets/js/pages/plugins/lord-icon-2.1.0.js"></script>
    <script src="<?= site_url() ?>public/assets/js/plugins.js"></script>
    <!-- particles js -->
    <script src="<?= site_url() ?>public/assets/libs/particles.js/particles.js"></script>
    <!-- particles app js -->
    <script src="<?= site_url() ?>public/assets/js/pages/particles.app.js"></script>
    <!-- password-addon init -->
    <script src="<?= site_url() ?>public/assets/js/pages/password-addon.init.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/jquery.validate.min.js" integrity="sha512-FOhq9HThdn7ltbK8abmGn60A/EMtEzIzv1rvuh+DqzJtSGq8BRdEN0U+j0iKEIffiw/yEtVuladk6rsG4X6Uqg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js" integrity="sha512-zlWWyZq71UMApAjih4WkaRpikgY9Bz1oXIW5G0fED4vk14JjGlQ1UmkGM392jEULP8jbNMiwLWdM8Z87Hu88Fw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
        //togglePassIcon
        const togglePassIcon = (argV) => {
            // $(argV).closest('pass-icon').removeClass();
            $(argV).closest('.list_product').remove();
            console.log('abc')
        }
        $('.signin-frm').validate({
            rules: {
                user_name: 'required',
                password: {
                    required: true,
                    // minlength: 5,
                    // maxlength: 25
                }
            },
            submitHandler: async (frm) => {
                try {
                    const formData = $('.signin-frm').serialize();

                    const rawResponse = await fetch(frm.action, {
                        method: frm.method,

                        body: new FormData(frm)
                    });
                    const content = await rawResponse.json();
                    const status = await content.status;
                    const msg = await content.msg
                    const redirect_url = await content.redirect_url;
                    // console.log(content);return
                    if (status === 200) {
                        $.toast({
                            heading: 'Success',
                            text: msg,
                            position: 'top-right',
                            stack: false
                        })

                        setTimeout(() => {
                            window.location.href = `<?= site_url() ?>${redirect_url}`
                        }, 1500);
                    } else {
                        $.toast({
                            heading: 'Error',
                            text: msg,
                            position: 'top-right',
                            stack: false
                        })
                    }



                } catch (error) {
                    $.toast({
                        heading: 'Error',
                        text: error,
                        position: 'top-right',
                        stack: false
                    })
                }



            },
        });
    </script>
</body>


</html>