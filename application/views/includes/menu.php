<div class="app-menu navbar-menu">
    <!-- LOGO -->
    <div class="navbar-brand-box">
        <!-- Dark Logo-->
        <a href="<?= site_url('dashboard') ?>" class="logo logo-dark">
            <span class="logo-sm">
                <!-- <h2>B.S.</h2> -->
                <img src="<?= site_url() ?>public/Banner_icon_small.png" alt="" height="22" class="small_logo">
                <!-- <img src="<?= site_url() ?>public/assets/images/logo-sm.png" alt="" height="22"> -->
            </span>
            <span class="logo-lg">

                <!-- <h2>Banner Survey</h2> -->
                <img src="<?= site_url() ?>public/Banner_Survey.png" alt="" height="22" class="main_logo">
            </span>
        </a>
        <!-- Light Logo-->
        <a href="<?= site_url('dashboard') ?>" class="logo logo-light">
            <span class="logo-sm">
                <!-- <h2>B.S.</h2> -->
                <img src="<?= site_url() ?>public/Banner_icon_small.png" alt="" height="22" class="small_logo">

                <!-- <img src="<?= site_url() ?>public/assets/images/logo-sm.png" alt="" height="22"> -->
            </span>
            <span class="logo-lg">

                <!-- <h2>Banner Survey</h2> -->
                <img src="<?= site_url() ?>public/Banner_Survey.png" alt="" height="22" class="main_logo">
            </span>
        </a>
        <button type="button" class="btn btn-sm p-0 fs-20 header-item float-end btn-vertical-sm-hover" id="vertical-hover">
            <i class="ri-record-circle-line"></i>
        </button>
    </div>

    <div id="scrollbar">
        <div class="container-fluid">

            <div id="two-column-menu">
            </div>
            <ul class="navbar-nav" id="navbar-nav">
                <?php
                $user_type = $_SESSION['USER']['user_type'];
                if ($user_type == 'Admin' || $user_type == 'admin' || $user_type =='si' || $user_type =='SI') :
                ?>
                    <li class="nav-item">
                        <a class="nav-link menu-link" href="<?= site_url('dashboard') ?>" role="button" aria-expanded="false" aria-controls="sidebarDashboards">
                            <i class="las la-tachometer-alt"></i> <span data-key="t-dashboards">Dashboards</span>
                        </a>

                    </li>
                    <?php endif;
                     if ($user_type == 'Admin' || $user_type == 'admin') :
                     ?>

                    <li class="nav-item">
                        <a class="nav-link menu-link" href="<?= site_url('survey-count') ?>" role="button" aria-expanded="false" aria-controls="sidebarDashboards">
                            <i class="bx bxs-user-pin"></i> <span data-key="t-dashboards">Banner Survey Count (Enumerator wise)</span>
                        </a>
                    </li>
                    <?php endif; 
                     if ($user_type == 'Admin' || $user_type == 'admin' || $user_type =='si' || $user_type =='SI') :?>


                    <li class="nav-item">
                        <a class="nav-link menu-link" href="<?= site_url('ward-summary-list') ?>" role="button" aria-expanded="false" aria-controls="sidebarDashboards">
                            <i class="bx bxs-user-pin"></i> <span data-key="t-dashboards">Ward Summary</span>
                        </a>

                    </li>


                <?php endif; ?>
                <?php if(!in_array($user_type,array('si','google_street_user'))) :?>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="<?= site_url('survey-list') ?>" role="button" aria-expanded="false" aria-controls="sidebarApps">
                        <i class="bx bx-list-ol"></i> <span data-key="t-apps">Survey List</span>
                    </a>

                </li>
                <?php endif; ?>

                <?php if($user_type!='google_street_user') :?>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="<?= site_url('map') ?>" role="button" aria-expanded="false" aria-controls="sidebarApps">
                        <i class="bx bx-map-pin"></i> <span data-key="t-apps">Map</span>
                    </a>

                </li>
                <?php endif; ?>
                <?php if($user_type=='google_street_user'||$user_type == 'Admin' || $user_type == 'admin') :?>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="<?= site_url('signboard-list') ?>" role="button" aria-expanded="false" aria-controls="sidebarApps">
                        <i class='bx bxl-google-cloud'></i> <span data-key="t-apps">Signboard Google Street</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="<?= site_url('users-report') ?>" role="button" aria-expanded="false" aria-controls="sidebarApps">
                        <i class="bx bxs-report"></i> <span data-key="t-apps">Google Street Report</span>
                    </a>

                </li>


                <?php
                endif;
                if ($user_type == 'Admin' || $user_type == 'admin') :
                ?>

                    <li class="nav-item">
                        <a class="nav-link menu-link" href="#sidebarDashboards" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarDashboards">
                            <i class="las la-tachometer-alt"></i> <span data-key="t-dashboards">Masters</span>
                        </a>
                        <div class="collapse menu-dropdown" id="sidebarDashboards">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a href="<?= site_url('master/categories') ?>" class="nav-link" data-key="t-analytics"> Category </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?= site_url('master/banner-status') ?>" class="nav-link" data-key="t-crm">Bannner Status </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?= site_url('master/board-nature') ?>" class="nav-link" data-key="t-ecommerce"> Board Nature </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?= site_url('master/government-type') ?>" class="nav-link" data-key="t-crypto"> Government Types </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?= site_url('master/installed-on') ?>" class="nav-link" data-key="t-projects"> Installed On </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?= site_url('master/media-type') ?>" class="nav-link" data-key="t-nft">Media Types</a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?= site_url('master/user-type') ?>" class="nav-link" data-key="t-nft">User Types</a>
                                </li>

                                <li class="nav-item">
                                    <a href="<?= site_url('master/nature-of-property') ?>" class="nav-link" data-key="t-ecommerce"> Nature Of Property </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?= site_url('master/notice-type') ?>" class="nav-link" data-key="t-crypto">Notice Types</a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?= site_url('master/one-half-meter') ?>" class="nav-link" data-key="t-projects">One Half Meters From Road</a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?= site_url('master/owner-type') ?>" class="nav-link" data-key="t-nft">Owner Types</a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?= site_url('master/plate-available') ?>" class="nav-link" data-key="t-ecommerce"> Plate Available</a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?= site_url('master/wards') ?>" class="nav-link" data-key="t-crypto"> Wards</a>
                                </li>
                            </ul>
                        </div>
                    </li> <!-- end Dashboard Menu -->
                    <li class="nav-item">
                        <a class="nav-link menu-link" href="<?= site_url('users-list') ?>" role="button" aria-expanded="false" aria-controls="sidebarApps">
                            <i class="bx bx-list-ol"></i> <span data-key="t-apps">User Management</span>
                        </a>

                    </li>
                    <?php endif ?>
                    




            </ul>
        </div>
        <!-- Sidebar -->
    </div>

    <div class="sidebar-background"></div>
</div>