<?php
class SignboardModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function insert($data, $table = NULL)
    {
        $this->db->set($data);
        $this->db->insert($table);
        return $last_id = $this->db->insert_id();
    }

    public function update($where = NULL, $data=NULL, $table = NULL)
    {
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        // $this->db->update($table, $data);
        $this->db->set($data);
       return $this->db->update($table);
       // return $this->db->get_compiled_update($table);
    }
    public function get_single_data($where = NULL, $table = NULL)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where_in('status',array(1,0));
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                $this->db->where($key, $value);
            }
        }
//        return $this->db->get_compiled_select();
        return $this->db->get()->row_array();
    }
    
    public function get_all_table_data($where = NULL, $table=NULL)
    {
        $this->db->select('*');
        $this->db->from($table);
      //   $this->db->where_in('status', array(1, 0));
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        // return $this->db->get_compiled_select();
        return $this->db->get()->result_array();
    }
   
    public function get_all_single_join_data($where = NULL,$tab_col = NULL, $table = NULL, $jointable = NULL)
    {
        if ($tab_col != NULL) {
            $this->db->select($tab_col);
        } else {
            $this->db->select('*');
        }
        $this->db->from($table . ' l');
      //   $this->db->where_in('l.status', array(1, 0));
        $this->db->join($jointable . ' ut', 'l.' . $jointable . '_id=ut.id');
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        return $this->db->get()->result_array();
    }
    public function get_all_double_join_data($where = NULL, $tab_col = NULL, $table = NULL, $jointable1 = NULL, $jointable2 = NULL)
    {
        if ($tab_col != NULL) {
            $this->db->select($tab_col);
        } else {
            $this->db->select('*');
        }
        $this->db->from($table . ' l');
        $this->db->join($jointable1 . ' jt1', 'l.' . $jointable1 . '_id=jt1.id');
        $this->db->join($jointable2 . ' jt2', 'jt1.' . $jointable2 . '_id=jt2.id');
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        return $this->db->get()->result_array();
    }
    public function get_all_triple_join_data($where = NULL, $tab_col = NULL, $table = NULL, $jointable1 = NULL, $jointable2 = NULL, $jointable3 = NULL)
    {
        if ($tab_col != NULL) {
            $this->db->select($tab_col);
        } else {
            $this->db->select('*');
        }
        $this->db->from($table . ' l');
        $this->db->join($jointable1 . ' jt1', 'l.' . $jointable1 . '_id=jt1.id');
        $this->db->join($jointable2 . ' jt2', 'jt1.' . $jointable2 . '_id=jt2.id');
        $this->db->join($jointable3 . ' jt3', 'jt2.' . $jointable3 . '_id=jt3.id');
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        return $this->db->get()->result_array();
    }
    public function get_all_fourth_join_data($where = NULL, $tab_col = NULL, $table = NULL, $jointable1 = NULL, $jointable2 = NULL, $jointable3 = NULL, $jointable4 = NULL)
    {
        if ($tab_col != NULL) {
            $this->db->select($tab_col);
        } else {
            $this->db->select('*');
        }
        $this->db->from($table . ' l');
        $this->db->join($jointable1 . ' jt1', 'l.' . $jointable1 . '_id=jt1.id');
        $this->db->join($jointable2 . ' jt2', 'jt1.' . $jointable2 . '_id=jt2.id');
        $this->db->join($jointable3 . ' jt3', 'jt2.' . $jointable3 . '_id=jt3.id');
        $this->db->join($jointable4 . ' jt4', 'jt3.' . $jointable4 . '_id=jt4.id');
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        //        return $this->db->get_compiled_select();
        return $this->db->get()->result_array();
    }
    
    public function get_all_fifth_join_data($where = NULL, $tab_col = NULL, $table = NULL, $jointable1 = NULL, $jointable2 = NULL, $jointable3 = NULL, $jointable4 = NULL, $jointable5 = NULL)
    {
        if ($tab_col != NULL) {
            $this->db->select($tab_col);
        } else {
            $this->db->select('*');
        }
        $this->db->from($table . ' l');
        $this->db->join($jointable1 . ' jt1', 'l.' . $jointable1 . '_id=jt1.id');
        $this->db->join($jointable2 . ' jt2', 'jt1.' . $jointable2 . '_id=jt2.id');
        $this->db->join($jointable3 . ' jt3', 'jt2.' . $jointable3 . '_id=jt3.id');
        $this->db->join($jointable4 . ' jt4', 'jt3.' . $jointable4 . '_id=jt4.id');
        $this->db->join($jointable5 . ' jt5', 'jt4.' . $jointable5 . '_id=jt5.id');
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        //        return $this->db->get_compiled_select();
        return $this->db->get()->result_array();
    }
    public function get_all_sixth_join_data($where = NULL, $tab_col = NULL, $table = NULL, $jointable1 = NULL, $jointable2 = NULL, $jointable3 = NULL, $jointable4 = NULL, $jointable5 = NULL, $jointable6 = NULL)
    {
        if ($tab_col != NULL) {
            $this->db->select($tab_col);
        } else {
            $this->db->select('*');
        }
        $this->db->from($table . ' l');
        $this->db->join($jointable1 . ' jt1', 'l.' . $jointable1 . '_id=jt1.id');
        $this->db->join($jointable2 . ' jt2', 'jt1.' . $jointable2 . '_id=jt2.id');
        $this->db->join($jointable3 . ' jt3', 'jt2.' . $jointable3 . '_id=jt3.id');
        $this->db->join($jointable4 . ' jt4', 'jt3.' . $jointable4 . '_id=jt4.id');
        $this->db->join($jointable5 . ' jt5', 'jt4.' . $jointable5 . '_id=jt5.id');
        $this->db->join($jointable6 . ' jt6', 'l.' . $jointable6 . '_id=jt6.id');
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        //        return $this->db->get_compiled_select();
        return $this->db->get()->result_array();
    }
    public function get_all_seven_join_data($where = NULL, $tab_col = NULL, $table = NULL, $jointable1 = NULL, $jointable2 = NULL, $jointable3 = NULL, $jointable4 = NULL, $jointable5 = NULL, $jointable6 = NULL)
    {
        if ($tab_col != NULL) {
            $this->db->select($tab_col);
        } else {
            $this->db->select('*');
        }
        $this->db->from($table . ' l');
        $this->db->join($jointable1 . ' jt1', 'l.' . $jointable1 . '_id=jt1.id',"left");
        $this->db->join($jointable2 . ' jt2', 'jt1.' . $jointable2 . '_id=jt2.id',"left");
        $this->db->join($jointable3 . ' jt3', 'jt2.' . $jointable3 . '_id=jt3.id',"left");
        $this->db->join($jointable4 . ' jt4', 'jt3.' . $jointable4 . '_id=jt4.id',"left");
        $this->db->join($jointable5 . ' jt5', 'jt4.' . $jointable5 . '_id=jt5.id',"left");
        $this->db->join($jointable6 . ' jt6', 'jt5.' . $jointable6 . '_id=jt6.id',"left");
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                $this->db->where($key, $value);
            }
        }
            //    return $this->db->get_compiled_select();
        return $this->db->get()->result_array();
    }    
    function get($id = NULL)
    {
        $this->db->select("*");
        $this->db->from('users');
        if ($id != NULL) {
            $this->db->where("id", $id);
            return $this->db->get()->row_array();
        } else {
            return $this->db->get()->result_array();
        }
    }
    function get_data($where = NULL, $tab_col = NULL, $table = NULL)
    {
        if ($tab_col != NULL) {
            $this->db->select($tab_col);
        } else {
            $this->db->select('*');
        }
        $this->db->from($table);
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                $this->db->where($key, $value);
            }
        }
//        return $this->db->get_compiled_select();
        return $this->db->get()->result_array();
    }
    function get_data_in_single_table($where = NULL, $tab_col = NULL)
    {
        if ($tab_col != NULL) {
            $this->db->select($tab_col);
        } else {
            $this->db->select('*');
        }
        $this->db->from("signboard_google_street s");
        $this->db->join("media_type m","s.media_type_id=m.media_type_id","left");
        $this->db->join("nature_of_property n","s.nature_of_property_id=n.nature_of_property_id","left");
        $this->db->join("wards w","s.ward_name_id=w.ward_name_id","left");
        $this->db->join("banner_user b","s.created_by_user_id=b.uid","left");
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                $this->db->where_in($key, $value);
            }
        }
        //        return $this->db->get_compiled_select();
        return $this->db->get()->result_array();
    }
    public function get_user_count_report($where=NULL)
    {
        $this->db->select('b.first_name,b.last_name,count(*) as total_count');
        $this->db->from("signboard_google_street s");
        $this->db->join("banner_user b","s.created_by_user_id=b.uid","left");
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        $this->db->group_by('b.first_name,b.last_name');
        // return $this->db->get_compiled_select();
        return $this->db->get()->result_array();
    }
    public function deactive_table_row_query($where_not_condition, $where_in_condition, $data, $table)
    {
        foreach ($where_not_condition as $key => $value) {
            $this->db->where_not_in($key, $value);
        }
        foreach ($where_in_condition as $key => $value) {
            $this->db->where($key, $value);
        }
        $this->db->update($table, $data);
    }

}