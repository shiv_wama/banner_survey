        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">
            <style>
                /* CUSTOM RADIO BUTTON */
                .rad {
                    cursor: pointer;
                    user-select: none;
                    -webkit-user-select: none;
                    -webkit-touch-callout: none;
                }

                .rad>input {
                    /* HIDE ORG RADIO & CHECKBOX */
                    position: absolute;
                    opacity: 0;
                    width: 0;
                    height: 0;
                }

                /* RADIO & CHECKBOX STYLES */
                /* DEFAULT <i> STYLE */
                .rad>i{
                    display: inline-block;
                    vertical-align: middle;
                    height: 16px;
                    transition: 0.2s;
                    box-shadow: inset 0 0 0 8px #fff;
                    border: 1px solid gray;
                    background: gray;
                }

                .rad>i {
                    width: 16px;
                    border-radius: 50%;
                }

           
                .rad:hover>i {
                    /* HOVER <i> STYLE */
                    box-shadow: inset 0 0 0 3px #fff;
                    background: gray;
                }

                .rad>input:focus+i {
                    /* FOCUS <i> STYLE */
                    outline: 1px solid blue;
                }

                .rad>input:checked+i {
                    /* (RADIO CHECKED) <i> STYLE */
                    box-shadow: inset 0 0 0 3px #fff;
                    background: orange;
                }

    

             
            </style>

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0"><?= $page_title ?></h4>

                                <!-- <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Tables</a></li>
                                        <li class="breadcrumb-item active">Datatables</li>
                                    </ol>
                                </div> -->

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <?php
                                            $name = $this->uri->segment(2);
                                            ?>
                                            <button class="btn btn-primary btn-sm" onclick="addNewInMaster(this)" data-name="<?= $name ?>">Add New</button>
                                        </div>

                                    </div>
                                </div>

                                <div class="card-body">
                                    <table class="master_datatable table nowrap align-middle" style="width:100%">
                                        <?=
                                        $data;
                                        ?>

                                        <tbody>
                                            <?php

                                            switch ($type) {
                                                case 'category':

                                                    if (!empty($tbody_data)) :
                                                        foreach ($tbody_data as $key => $value) :
                                                            echo '<tr>
                                                            <td>' . $value['category_value'] . '</td>
                                                            <td>
                                                            
                                                            <div class="dropdown d-inline-block">
                                                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                                    <i class="ri-more-fill align-middle"></i>
                                                                </button>
                                                                <ul class="dropdown-menu dropdown-menu-end">
                                                                
                                                                    <li><button type="button" onclick="getMasterData(this)" 

                                                                    data-column-name="category_id"
                                                                    data-table-name="category"

                                                                    data-id="' . base64_encode($value['category_id']) . '" 
                                                                    data-form-number="1"
                                                                    class="dropdown-item edit-item-btn">
                                                                    <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit </button>
                                                                    </li>

                                                                    <li><button type="button" onclick="deleteMasterData(this)" 
                                                                    data-id="' . base64_encode($value['category_id']) . '" 

                                                                       data-column-name="category_id"
                                                                    data-table-name="category"

                                                                    class="dropdown-item edit-item-btn"
                                                                    data-form-number="2"
                                                                    >
                                                                    <i class=" bx bxs-trash-alt align-bottom me-2 text-muted"></i> Delete</button>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </td>
                                                            </tr>';
                                                        endforeach;
                                                    endif;

                                                    break;

                                                case 'banner_status':

                                                    if (!empty($tbody_data)) :
                                                        foreach ($tbody_data as $key => $value) :
                                                            echo '<tr>
                                                            <td>' . $value['id'] . '</td>
                                                            <td>' . $value['banner_status_value']  . '</td>
                                                            <td>';
                                                            
                                                            switch ($value['banner_status_value']) {
                                                                case 'Approached for sanction':
                                                                   echo ' 
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id']. '" name="rad1" value="approached_for_sanction_1.png" 
                                                                    '. ($value['banner_marker'] == 'approached_for_sanction_1.png' ? 'checked' : '' ) .'/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/approached_for_sanction_1.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad1" 
                                                                    '. ($value['banner_marker'] == 'approached_for_sanction_2.png' ? 'checked' : '' ) .'/>
                                         
                                                                    <i></i> <img src="' . base_url('public/banner_markers/approached_for_sanction_2.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad1" value="approached_for_sanction_3.png"
                                                                    '. ($value['banner_marker'] == 'approached_for_sanction_3.png' ? 'checked' : '' ) .'/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/approached_for_sanction_3.png') . '">
                                                                    </label>';
                                                                    break;



                                                                case 'Court Case':
                                                                    case  "Court case":
                                                                    echo ' 
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad2" value="court_case_1.png"
                                                                     '. ($value['banner_marker'] == 'court_case_1.png' ? 'checked' : '' ) .'/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/court_case_1.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad2" value="court_case_2.png"  
                                                                    '. ($value['banner_marker'] == 'court_case_2.png' ? 'checked' : '' ) .'/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/court_case_2.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad2" value="court_case_3.png"  
                                                                    '. ($value['banner_marker'] == 'court_case_3.png' ? 'checked' : '' ) .'/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/court_case_3.png') . '">
                                                                    </label>
                                                                    ';
                                                                    break;



                                                                      case 'Demolished':
                                                                    echo ' 
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad3" value="demolished_1.png" 
                                                                    '. ($value['banner_marker'] == 'demolished_1.png' ? 'checked' : '' ) .'/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/demolished_1.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad3" value="demolished_2.png" 
                                                                    '. ($value['banner_marker'] == 'demolished_2.png' ? 'checked' : '' ) .'/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/demolished_2.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad3" value="demolished_3.png"
                                                                    '. ($value['banner_marker'] == 'demolished_3.png' ? 'checked' : '' ) .'/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/demolished_3.png') . '">
                                                                    </label>
                                                                    ';
                                                                    break;


                                                                case 'Existing Id Found':
                                                                    case "Existing Id found":
                                                                    echo ' 
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad4" value="existing_id_found_1.png" 
                                                                    '. ($value['banner_marker'] == 'existing_id_found_1.png' ? 'checked' : '' ) .'/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/existing_id_found_1.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id']. '" name="rad4" value="existing_id_found_2.png" 
                                                                    ' . ($value['banner_marker'] == 'existing_id_found_2.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/existing_id_found_2.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id']. '" name="rad4" value="existing_id_found_3.png"
                                                                    ' . ($value['banner_marker'] == 'existing_id_found_3.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/existing_id_found_3.png') . '">
                                                                    </label>
                                                                    ';
                                                                    break;



                                                                case 'Illegal':
                                                                    echo ' 
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id']. '" name="rad5" value="Illegal_1.png" 
                                                                    ' . ($value['banner_marker'] == 'Illegal_1.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/Illegal_1.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad5" value="Illegal_2.png" 
                                                                    ' . ($value['banner_marker'] == 'Illegal_2.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/Illegal_2.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id']. '" name="rad5" value="Illegal_3.png"
                                                                    ' . ($value['banner_marker'] == 'Illegal_3.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/Illegal_3.png') . '">
                                                                    </label>
                                                                    ';
                                                                    break;



                                                                case 'Legalized':
                                                                    echo ' 
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad6" value="legalized_1.png"
                                                                    ' . ($value['banner_marker'] == 'legalized_1.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/legalized_1.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad6" value="legalized_2.png" 
                                                                     ' . ($value['banner_marker'] == 'legalized_2.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/legalized_2.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad6" value="legalized_3.png"
                                                                    ' . ($value['banner_marker'] == 'legalized_3.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/legalized_3.png') . '">
                                                                    </label>
                                                                    ';
                                                                    break;



                                                                case 'Not Useful':
                                                                    case "Not useful":
                                                                    echo ' 
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id']. '" name="rad7" value="not_useful_1.png" 
                                                                      ' . ($value['banner_marker'] == 'not_useful_1.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/not_useful_1.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id']. '" name="rad7" value="not_useful_2.png" 
                                                                      ' . ($value['banner_marker'] == 'not_useful_2.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/not_useful_2.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad7" value="not_useful_3.png"
                                                                      ' . ($value['banner_marker'] == 'not_useful_3.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/not_useful_3.png') . '">
                                                                    </label>
                                                                    ';
                                                                    break;


                                                                case 'Notice delivered':
                                                                    echo ' 
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad8" value="notice_delivered_1.png" 
                                                                    ' . ($value['banner_marker'] == 'notice_delivered_1.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/notice_delivered_1.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id']. '" name="rad8" value="notice_delivered_2.png"
                                                                     ' . ($value['banner_marker'] == 'notice_delivered_2.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/notice_delivered_2.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id']. '" name="rad8" value="notice_delivered_3.png"
                                                                     ' . ($value['banner_marker'] == 'notice_delivered_3.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/notice_delivered_3.png') . '">
                                                                    </label>
                                                                    ';
                                                                    break;


                                                                case 'Notice Signed':
                                                                    echo ' 
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id']. '" name="rad9" value="notice_signed_1.png" 
                                                                     ' . ($value['banner_marker'] == 'notice_signed_1.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/notice_signed_1.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id']. '" name="rad9" value="notice_signed_2.png" 
                                                                     ' . ($value['banner_marker'] == 'notice_signed_2.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/notice_signed_2.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id']. '" name="rad9" value="notice_signed_3.png"
                                                                     ' . ($value['banner_marker'] == 'notice_signed_3.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/notice_signed_3.png') . '">
                                                                    </label>
                                                                    ';
                                                                    break;



                                                                case 'Ready For Action':
                                                                    echo ' 
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id']. '" name="rad10" value="ready_for_action_1.png" 
                                                                     ' . ($value['banner_marker'] == 'ready_for_action_1.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/ready_for_action_1.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad10" value="ready_for_action_2.png" 
                                                                     ' . ($value['banner_marker'] == 'ready_for_action_2.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/ready_for_action_2.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad10" value="ready_for_action_3.png"
                                                                     ' . ($value['banner_marker'] == 'ready_for_action_3.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/ready_for_action_3.png') . '">
                                                                    </label>
                                                                    ';
                                                                    break;


                                                                case 'Ready For Sign':
                                                                    echo ' 
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad11" value="ready_for_sign_1.png" 
                                                                     ' . ($value['banner_marker'] == 'ready_for_sign_1.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/ready_for_sign_1.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id']. '" name="rad11" value="ready_for_sign_2.png" 
                                                                    ' . ($value['banner_marker'] == 'ready_for_sign_2.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/ready_for_sign_2.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id']. '" name="rad11" value="ready_for_sign_3.png"
                                                                    ' . ($value['banner_marker'] == 'ready_for_sign_3.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/ready_for_sign_3.png') . '">
                                                                    </label>
                                                                    ';
                                                                    break;




                                                                case 'Signboard':
                                                                    echo ' 
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id']. '" name="rad12" value="signboard_1.png" 
                                                                    ' . ($value['banner_marker'] == 'signboard_1.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/signboard_1.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad12" value="signboard_2.png"
                                                                     ' . ($value['banner_marker'] == 'signboard_2.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/signboard_2.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad12" value="signboard_3.png"
                                                                     ' . ($value['banner_marker'] == 'signboard_3.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/signboard_3.png') . '">
                                                                    </label>
                                                                    ';
                                                                    break;




                                                                case 'To be verified':
                                                                    echo ' 
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id']. '" name="rad13" value="to_be_verified_1.png" 
                                                                     ' . ($value['banner_marker'] == 'to_be_verified_1.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/to_be_verified_1.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad13" value="to_be_verified_2.png" 
                                                                     ' . ($value['banner_marker'] == 'to_be_verified_2.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/to_be_verified_2.png') . '">
                                                                    </label>
                                                                    <label class="rad">
                                                                    <input type="radio" onchange="changeMarker(this)" data-id="'.$value['id'].'" name="rad13" value="to_be_verified_3.png"
                                                                     ' . ($value['banner_marker'] == 'to_be_verified_3.png' ? 'checked' : '') . '/>
                                                                    <i></i> <img src="' . base_url('public/banner_markers/to_be_verified_3.png') . '">
                                                                    </label>
                                                                    ';
                                                                    break;


                                                                  




                                                                
                                                                default:
                                                                    # code...
                                                                    break;
                                                            }
                                                        


                                                           

                                                            echo '</td>


                                                            <td>
                                                            
                                                            <div class="dropdown d-inline-block">
                                                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                                    <i class="ri-more-fill align-middle"></i>
                                                                </button>
                                                                <ul class="dropdown-menu dropdown-menu-end">
                                                                
                                                                    <li><button type="button" onclick="getMasterData(this)" 

                                                                    data-column-name="id"
                                                                    data-table-name="banner_status"

                                                                    data-id="' . base64_encode($value['id']) . '" 
                                                                    data-form-number="1"
                                                                    class="dropdown-item edit-item-btn">
                                                                    <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit </button>
                                                                    </li>

                                                                    <li><button type="button" onclick="deleteMasterData(this)" 
                                                                    data-id="' . base64_encode($value['id']) . '" 

                                                                    data-column-name="id"
                                                                    data-table-name="banner_status"

                                                                    class="dropdown-item edit-item-btn"
                                                                    data-form-number="2"
                                                                    >
                                                                    <i class=" bx bxs-trash-alt align-bottom me-2 text-muted"></i> Delete</button>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </td>
                                                            </tr>';
                                                        endforeach;
                                                    endif;

                                                    break;

                                                case 'board_nature':

                                                    if (!empty($tbody_data)) :
                                                        foreach ($tbody_data as $key => $value) :
                                                            echo '<tr>
                                                            <td>' . $value['board_nature_value'] . '</td>
                                                            <td>
                                                            
                                                            <div class="dropdown d-inline-block">
                                                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                                    <i class="ri-more-fill align-middle"></i>
                                                                </button>
                                                                <ul class="dropdown-menu dropdown-menu-end">
                                                                
                                                                    <li><button type="button" onclick="getMasterData(this)" 
                                                                    data-column-name="board_nature_id"
                                                                    data-table-name="board_nature"
                                                                    data-id="' . base64_encode($value['board_nature_id']) . '" 
                                                                    data-form-number="1"
                                                                    class="dropdown-item edit-item-btn">
                                                                    <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit </button>
                                                                    </li>

                                                                    <li><button type="button" onclick="deleteMasterData(this)" 
                                                                    data-id="' . base64_encode($value['board_nature_id']) . '" 

                                                                    data-column-name="board_nature_id"
                                                                    data-table-name="board_nature"

                                                                    class="dropdown-item edit-item-btn"
                                                                    data-form-number="2"
                                                                    >
                                                                    <i class=" bx bxs-trash-alt align-bottom me-2 text-muted"></i> Delete</button>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </td>
                                                            </tr>';
                                                        endforeach;
                                                    endif;

                                                    break;

                                                case 'govt_type':

                                                    if (!empty($tbody_data)) :
                                                        foreach ($tbody_data as $key => $value) :
                                                            echo '<tr>
                                                            <td>' . $value['govt_type_value'] . '</td>
                                                            <td>
                                                            
                                                            <div class="dropdown d-inline-block">
                                                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                                    <i class="ri-more-fill align-middle"></i>
                                                                </button>
                                                                <ul class="dropdown-menu dropdown-menu-end">
                                                                
                                                                    <li><button type="button" onclick="getMasterData(this)" 
                                                                    data-column-name="govt_type_id"
                                                                    data-table-name="govt_type"
                                                                    data-id="' . base64_encode($value['govt_type_id']) . '" 
                                                                    data-form-number="1"
                                                                    class="dropdown-item edit-item-btn">
                                                                    <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit </button>
                                                                    </li>

                                                                    <li><button type="button" onclick="deleteMasterData(this)" 
                                                                    data-id="' . base64_encode($value['govt_type_id']) . '" 

                                                                    data-column-name="govt_type_id"
                                                                    data-table-name="govt_type"

                                                                    class="dropdown-item edit-item-btn"
                                                                    data-form-number="2"
                                                                    >
                                                                    <i class=" bx bxs-trash-alt align-bottom me-2 text-muted"></i> Delete</button>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </td>
                                                            </tr>';
                                                        endforeach;
                                                    endif;

                                                    break;

                                                case 'installed_on':

                                                    if (!empty($tbody_data)) :
                                                        foreach ($tbody_data as $key => $value) :
                                                            echo '<tr>
                                                            <td>' . $value['installed_on_value'] . '</td>
                                                            <td>
                                                            
                                                            <div class="dropdown d-inline-block">
                                                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                                    <i class="ri-more-fill align-middle"></i>
                                                                </button>
                                                                <ul class="dropdown-menu dropdown-menu-end">
                                                                
                                                                    <li><button type="button" onclick="getMasterData(this)" 
                                                                    data-column-name="installed_on_id"
                                                                    data-table-name="installed_on"
                                                                    data-id="' . base64_encode($value['installed_on_id']) . '" 
                                                                    data-form-number="1"
                                                                    class="dropdown-item edit-item-btn">
                                                                    <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit </button>
                                                                    </li>

                                                                    <li><button type="button" onclick="deleteMasterData(this)" 
                                                                    data-id="' . base64_encode($value['installed_on_id']) . '" 

                                                                    data-column-name="installed_on_id"
                                                                    data-table-name="installed_on"

                                                                    class="dropdown-item edit-item-btn"
                                                                    data-form-number="2"
                                                                    >
                                                                    <i class=" bx bxs-trash-alt align-bottom me-2 text-muted"></i> Delete</button>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </td>
                                                            </tr>';
                                                        endforeach;
                                                    endif;

                                                    break;

                                                case 'media_type':

                                                    if (!empty($tbody_data)) :
                                                        foreach ($tbody_data as $key => $value) :
                                                            echo '<tr>
                                                            <td>' . $value['media_type_value'] . '</td>
                                                            <td>
                                                            
                                                            <div class="dropdown d-inline-block">
                                                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                                    <i class="ri-more-fill align-middle"></i>
                                                                </button>
                                                                <ul class="dropdown-menu dropdown-menu-end">
                                                                
                                                                    <li><button type="button" onclick="getMasterData(this)" 
                                                                    data-column-name="media_type_id"
                                                                    data-table-name="media_type"
                                                                    data-id="' . base64_encode($value['media_type_id']) . '" 
                                                                    data-form-number="1"
                                                                    class="dropdown-item edit-item-btn">
                                                                    <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit </button>
                                                                    </li>

                                                                    <li><button type="button" onclick="deleteMasterData(this)" 
                                                                    data-id="' . base64_encode($value['media_type_id']) . '" 

                                                                    data-column-name="media_type_id"
                                                                    data-table-name="media_type"

                                                                    class="dropdown-item edit-item-btn"
                                                                    data-form-number="2"
                                                                    >
                                                                    <i class=" bx bxs-trash-alt align-bottom me-2 text-muted"></i> Delete</button>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </td>
                                                            </tr>';
                                                        endforeach;
                                                    endif;

                                                    break;

                                                
                                                case 'user_type':

                                                    if (!empty($tbody_data)) :
                                                        foreach ($tbody_data as $key => $value) :
                                                            echo '<tr>
                                                            <td>' . $value['user_role'] . '</td>
                                                            <td>
                                                            
                                                            <div class="dropdown d-inline-block">
                                                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                                    <i class="ri-more-fill align-middle"></i>
                                                                </button>
                                                                <ul class="dropdown-menu dropdown-menu-end">
                                                                
                                                                    <li><button type="button" onclick="getMasterData(this)" 
                                                                    data-column-name="role_type"
                                                                    data-table-name="user_roles"
                                                                    data-id="' . base64_encode($value['role_type']) . '" 
                                                                    data-form-number="1"
                                                                    class="dropdown-item edit-item-btn">
                                                                    <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit </button>
                                                                    </li>

                                                                    <li><button type="button" onclick="deleteMasterData(this)" 
                                                                    data-id="' . base64_encode($value['role_type']) . '" 

                                                                    data-column-name="role_type"
                                                                    data-table-name="user_roles"

                                                                    class="dropdown-item edit-item-btn"
                                                                    data-form-number="2"
                                                                    >
                                                                    <i class=" bx bxs-trash-alt align-bottom me-2 text-muted"></i> Delete</button>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </td>
                                                            </tr>';
                                                        endforeach;
                                                    endif;

                                                    break;

                                                case 'nature_of_property':

                                                    if (!empty($tbody_data)) :
                                                        foreach ($tbody_data as $key => $value) :
                                                            echo '<tr>
                                                            <td>' . $value['nature_of_property_value'] . '</td>
                                                            <td>
                                                            
                                                            <div class="dropdown d-inline-block">
                                                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                                    <i class="ri-more-fill align-middle"></i>
                                                                </button>
                                                                <ul class="dropdown-menu dropdown-menu-end">
                                                                
                                                                    <li><button type="button" onclick="getMasterData(this)" 
                                                                    data-column-name="nature_of_property_id"
                                                                    data-table-name="nature_of_property"
                                                                    data-id="' . base64_encode($value['nature_of_property_id']) . '" 
                                                                    data-form-number="1"
                                                                    class="dropdown-item edit-item-btn">
                                                                    <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit </button>
                                                                    </li>

                                                                    <li><button type="button" onclick="deleteMasterData(this)" 
                                                                    data-id="' . base64_encode($value['nature_of_property_id']) . '" 

                                                                    data-column-name="nature_of_property_id"
                                                                    data-table-name="nature_of_property"

                                                                    class="dropdown-item edit-item-btn"
                                                                    data-form-number="2"
                                                                    >
                                                                    <i class=" bx bxs-trash-alt align-bottom me-2 text-muted"></i> Delete</button>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </td>
                                                            </tr>';
                                                        endforeach;
                                                    endif;

                                                    break;

                                                case 'notice_type':

                                                    if (!empty($tbody_data)) :
                                                        foreach ($tbody_data as $key => $value) :
                                                            echo '<tr>
                                                            <td>' . $value['notice_type_value'] . '</td>
                                                            <td>
                                                            
                                                            <div class="dropdown d-inline-block">
                                                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                                    <i class="ri-more-fill align-middle"></i>
                                                                </button>
                                                                <ul class="dropdown-menu dropdown-menu-end">
                                                                
                                                                    <li><button type="button" onclick="getMasterData(this)" 
                                                                    data-column-name="notice_type_id"
                                                                    data-table-name="notice_type"
                                                                    data-id="' . base64_encode($value['notice_type_id']) . '" 
                                                                    data-form-number="1"
                                                                    class="dropdown-item edit-item-btn">
                                                                    <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit </button>
                                                                    </li>

                                                                    <li><button type="button" onclick="deleteMasterData(this)" 
                                                                    data-id="' . base64_encode($value['notice_type_id']) . '" 

                                                                    data-column-name="notice_type_id"
                                                                    data-table-name="notice_type"

                                                                    class="dropdown-item edit-item-btn"
                                                                    data-form-number="2"
                                                                    >
                                                                    <i class=" bx bxs-trash-alt align-bottom me-2 text-muted"></i> Delete</button>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </td>
                                                            </tr>';
                                                        endforeach;
                                                    endif;

                                                    break;

                                                case 'one_half_meters_from_road':

                                                    if (!empty($tbody_data)) :
                                                        foreach ($tbody_data as $key => $value) :
                                                            echo '<tr>
                                                            <td>' . $value['one_and_a_half_meters_from_road_value'] . '</td>
                                                            <td>
                                                            
                                                            <div class="dropdown d-inline-block">
                                                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                                    <i class="ri-more-fill align-middle"></i>
                                                                </button>
                                                                <ul class="dropdown-menu dropdown-menu-end">
                                                                
                                                                    <li><button type="button" onclick="getMasterData(this)" 
                                                                    data-column-name="one_and_a_half_meters_from_road_id"
                                                                    data-table-name="one_half_meters_from_road"
                                                                    data-id="' . base64_encode($value['one_and_a_half_meters_from_road_id']) . '" 
                                                                    data-form-number="1"
                                                                    class="dropdown-item edit-item-btn">
                                                                    <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit </button>
                                                                    </li>

                                                                    <li><button type="button" onclick="deleteMasterData(this)" 
                                                                    data-id="' . base64_encode($value['one_and_a_half_meters_from_road_id']) . '" 

                                                                    data-column-name="one_and_a_half_meters_from_road_id"
                                                                    data-table-name="one_half_meters_from_road"

                                                                    class="dropdown-item edit-item-btn"
                                                                    data-form-number="2"
                                                                    >
                                                                    <i class=" bx bxs-trash-alt align-bottom me-2 text-muted"></i> Delete</button>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </td>
                                                            </tr>';
                                                        endforeach;
                                                    endif;

                                                    break;

                                                case 'owner_type':

                                                    if (!empty($tbody_data)) :
                                                        foreach ($tbody_data as $key => $value) :
                                                            echo '<tr>
                                                            <td>' . $value['owner_type_value'] . '</td>
                                                            <td>
                                                            
                                                            <div class="dropdown d-inline-block">
                                                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                                    <i class="ri-more-fill align-middle"></i>
                                                                </button>
                                                                <ul class="dropdown-menu dropdown-menu-end">
                                                                
                                                                    <li><button type="button" onclick="getMasterData(this)" 
                                                                    data-column-name="owner_type_id"
                                                                    data-table-name="owner_type"
                                                                    data-id="' . base64_encode($value['owner_type_id']) . '" 
                                                                    data-form-number="1"
                                                                    class="dropdown-item edit-item-btn">
                                                                    <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit </button>
                                                                    </li>

                                                                    <li><button type="button" onclick="deleteMasterData(this)" 
                                                                    data-id="' . base64_encode($value['owner_type_id']) . '" 

                                                                    data-column-name="owner_type_id"
                                                                    data-table-name="owner_type"

                                                                    class="dropdown-item edit-item-btn"
                                                                    data-form-number="2"
                                                                    >
                                                                    <i class=" bx bxs-trash-alt align-bottom me-2 text-muted"></i> Delete</button>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </td>
                                                            </tr>';
                                                        endforeach;
                                                    endif;

                                                    break;

                                                case 'plate_available':

                                                    if (!empty($tbody_data)) :
                                                        foreach ($tbody_data as $key => $value) :
                                                            echo '<tr>
                                                            <td>' . $value['plate_available_value'] . '</td>
                                                            <td>
                                                            
                                                            <div class="dropdown d-inline-block">
                                                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                                    <i class="ri-more-fill align-middle"></i>
                                                                </button>
                                                                <ul class="dropdown-menu dropdown-menu-end">
                                                                
                                                                    <li><button type="button" onclick="getMasterData(this)" 
                                                                    data-column-name="plate_available_id"
                                                                    data-table-name="plate_available"
                                                                    data-id="' . base64_encode($value['plate_available_id']) . '" 
                                                                    data-form-number="1"
                                                                    class="dropdown-item edit-item-btn">
                                                                    <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit </button>
                                                                    </li>

                                                                    <li><button type="button" onclick="deleteMasterData(this)" 
                                                                    data-id="' . base64_encode($value['plate_available_id']) . '" 

                                                                    data-column-name="plate_available_id"
                                                                    data-table-name="plate_available"

                                                                    class="dropdown-item edit-item-btn"
                                                                    data-form-number="2"
                                                                    >
                                                                    <i class=" bx bxs-trash-alt align-bottom me-2 text-muted"></i> Delete</button>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </td>
                                                            </tr>';
                                                        endforeach;
                                                    endif;

                                                    break;

                                                case 'wards':

                                                    if (!empty($tbody_data)) :
                                                        foreach ($tbody_data as $key => $value) :
                                                            echo '<tr>
															<td>'.$value['ward_name_id'].'</td>
                                                            <td>' . $value['ward_name'] . '</td>
                                                            <td>
                                                            
                                                            <div class="dropdown d-inline-block">
                                                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                                    <i class="ri-more-fill align-middle"></i>
                                                                </button>
                                                                <ul class="dropdown-menu dropdown-menu-end">
                                                                
                                                                    <li><button type="button" onclick="getMasterData(this)" 
                                                                    data-column-name="ward_name_id"
                                                                    data-table-name="wards"
                                                                    data-id="' . base64_encode($value['ward_name_id']) . '" 
                                                                    data-form-number="1"
                                                                    class="dropdown-item edit-item-btn">
                                                                    <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit </button>
                                                                    </li>

                                                                    <li><button type="button" onclick="deleteMasterData(this)" 
                                                                    data-id="' . base64_encode($value['ward_name_id']) . '" 

                                                                    data-column-name="ward_name_id"
                                                                    data-table-name="wards"

                                                                    class="dropdown-item edit-item-btn"
                                                                    data-form-number="2"
                                                                    >
                                                                    <i class=" bx bxs-trash-alt align-bottom me-2 text-muted"></i> Delete</button>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </td>
                                                            </tr>';
                                                        endforeach;
                                                    endif;

                                                    break;

                                                    //

                                            }

                                            ?>

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>