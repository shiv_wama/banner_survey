<?php
class DashboardController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('DashboardModel');
        $this->load->model('CommonModel');
        $this->user_id = isset($_SESSION['USER']['uid']) ?  $_SESSION['USER']['uid'] : 0;
        $this->user_type = isset($_SESSION['USER']['user_type']) ?  $_SESSION['USER']['user_type'] : 0;
        if ($this->user_id  ==  0)
            redirect('AuthController');
    }
    public function index()
    {
        $page_data['count_data'] = $this->DashboardModel->getDashboardCount();
        $page_data['title'] = 'Banner Survey | Dashboard';
        $this->load->view('includes/header', $page_data);
        $this->load->view('dashboard/dashboard', $page_data);
        $this->load->view('includes/footer');
    }

    //getExistingIllegalGraphData
    public function getExistingIllegalGraphData()
    {
        $res = [];
        $count_data = $this->DashboardModel->getDashboardCount();
        http_response_code(200);
        echo json_encode(['status'=>http_response_code(),'msg'=>'Success','data'=>$count_data]);
        die;
    }
    //getMediaTypeData
    public function getMediaTypeData()
    {
        $data = $this->DashboardModel->getMediaTypeData();
        foreach ($data as $key => $value) {
            $media_type = $this->CommonModel->getSingleData('media_type', ['media_type_id' => $value['media_type_id']], ['media_type_value']);
            $res[] = [
                'value' => (int)$value['count'],
                'name' => $media_type['media_type_value']
            ];
        }
        http_response_code(200);
        echo json_encode(['status' => http_response_code(), 'msg' => 'success', 'data' => $res]);
        die;
    }
    //getAvailablePlateData
    public function getAvailablePlateData()
    {
        $data = $this->DashboardModel->getAvailablePlateData();
        foreach ($data as $key => $value) {
            if ($value['plate_available_id'] != '') {
                $plate_value = $this->CommonModel->getSingleData('plate_available', ['plate_available_id' => $value['plate_available_id']], ['plate_available_value']);
                // echo '<pre>';
                // print_r($plate_value);die;
                $res[] = [
                    'value' => isset($value['count']) ? (int)$value['count'] : 0,
                    'name' => isset($plate_value['plate_available_value']) ? $plate_value['plate_available_value'] : ''
                ];
            }
        }
        http_response_code(200);
        echo json_encode(['status' => http_response_code(), 'msg' => 'success', 'data' => $res]);
        die;
    }
    //getSurveyData
    public function getSurveyData()
    {
        $res = array();
        $data = $this->DashboardModel->getLastSevenDaysSurveyData();


       
        // echo '<pre>';
        // print_r($data);
        // die;
        // foreach ($data as $key => $value) {
        //     $res[] = 
        //     [   
        //         'name'=>$value['created_date'],
        //         'data'=>array((int)$value['count']),
        //     ];
        // }
        // echo '<pre>';
        // print_r($res);die;
        http_response_code(200);
        echo json_encode(['status' => http_response_code(), 'msg' => 'success', 'data' => $data]);
        die;
    }
}
