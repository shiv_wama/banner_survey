<?php
class AuthController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AuthModel');
        $this->load->model('CommonModel');
        $this->userID = isset($_SESSION['USER']['uid']) ? $_SESSION['USER']['uid'] : 0;
        if ($this->userID == 0)
            $this->load->view('auth/sigin');
    }


    public function index()
    {
        // echo '<pre>';
        // print_r($_SESSION);die;
        $userID = isset($_SESSION['USER']['uid']) ? $_SESSION['USER']['uid'] : 0;
        if($userID == 0) 
        $this->load->view('auth/sigin');
        else
        redirect('/dashboard');
    }

    //users
    public function users()
    {
        $page_data['title'] = 'Users List';
        $page_data['user_roles'] = $this->CommonModel->getMultipleData('user_roles',NULL,NULL);
/*         echo "<pre>";
        print_r($page_data['user_roles']);die; */

        $this->load->view('includes/header',$page_data);
        $this->load->view('users/user_list',$page_data);
        $this->load->view('includes/footer');

    }


    //getUserRoles
    public function getUserRoles()
    {
        $data = $this->CommonModel->getMultipleData('user_roles',NULL,NULL);
        http_response_code(200);
        echo json_encode(['status'=>http_response_code(),'msg'=>'success','user_role'=>$data]);
        die;
    }

    //getUserList
    public function getUserList()
    {

        $result = $this->AuthModel->getUserList($_POST);
        $allCount = $this->AuthModel->countAllUserDetail();
        $filteredCount = $this->AuthModel->countFilterUserDetail($_POST);
        $final = array();
        if (!empty($result)) {

            foreach ($result as $readData) {
                $row = array();
                $row[] = isset($readData['uid']) ? $readData['uid']  : '--';
                $row[] = isset($readData['first_name']) ? $readData['first_name'].' '.$readData['last_name'].'<br>'. '<a class="badge badge-outline-primary" href="mailto:'.$readData['email'].'">'.$readData['email']. '</a><br><a class="badge badge-outline-info" href="tel:' . $readData['mobile_no'] . '">'.$readData['mobile_no'].'</a>'  : '--';
                $row[] = isset($readData['user_type']) ? $readData['user_type']  : '--';
                $row[] = isset($readData['sqid']) ? $readData['sqid'] : '--';
                if($readData['enabled'] == 1)
                    $status = '<span class="badge badge-soft-success">Active</span>';
                else
                    $status = '<span class="badge badge-soft-danger">Inactive/Deleted</span>';

                $row[] = $status;
                if($readData['enabled'] == 1){

                $btn = '
                                    <li><button type="button" onclick="deleteMasterData(this)" 
                                    data-id="' . base64_encode($readData['uid']) . '" 

                                        data-column-name="uid"
                                    data-table-name="banner_user"

                                    class="dropdown-item edit-item-btn"
                                    data-form-number="2"
                                    >
                                    <i class=" bx bxs-trash-alt align-bottom me-2 text-muted"></i> Delete</button>
                                    </li>';
                }
                else
                {

                    $btn = '
                                    <li><button type="button" onclick="activateUser(this)" 
                                    data-id="' . base64_encode($readData['uid']) . '" 

                                        data-column-name="uid"
                                    data-table-name="banner_user"

                                    class="dropdown-item edit-item-btn"
                                    data-form-number="2"
                                    >
                                    <i class=" bx bx-refresh align-bottom me-2 text-muted"></i> Activate</button>
                                    </li>';
                }

                                    
                                     

                $action = '<div class="dropdown d-inline-block">
                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="ri-more-fill align-middle"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-end">
                                   
                                    <li><button type="button" onclick="editUser(this)" 
                                    data-id="' . base64_encode($readData['uid']) . '" 
                                 
                                    class="dropdown-item edit-item-btn">
                                    <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit</button>
                                    </li>'. $btn.'</ul>
                            </div>';
                $row[] = $action;

                $final[] = $row;
            }
        }

        $results = array(
            "sEcho" => $_POST['draw'],
            "iTotalRecords" => $allCount,
            "iTotalDisplayRecords" => $filteredCount,
            "aaData" => $final
        );
        echo json_encode($results);
    }

    //activateUser
    public function activateUser()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $tableName = $data['table_name'];
        $column = $data['column'];
        $id = base64_decode($data['id']);
        $tableData = $this->CommonModel->getSingleData($tableName, [$column => $id], NULL);
        if(empty($tableData)){
            json_encode(400);
            echo json_encode(['status'=>http_response_code(),'msg'=>'Data does not exist']);
            die;
        }
        $res = $this->CommonModel->updateData($tableName,[$column=>$id],['enabled'=>1]);
        http_response_code(200);
        echo json_encode(['status'=> http_response_code(),'msg'=>'Success']);die;
    }

    // /getUserData
    public function getUserData()
    {
        $data = json_decode(file_get_contents('php://input'),true);
        $id = base64_decode($data['id']);

        $userRoleData = $this->CommonModel->getMultipleData('user_roles',NULL,NULL);


        $user_data = $this->CommonModel->getSingleData('banner_user',['uid'=>$id], [
            'address',
            'city',
            'uid',
            'email',
            'first_name',
            'last_name',
            'mobile_no',
            'pincode',
            'sqid',
            'state',
            'user_type'
        ]);
        http_response_code(200);
        echo json_encode(['status'=>http_response_code(),'msg'=>'Success','data'=>$user_data, 'user_role' => $userRoleData]);die;
    }

    //store
    public function store()
    {
        // echo '<pre>';
        // print_r($_POST);
        // die;
        $userId = $_POST['user__id'];
        if($userId > 0)
        {
            //update
            $oldData = $this->CommonModel->getSingleData('banner_user',['uid'=>$userId],NULL);
            $oldJsonData = json_encode($oldData);
            if($_POST['password']!='')
            {
            $updateArr = [
                'password' => sha1($_POST['password']),            
                'first_name'=>$_POST['first__name'], 
                'last_name'=>$_POST['last__name'], 
                'email'=>$_POST['email'], 
                'mobile_no'=>$_POST['mobile_number'], 
                'user_type'=>$_POST['user__type'], 
                'sqid'=>$_POST['sqid'], 
                'address'=>$_POST['address'], 
                'city'=>$_POST['city'], 
                'state'=>$_POST['state'], 
                'pincode'=>$_POST['pincode']
            ];
            }
            else
            {
                $updateArr = [
                        'first_name' => $_POST['first__name'],
                        'last_name' => $_POST['last__name'],
                        'email' => $_POST['email'],
                        'mobile_no' => $_POST['mobile_number'],
                        'user_type' => $_POST['user__type'],
                        'sqid' => $_POST['sqid'],
                        'address' => $_POST['address'],
                        'city' => $_POST['city'],
                        'state' => $_POST['state'],
                        'pincode' => $_POST['pincode']
                    ];
            }
          

            $res = $this->CommonModel->updateData('banner_user',['uid'=>$userId],$updateArr);

            $newData =  $this->CommonModel->getSingleData('banner_user', ['uid' => $userId], NULL);
            $newJsonData = json_encode($newData);

            $insertArr = [
                'table_name'=>'banner_user', 
                'old_data'=>$oldJsonData, 
                'new_data'=>$newJsonData, 
                'updated_by'=> $this->userID, 
                'created_at'=>date('Y-m-d H:i:s')

            ];
            $res = $this->CommonModel->insertData('logs',$insertArr);
            http_response_code(200);
            echo json_encode(['status'=>http_response_code(),'msg'=>'User updated.']);die;
        }
        else{
            //insert
            $checkUser = $this->CommonModel->getSingleData('banner_user',['email'=>$_POST['email']],NULL);
            if(!empty($checkUser))
            {
                http_response_code(400);
                echo json_encode(['status'=>http_response_code(),'msg'=>'User already exist.']);
                die;
            }
          
            $insertArr = [
                'password'=>sha1($_POST['password']), 
                'first_name'=>$_POST['first__name'], 
                'last_name'=>$_POST['last__name'], 
                'email'=>$_POST['email'], 
                'mobile_no'=>$_POST['mobile_number'], 
                // 'uid'=> $lastID['uid']+1, 
                'user_type'=>$_POST['user__type'], 
                'sqid'=>isset($_POST['sqid']) ? ($_POST['sqid'] > 0 ? $_POST['sqid'] : 0) :0, 
                'address'=>$_POST['address'], 
                'city'=>$_POST['city'], 
                'state'=>$_POST['state'], 
                'pincode'=>$_POST['pincode'], 
                'creation_date'=>date('Y-m-d H:i:s'), 
                'enabled'=>1
            ];

            $res = $this->CommonModel->insertData('banner_user',$insertArr);
            http_response_code(201);
            echo json_encode(['status' => http_response_code(), 'msg' => 'User created.']);
            die;
        }
    }



    public function signIn()
    {
        $user_name = $_POST['user_name'];
        $password = $_POST['password'];

        if($user_name=='' || $password=='')
        {
            http_response_code(404);
            echo json_encode(['status'=>http_response_code(),'msg'=>'Required field missing']);
            die;
        }

        try {
            $userData = $this->CommonModel->getSingleData('banner_user', ['email' => $user_name, 'password' => sha1($password)], ['uid', 'first_name', 'last_name', 'user_type']);
            
                // echo '<pre>';
                // print_r($userData);die;
            
            
            if(empty($userData))
            {
                http_response_code(400);
                echo json_encode(['status'=> http_response_code(),'msg'=>'Please check user name or password.']);
                die;
            }
            $SESSION['USER']['uid'] = $userData['uid'];
            $SESSION['USER']['first_name'] = $userData['first_name'];
            $SESSION['USER']['last_name'] = $userData['last_name'];
            $SESSION['USER']['user_type'] = $userData['user_type'];
            $this->session->set_userdata($SESSION);
            $redirect_url = '';
            if($userData['user_type'] == 'Admin' || $userData['user_type'] == 'admin' )
            $redirect_url = 'dashboard';
            elseif($userData['user_type'] == 'si' || $userData['user_type'] == 'SI')
                $redirect_url = 'dashboard';
			elseif($userData['user_type'] == 'google_street_user' || $userData['user_type'] == 'Google_Street_User')
                $redirect_url = 'signboard-list';
            else
                $redirect_url = 'survey-list';
            http_response_code(200);
            echo json_encode(['status'=>http_response_code(),'msg'=>'User log in  success.' ,'redirect_url'=>$redirect_url]);
            die;
        
        } catch (\Exception $e) {
            http_response_code(400);
            echo json_encode(['status'=>http_response_code(),'msg'=>$e->getMessage()]);
            die;
        }
     




        
    }


    public function logout()
    {
        $track_id =  isset($_SESSION['USER']['uid']) ? $_SESSION['USER']['uid'] : 0 ;
        //update logout time
        // $res = $this->CommonModel->updateData('track_user', ['id' => $track_id], ['logout_time' => date('H:i:s')]);
        unset($_SESSION['USER']);
        unset($_SESSION['error']);
        // session_destroy();
        redirect("AuthController", "location");
    }



}
