<?php
class SurveyModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'banner_survey';
        $this->baner_user = 'banner_user';
        $this->user_type = isset($_SESSION['USER']['user_type']) ?  $_SESSION['USER']['user_type'] : '';
        $this->user_id = isset($_SESSION['USER']['uid']) ?  $_SESSION['USER']['uid'] : '';
    }

    //getCurrentMonthSureyCount
    public function getCurrentMonthSureyCount($start_date=NULL,$end_date =NULL,$surevout_ids = NULL,$ward__id =NULL)
    {
        $currMonth =  date('m');
        $this->db->select('count(banner_id) as count,w.ward_name, bU.first_name,bU.last_name,EXTRACT(month from created_date)  as month');
        $this->db->from('banner_survey bS');
        $this->db->join('wards w','bS.ward_name_id = w.ward_name_id','LEFT');
        $this->db->join('banner_user bU','bS.user_id = bU.uid');
        if($surevout_ids!=NULL)
        $this->db->where_in('bS.user_id',$surevout_ids);

        if($ward__id!=NULL)
        $this->db->where_in('bS.ward_name_id',$ward__id);
        //startdate && enddate
        if($start_date!=NULL && $end_date!=NULL)
            $this->db->where(['created_date>=' => $start_date , 'created_date<=' => $end_date]);
        else
            $this->db->where('EXTRACT(month from created_date) = ' ,$currMonth);
        
        
        $this->db->group_by('w.ward_name,bU.first_name,bU.last_name,created_date');
        return $this->db->get()->result_array();
        // $this->db->select();
    }
    //getSurveyourTodayCount
    public function getSurveyourSurveCount($start_date = NUll, $end_date = NULL)
    {
        $res = array();
        //1. all surveyour list
        $surveyourData = $this->db->select('uid,first_name,last_name')
            ->from('banner_user')
            ->where('user_type', 'Enum')
            ->get()->result_array();
        //2. all surve count
        foreach ($surveyourData as $key => $value) {
            $result = $this->getServeyourCounts($value['uid'], $start_date, $end_date);
            $res[] =
                [
                    'name' => $value['first_name'] . ' ' . $value['last_name'] . ' <span class="badge badge-outline-primary">' . $value['uid'] . '</span>',
                    'formDataArr' => $result['formDataArr'],
                ];
        }
        return $res;
    }
    public function getServeyourCounts($id, $start_date = NULL, $end_date = NULL)
    {
        $dateDiff = $this->customlibrary->dateDiff($start_date, $end_date);
       
        $days = $dateDiff['days'];
        $month = $dateDiff['months'];
        if ($month > 0) {
            $month_days = $month * 30;
            $days = $days + $month_days;
        }
        $formDataArr = [];
        for ($i = 0; $i <= $days; $i++) {
            // $date = date('Y-m-d',strtotime($start_date,' + '.$i +' days'));
            $date = date('Y-m-d', strtotime($start_date . ' + ' . $i . ' days'));
            /***********************************form 1 *******************/
            //insert
            $this->db->select('count(banner_id) as count');
            $this->db->from('banner_survey bS');
            // $this->db->join('wards w','bS.ward_name_id = w.ward_name_id','INNER');
            if ($start_date != NULL && $end_date != NULL)
                $this->db->where(['bS.created_date' => $date]);
            else
                $this->db->where('bS.created_date', date('Y-m-d'));
          
            $this->db->where(['bS.user_id' => $id]);
            // $this->db->group_by('w.ward_name');
            $form1CreatedCount = $this->db->get()->row_array();
            //updated
            $this->db->select('count(banner_id) as count');
            $this->db->from('banner_survey bS');
           
            if ($start_date != NULL && $end_date != NULL)
                $this->db->where(['bS.updated_date' => $date]);
            else
                $this->db->where('bS.updated_date', date('Y-m-d'));
            $this->db->where(['bS.user_id' => $id]);
          
            $form1UpdatedCount = $this->db->get()->row_array();
            // if(!empty($form1UpdatedCount) || !empty($form1CreatedCount))
            // {
            // array_push($formDataArr, isset($form1CreatedCount['ward_name']) ? $form1CreatedCount['ward_name'] : '--');
            array_push($formDataArr, isset($form1CreatedCount['count']) ? $form1CreatedCount['count'] : 0);
            array_push($formDataArr, isset($form1UpdatedCount['count']) ? $form1UpdatedCount['count'] : 0);
            array_push($formDataArr, isset($form1CreatedCount['count']) ? (isset($form1UpdatedCount['count'])  ? $form1CreatedCount['count'] + $form1UpdatedCount['count'] : 0) : 0);
            /***********************************end form 1 ************************/
            /***********************************form 2 *******************/
            $this->db->select('count(id) as count');
            $this->db->from('banner_details_contact_survey bCS');
            $this->db->join('banner_survey bS', 'bCS.banner_id = bS.banner_id');
         
            if ($start_date != NULL && $end_date != NULL)
                $this->db->where(['bCS.entered_date' => $date]);
            else
                $this->db->where('bCS.entered_date', date('Y-m-d'));
                
            $this->db->where(['bS.user_id' => $id]);
            
            $form2CreatedCount = $this->db->get()->row_array();
            //update
            $this->db->select('count(id) as count');
            $this->db->from('banner_details_contact_survey bCS');
            $this->db->join('banner_survey bS', 'bCS.banner_id = bS.banner_id');
           
            if ($start_date != NULL && $end_date != NULL)
                $this->db->where(['bCS.updated_date' => $date]);
            else
                $this->db->where('bCS.updated_date', date('Y-m-d'));
            $this->db->where(['bS.user_id' => $id]);
            
            $form2UpdatedCount = $this->db->get()->row_array();
            array_push($formDataArr, isset($form2CreatedCount['count']) ? $form2CreatedCount['count'] : 0);
            array_push($formDataArr, isset($form2UpdatedCount['count']) ? $form2UpdatedCount['count'] : 0);
            array_push($formDataArr, isset($form2CreatedCount['count']) ? (isset($form2UpdatedCount['count'])  ? $form2CreatedCount['count'] + $form2UpdatedCount['count'] : 0) : 0);
            /***********************************end form 2 ************************/
            /***********************************form 3 *******************/
            $this->db->select('count(notice_id) as count');
            $this->db->from('notice_details nD');
            $this->db->join('banner_survey bS', 'nD.banner_id = bS.banner_id');
           
            if ($start_date != NULL && $end_date != NULL)
                $this->db->where(['nD.entered_date' => $date]);
            else
                $this->db->where('nD.entered_date', date('Y-m-d'));
            $this->db->where(['bS.user_id' => $id]);
          
            $form3CreatedCount = $this->db->get()->row_array();
         
            $this->db->from('notice_details nD');
            $this->db->join('banner_survey bS', 'nD.banner_id = bS.banner_id');
         
            if ($start_date != NULL && $end_date != NULL)
                $this->db->where(['nD.updated_date' => $date]);
            else
                $this->db->where('nD.updated_date', date('Y-m-d'));
            $this->db->where(['bS.user_id' => $id]);
           
            // $this->db->group_by('nD.updated_date');
            // if ($start_date != NULL && $end_date != NULL)
            //     $form3UpdatedCount = $this->db->get()->result_array();
            // else
                $form3UpdatedCount = $this->db->get()->row_array();
            array_push($formDataArr, isset($form3CreatedCount['count']) ? $form3CreatedCount['count'] : 0);
            array_push($formDataArr, isset($form3UpdatedCount['count']) ? $form3UpdatedCount['count'] : 0);
            array_push($formDataArr, isset($form3CreatedCount['count']) ? (isset($form3UpdatedCount['count'])  ? $form3CreatedCount['count'] + $form3UpdatedCount['count'] : 0) : 0);
        }
        /***********************************end form 3 ************************/
        return [
            'formDataArr' => $formDataArr,
        ];
    }
    ////////////////////////////////////-----getBannerSurveyList --------------////////////////////////////////
    public function getBannerSurveyList($postData, $start_date = NULL, $end_date = NULL,$ward_ids = NULL,$banner_id = NULL)
    {
        $this->_get_datatables_query__banner__survey__list($postData, $start_date, $end_date,$ward_ids,$banner_id);
        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        return $query->result_array();
    }
    public function countAllBannerSurveyDetail($start_date = NULL, $end_date = NULL,$ward_ids = NULL,$banner_id=NULL)
    {
        $this->db->from($this->table. ' t1');
        $this->db->join('wards w','t1.ward_name_id = w.ward_name_id','LEFT');
        if($ward_ids!=NULL)
        $this->db->where_in('t1.ward_name_id',$ward_ids);
        if ($start_date != NULL && $end_date != NULL)
            $this->db->where(['created_date >=' => $start_date, 'created_date <=' => $end_date]);
        elseif ($start_date != NULL && $end_date == NULL)
            $this->db->where(['created_date' => $start_date]);
        if ($this->user_type == 'Enum')
            $this->db->where('user_id', $this->user_id);
        if($banner_id!=NULL)
            $this->db->where('banner_id',$banner_id);
        return $this->db->count_all_results();
    }
    public function countFilterBannerSurveyDetail($postData, $start_date = NULL, $end_date = NULL,$ward_ids = NULL,$banner_id =NULL)
    {
        $this->_get_datatables_query__banner__survey__list($postData, $start_date, $end_date,$ward_ids,$banner_id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    private function _get_datatables_query__banner__survey__list($postData, $start_date, $end_date,$ward_ids,$banner_id)
    {
        $this->db->select('
            bS.banner_id, 
            mT.media_type_value, 
            bS.address,
            bS.location_lat,
            bS.location_lng,
            bS.property_name,
            bS.society_name, 
            bS.created_date as survey_date,
            bS.created_time as survey_time,
            w.ward_name
        ');
        $this->db->from($this->table . ' as bS');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id', 'LEFT');
        $this->db->join('wards w','bS.ward_name_id = w.ward_name_id','LEFT');
        if($ward_ids!=NULL)
        $this->db->where_in('bS.ward_name_id',$ward_ids);
        if ($start_date != NULL && $end_date != NULL)
            $this->db->where(['bS.created_date >=' => $start_date, 'bS.created_date <=' => $end_date]);
        elseif ($start_date != NULL && $end_date == NULL)
            $this->db->where(['bS.created_date' => $start_date]);
        if ($this->user_type == 'Enum')
            $this->db->where('bS.user_id', $this->user_id);
        if($banner_id!=NULL)
        $this->db->where('bS.banner_id',$banner_id);
        $i = 0;
        // loop searchable columns
        $column_order = array(
            'bS.banner_id',
            'w.ward_name',
            'mT.media_type_value',
            'bS.address',
            'bS.property_name',
            'bS.society_name',
            'bS.created_date',
            'bS.created_time'
        );
        $column_search = array(
            'bS.banner_id',
            'w.ward_name',
            'mT.media_type_value',
            'bS.address',
            'bS.property_name',
            'bS.society_name',
        );
        $order = array('bS.banner_id' => 'DESC');
        foreach ($column_search as $item) {
            // if datatable send POST for search
            if ($postData['search']['value']) {
                // first loop
                if ($i === 0) {
                    // open bracket
                    // echo (int)$postData['search']['value'];die;
                    $this->db->group_start();
                    if (((int)$postData['search']['value'] > 0)) {
                        $this->db->where($item, $postData['search']['value']);
                        // echo 'inside if';die;
                        // $i++;
                    }
                } else {
                    $this->db->or_like($item, $postData['search']['value']);
                }
                // last loop
                if (count($column_search) - 1 == $i) {
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }
        if (isset($postData['order'])) {
            $this->db->order_by($column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($order)) {
            $order = $order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    ///////////////////////////--------------getBannerSurveyList end -----------------////////////////////////
    public function getLatLongForMap($media_id=NULL,$banner_id= NULL)
    {
        
         $this->db->select(
            '
            bS.banner_id,
            bS.address,
            mT.media_type_value,
            bS.banner_location_lat,
            bS.banner_location_lng,
            bnS.banner_status_value,
            bnS.banner_marker
            '
         );
             $this->db->from('banner_survey bS');
             $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
             $this->db->join('banner_status bnS', 'bS.banner_status_id = bnS.id', 'LEFT');
             if($media_id!=null && $media_id!='NULL')
             $this->db->where('bS.media_type_id',$media_id);

             if($banner_id!=null)
             $this->db->where('bS.banner_id',$banner_id);
             
             return $this->db->get()->result_array();
    }
    // /getSurveyDataforCSV
    public function getSurveyDataforCSV($start_date, $end_date)
    {
        $this->db->select('
        bS.banner_id,
        bS.address,
        bS.approx_banner_height,
        bS.approx_banner_length,
        bS.approx_banner_width,
        bS.banner_location_lat,
        bS.banner_location_lng,
        bS.landmark,
        bS.location_lat,
        bS.location_lng,
        bS.property_contact_no,
        bS.property_name,
        bS.road_status,
        bS.society_name,
        bS.created_date,
        bS.created_time,
        bS.old_id,
        bS.proper_road_name,
        bS.survey_date,
        bS.survey_time,
        bS.remarks,
        mT.media_type_value,
        pA.plate_available_value,
        bU.first_name,
        bU.last_name,
        bU.email,
        bU.mobile_no,
        bU.user_type,
        bU.city,
        bU.state,
        bU.pincode,
        w.ward_name,
        baS.banner_status_value
        ');
        $this->db->from('banner_survey bS');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id', 'LEFT');
        $this->db->join('plate_available pA', 'bS.plate_available_id = pA.plate_available_id', 'LEFT');
        $this->db->join('banner_user bU', 'bS.user_id = bU.uid', 'INNER');
        $this->db->join('wards w', 'bS.ward_name_id = w.ward_name_id', 'LEFT');
        $this->db->join('banner_status baS', 'bS.banner_status_id = baS.id', 'LEFT');
        if ($start_date != 0 && $end_date != 0)
            $this->db->where(['bS.created_date >=' => $start_date, 'bS.created_date<=' => $end_date]);
        if ($start_date != 0 && $end_date == 0)
            $this->db->where(['bS.created_date >=' => $start_date]);
        $this->db->order_by('bS.banner_id', 'DESC');
        return $this->db->get()->result_array();
    }
    //getFormOneData
    public function getFormOneData($lat, $long)
    {
        $this->db->select('
       bS.*,
        mT.media_type_value,
        pA.plate_available_value,
        bU.first_name,
        bU.last_name,
        bU.email,
        bU.mobile_no,
        bU.user_type,
        bU.city,
        bU.state,
        bU.pincode,
        w.ward_name,
        baS.banner_status_value,
        iON.installed_on_value,
        ');
        $this->db->from('banner_survey bS');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id', 'LEFT');
        $this->db->join('plate_available pA', 'bS.plate_available_id = pA.plate_available_id', 'LEFT');
        $this->db->join('banner_user bU', 'bS.user_id = bU.uid', 'INNER');
        $this->db->join('wards w', 'bS.ward_name_id = w.ward_name_id', 'LEFT');
        $this->db->join('banner_status baS', 'bS.banner_status_id = baS.id', 'LEFT');
        $this->db->join('installed_on iON', 'bS.installed_on_id = iON.installed_on_id', 'LEFT');
        $this->db->where(['bS.banner_location_lat' => $lat, 'bS.banner_location_lng' => $long]);
        $this->db->order_by('bS.banner_id', 'DESC');
        return $this->db->get()->row_array();
    }
}
