<?php
class Report extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('SignboardModel');
        $this->load->model('CommonModel');
        $this->user_id = isset($_SESSION['USER']['uid']) ?  $_SESSION['USER']['uid'] : 0;
        $this->user_type = isset($_SESSION['USER']['user_type']) ?  $_SESSION['USER']['user_type'] : 0;
        // echo $this->user_id;die;
        if ($this->user_id  ==  0)
        redirect('AuthController');
        // if (($this->user_type != 'google_street_user')||($this->user_type != 'Admin')) {
        // if(!in_array($this->user_type,array('google_street_user','Admin'))){
        //    echo json_encode(['status' => 401, 'msg' => "You don't have permission to access this page."]);
        //     die;
        // }
    }
    public function index()
    {
        $page_data['title'] = 'Report | Banner Survey';
        $this->load->view('includes/header');
        $this->load->view('signboard/report_list');
        $this->load->view('includes/footer');
    }
    public function list_data()
    {
        $all_data = array();
        $where = array();
        $GET = $this->input->get();
        if(isset($GET['from_date'])&&!empty($GET['from_date']))
        {
            $where['(s.created_datetime::date)>=']=$GET['from_date'];
        }
        if(isset($GET['from_date'])&&!empty($GET['from_date'])&&isset($GET['to_date'])&&!empty($GET['to_date']))
        {
            $where['(s.created_datetime::date)<=']=$GET['to_date'];
        }
        if(!in_array($this->user_type,array("Admin","admin")))
        {
            $where['s.created_by_user_id']=$this->user_id;
        }
        $where['s.status']=1;
        $all_data = $this->SignboardModel->get_user_count_report($where);
        // print_r($all_data);
        // die;
        $output_var['data'] = array();
        if(isset($all_data)&&!empty($all_data))
        {
            $data_count=0;
            foreach ($all_data as $key=> $value) {
                if(!empty($value))
                {
                    $output_var['data'][$data_count][0]=$value['first_name']." ".$value['last_name'];
                    $output_var['data'][$data_count][1]=$value['total_count'];
                    $data_count++;
                }
            }
        }
        echo json_encode($output_var); 
    }  
}
