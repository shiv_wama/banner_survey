<?php
class SurveyController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('SurveyModel');
        $this->load->model('CommonModel');
        $this->load->library('CustomLibrary');
        $this->user_id = isset($_SESSION['USER']['uid']) ?  $_SESSION['USER']['uid'] : 0;
        $this->user_type = isset($_SESSION['USER']['user_type']) ?  $_SESSION['USER']['user_type'] : 0;
        // echo $this->user_id;die;
        if ($this->user_id  ==  0)
            redirect('AuthController');
    }
    public function index()
    {
        $page_data['ward_data_arr'] = $this->CommonModel->getMultipleData('wards', NULL, ['ward_name_id', 'ward_name']);
        $page_data['signboard_types'] = $this->CommonModel->getMultipleData('signboard_types', ['signboard_status' => 1], ['id', 'signboard_type_name']);
        $page_data['leglization_posibilities'] =  $this->CommonModel->getMultipleData('legalization_possibility', NULL, NULL);
        $page_data['banner_id_data'] = $this->CommonModel->getMultipleData('banner_survey',NULL,['banner_id']);
        // echo '<pre>';
        // print_r($page_data['leglization_posibilities']);
        // die;
        $this->load->view('includes/header');
        $this->load->view('survey/list', $page_data);
        $this->load->view('includes/footer');
    }
    //surveyCount
    public function surveyCount()
    {
        $page_data['surveyour_counts'] = $this->SurveyModel->getSurveyourSurveCount();
        $page_data['ward_data_arr'] = $this->CommonModel->getMultipleData('wards', NULL, ['ward_name_id', 'ward_name']);
        $page_data['surveyour_data'] = $this->CommonModel->getMultipleData('banner_user', NULL, ['uid', 'first_name', 'last_name']);
        $page_data['title'] = 'Surveyour Count';
        $this->load->view('includes/header', $page_data);
        $this->load->view('survey/survey_count', $page_data);
        $this->load->view('includes/footer');
    }
    //getSurveyourCounts
    public function getSurveyourCounts()
    {
        $res = array();
        $data = json_decode(file_get_contents('php://input'), true);
        // echo '<pre>';
        // print_r($data);die;
        $userIds = isset($data['userIds']) ? $data['userIds'] : '';
        $date = isset($data['date']) ? $data['date'] : NULL;
        $start_date = '';
        $end_date = '';
        // echo $date;die;
        if ($date > 0) {
            $dateArr = explode('to', $date);
            $start_date = $dateArr[0];
            $end_date = $dateArr[1];
        }
        $this->load->library('CustomLibrary');
        $dateDiff = $this->customlibrary->dateDiff($start_date, $end_date);
        $days = $dateDiff['days'];
        $month = $dateDiff['months'];
        if ($month > 0) {
            $month_days = $month * 30;
            $days = $days + $month_days;
        }
        $dateArr = [];
        $tableDateHeadingArr = [];
        $i = 0;
        array_push($tableDateHeadingArr, 'Surveyour Name/ID');
        // array_push($tableDateHeadingArr,'Ward Name');
        while ($i <= $days) {
            $date = date('Y-m-d', strtotime($start_date . ' + ' . $i . ' days'));
            array_push($dateArr, $date);
            array_push($tableDateHeadingArr, date('d M,Y', strtotime($date)) . ' (Form 1 Count)');
            array_push($tableDateHeadingArr, date('d M,Y', strtotime($date)) . ' (Form 1 Updated Count)');
            array_push($tableDateHeadingArr, date('d M,Y', strtotime($date)) . ' (Form 1 Total Count)');
            array_push($tableDateHeadingArr, date('d M,Y', strtotime($date)) . ' (Form 2 Count)');
            array_push($tableDateHeadingArr, date('d M,Y', strtotime($date)) . ' (Form 2 Updated Count)');
            array_push($tableDateHeadingArr, date('d M,Y', strtotime($date)) . ' (Form 2 Total Count)');
            array_push($tableDateHeadingArr, date('d M,Y', strtotime($date)) . ' (Form 3 Count)');
            array_push($tableDateHeadingArr, date('d M,Y', strtotime($date)) . ' (Form 3 Updated Count)');
            array_push($tableDateHeadingArr, date('d M,Y', strtotime($date)) . ' (Form 3 Total Count)');
            $i++;
        }
        if (empty($userIds) && empty($date)) {
            $user_count_data = $this->SurveyModel->getSurveyourSurveCount();
            $tableDateHeadingArr = [
                date('d M,Y') . 'Form 1 Count',
                date('d M,Y') . 'Form 1 Updated Count',
                date('d M,Y') . 'Form 1 Total Count',
                date('d M,Y') . 'Form 2 Count',
                date('d M,Y') . 'Form 2 Updated Count',
                date('d M,Y') . 'Form 2 Total Count',
                date('d M,Y') . 'Form 3 Count',
                date('d M,Y') . 'Form 3 Updated Count',
                date('d M,Y') . 'Form 3 Total Count'
            ];
            $res = [
                // 'date_arr'=>$dateArr,
                'table_headging_arr' => $tableDateHeadingArr,
                'count_data_arr' => $user_count_data
            ];
        }
        if (!empty($userIds)) {
            for ($i = 0; $i < count($userIds); $i++) {
                $countData = $this->SurveyModel->getServeyourCounts($userIds[$i], $start_date, $end_date);
                $userData = $this->CommonModel->getSingleData('banner_user', ['uid' => $userIds[$i]], ['uid', 'first_name', 'last_name']);
                $user_count_data[] = [
                    'name' => $userData['first_name'] . ' ' . $userData['last_name'],
                    'formDataArr' => $countData['formDataArr'],
                ];
            } //end for
            $res = [
                'date_arr' => $dateArr,
                'table_headging_arr' => $tableDateHeadingArr,
                'count_data_arr' => $user_count_data
            ];
        } //end if
        if (empty($userIds)) {
            $countData = $this->SurveyModel->getSurveyourSurveCount($start_date, $end_date);
            $res = [
                'date_arr' => $dateArr,
                'table_headging_arr' => $tableDateHeadingArr,
                'count_data_arr' => $countData
            ];
        }
        http_response_code(200);
        echo json_encode(['status' => http_response_code(), 'msg' => 'success', 'data' => $res]);
        die;
    }
    //getCurrentMonthSurveyCount
    public function getCurrentMonthSurveyCount()
    {
        $jData = json_decode(file_get_contents('php://input'), true);
        // echo '<pre>';
        // print_r($jData);die;
        $date = $jData['date'];
        $surveyour__id = $jData['surveyour__id'];
        $ward__id = isset($jData['ward__id']) ? $jData['ward__id'] : NULL;
        $start_date = '';
        $end_date = '';
        if ($date != '') {
            $dateArr = explode('to', $date);
            $start_date = $dateArr[0];
            $end_date = $dateArr[1];
        }
        $data = $this->SurveyModel->getCurrentMonthSureyCount($start_date, $end_date, $surveyour__id, $ward__id);
        // echo '<pre>';
        // print_r($data);die;
        http_response_code(200);
        echo json_encode(['status' => http_response_code(), 'msg' => 'Success', 'data' => $data]);
        die;
    }
    //getBannerSurveyList
    public function getBannerSurveyList()
    {
        // echo $apparel_type;
        // die;
        $start_date = NULL;
        $end_date = NULL;
        // echo '<pre>';
        // print_r($_POST);die;
        $date_range = $_POST['date_range'];
        if ($date_range != '') {
            $dateRArr = explode('to', $date_range);
            $start_date = date('Y-m-d', strtotime($dateRArr[0]));
            if (count($dateRArr) == 2)
                $end_date = date('Y-m-d', strtotime($dateRArr[1]));
        }
        $ward_ids  = isset($_POST['ward_ids']) ? $_POST['ward_ids'] : NULL;
        $banner_id  = isset($_POST['banner_id']) ? $_POST['banner_id'] : NULL;
        // echo $start_date . '|' . $end_date;
        // die;
        $result = $this->SurveyModel->getBannerSurveyList($_POST, $start_date, $end_date, $ward_ids,$banner_id);
        $allCount = $this->SurveyModel->countAllBannerSurveyDetail($start_date, $end_date, $ward_ids,$banner_id);
        $filteredCount = $this->SurveyModel->countFilterBannerSurveyDetail($_POST, $start_date, $end_date, $ward_ids,$banner_id);
        // 
        $final = array();
        if (!empty($result)) {
            foreach ($result as $readData) {
                $row = array();
                $row[] = isset($readData['banner_id']) ? $readData['banner_id']  : '--';
                $row[] = isset($readData['ward_name']) ? $readData['ward_name']  : '--';
                $row[] = isset($readData['location_lat']) ? '
                <iframe 
                width="250" 
                height="150" 
                frameborder="0" 
                scrolling="no" 
                marginheight="0" 
                marginwidth="0" 
                src="https://maps.google.com/maps?q=' . $readData['location_lat'] . ',' . $readData['location_lng'] . '&hl=en&z=14&amp;output=embed"
                >
                </iframe>
                <br />
                ' : '';
                $row[] = isset($readData['media_type_value']) ? $readData['media_type_value']  : '--';
                $row[] = isset($readData['address']) ? $readData['address']  : '--';
                $row[] = isset($readData['property_name']) ? $readData['property_name'] : '--';
                $row[] = isset($readData['society_name']) ? $readData['society_name'] : '--';
                $row[] = isset($readData['survey_date']) ? $readData['survey_date'] . ' ' . $readData['survey_time'] : '--';
                $action = '<div class="dropdown d-inline-block">
                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="ri-more-fill align-middle"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-end">
                                    <li><button type="button" onclick="openModal(this)" 
                                    data-id="' . base64_encode($readData['banner_id']) . '" 
                                    data-form-number="1"
                                    class="dropdown-item edit-item-btn">
                                    <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit Form 1</button>
                                    </li>
                                      <li><button type="button" onclick="openModal(this)" 
                                    data-id="' . base64_encode($readData['banner_id']) . '" 
                                    class="dropdown-item edit-item-btn"
                                      data-form-number="2"
                                    >
                                    <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit Form 2</button>
                                    </li>
                                      <li><button   type="button" onclick="openModal(this)" 
                                    data-id="' . base64_encode($readData['banner_id']) . '" 
                                      data-form-number="3"
                                    class="dropdown-item edit-item-btn">
                                    <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit Form 3</button>
                                    </li>
                                </ul>
                            </div>';
                $row[] = $action;
                $final[] = $row;
            }
        }
        $results = array(
            "sEcho" => $_POST['draw'],
            "iTotalRecords" => $allCount,
            "iTotalDisplayRecords" => $filteredCount,
            "aaData" => $final
        );
        echo json_encode($results);
    }
    //getBannerData
    public function getBannerData()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $reminderMode = array();
        $banner_id = base64_decode($data['banner_id']);
        $form_no = $data['formNo'];
        $mediaTypeData = $this->CommonModel->getMultipleData('media_type', '', '');
        $plateAvailableData = $this->CommonModel->getMultipleData('plate_available', '', '');
        $categoryData = $this->CommonModel->getMultipleData('category', NULL, ['category_id', 'category_value']);
        $govtTypeData = $this->CommonModel->getMultipleData('govt_type', NULL, ['govt_type_id', 'govt_type_value']);
        $installedOnData = $this->CommonModel->getMultipleData('installed_on', NULL, ['installed_on_id', 'installed_on_value']);
        $naturePropData = $this->CommonModel->getMultipleData('nature_of_property', NULL, ['nature_of_property_id', 'nature_of_property_value']);
        $ownerTypeData = $this->CommonModel->getMultipleData('owner_type', NULL, ['owner_type_id', 'owner_type_value']);
        $boardNatureData = $this->CommonModel->getMultipleData('board_nature', NULL, ['board_nature_id', 'board_nature_value']);
        $wardData = $this->CommonModel->getMultipleData('wards', NULL, ['ward_name_id', 'ward_name']);
        $oneHalfMeterRoadData = $this->CommonModel->getMultipleData('one_half_meters_from_road', NULL, ['one_and_a_half_meters_from_road_id', 'one_and_a_half_meters_from_road_value']);
        $noticeTypeData = $this->CommonModel->getMultipleData('notice_type', NULL, ['notice_type_id', 'notice_type_value']);
        $reminderMode = $this->CommonModel->getMultipleData('reminder_mode', NULL, ['reminder_mode_id', 'reminder_mode_value']);
        $bannerStatus = $this->CommonModel->getMultipleData('banner_status', ['is_active' => 1], ['id', 'banner_status_value']);
        $signboardAdditional = $this->CommonModel->getMultipleData('sign_board_details', ['banner_id' => $banner_id, 'sign_board_details_status' => 1], [
            'sign_board_id',
            'banner_id',
            'sn_length',
            'sn_breadth',
            'sn_area',
            'sn_additional_sq_ft',
            'sn_additional_ratable_value'
        ]);
        // echo $form_no;die;
        switch ($form_no) {
            case '1':
                $bannerData = $this->CommonModel->getSingleData('banner_survey', ['banner_id' => $banner_id], '');
                break;
            case '2':
                $bannerData = $this->CommonModel->getSingleData('banner_details_contact_survey', ['banner_id' => $banner_id], '');
                break;
            case '3':
                $bannerData = $this->CommonModel->getSingleData('notice_details', ['banner_id' => $banner_id], '');
                break;
        }
        http_response_code(200);
        echo json_encode([
            'status' => http_response_code(), 'msg' => 'success', 'banner_data' => $bannerData, 'media_data' => $mediaTypeData,
            'plate_data' => $plateAvailableData,
            'noticeTypeData' => $noticeTypeData,
            'categoryData' => $categoryData,
            'govtTypeData' => $govtTypeData,
            'installedOnData' => $installedOnData,
            'naturePropData' => $naturePropData,
            'ownerTypeData' => $ownerTypeData,
            'boardNatureData' => $boardNatureData,
            'wardData' => $wardData,
            'reminderMode' => $reminderMode,
            'bannerStatus' => $bannerStatus,
            'signboardAdditional' => $signboardAdditional,
            'oneHalfMeterRoadData' => $oneHalfMeterRoadData
        ]);
        die;
    }
    //update
    public function update()
    {
        if (empty($_POST)) {
            http_response_code(404);
            echo json_encode(['status' => http_response_code(), 'msg' => 'Required field missing']);
            die;
        }
        // echo '<pre>';
        // print_r($_POST);die;
        $formNumber = isset($_POST['form__number']) ?  $_POST['form__number']  : 0;
        // echo $formNumber;die;
        switch ($formNumber) {
            case 1:
                $response = $this->updateBannerSurveyData($_POST);
                if ($response) {
                    http_response_code(200);
                    $msg = 'Survey data updated';
                } else {
                    http_response_code(400);
                    $msg = 'Something went wrong.';
                }
                break;
            case 2:
                $response = $this->updateBannerContactDetailData($_POST);
                if ($response) {
                    http_response_code(200);
                    $msg = 'Survey Contact detail updated';
                } else {
                    http_response_code(400);
                    $msg = 'Something went wrong.';
                }
                break;
            case 3:
                $response = $this->updateBannerNoticeDetailData($_POST);
                if ($response) {
                    http_response_code(200);
                    $msg = 'Survey Notice detail updated';
                } else {
                    http_response_code(400);
                    $msg = 'Something went wrong.';
                }
                break;
        }
        echo json_encode(['status' => http_response_code(), 'msg' => $msg]);
        die;
    }
    private function updateBannerNoticeDetailData($POST)
    {
        // echo '<pre>';
        // print_r($POST);die;
        $old_data = $this->CommonModel->getSingleData('notice_details', ['banner_id' => $POST['banner__id']], '');
        $oldJsondata = json_encode($old_data);
        $updateArr = [
            'notice_type_id' => $POST['notice__type__id'],
            // 'remainder_one_mode'=>$POST['reminder__one__mode'], 
            // 'remainder_one_mode_date'=>$POST['reminder__two__mode__date'], 
            // 'remainder_one_mode_time'=>$POST['reminder__one__mode__time'], 
            // 'remainder_one_updated_by'=> $_SESSION['USER']['uid'],
            // 'remainder_two_mode'=>$POST['reminder__two__mode'], 
            // 'remainder_two_mode_date'=>$POST['reminder__two__mode__date'], 
            // 'remainder_two_mode_time'=>$POST['reminder__two__mode__time'], 
            // 'remainder_two_updated_by'=> $_SESSION['USER']['uid'],
            // 'remainder_three_mode'=>$POST['reminder__three__mode'], 
            // 'remainder_three_mode_date'=>$POST['reminder__three__mode__date'], 
            // 'remainder_three_mode_time'=>$POST['reminder__three__mode__time'], 
            // 'remainder_three_updated_by'=> $_SESSION['USER']['uid'],
            'remark' => $POST['remark'],
            // 'no_advertisement_count'=>$POST['number__of__advertisement__count'], 
            // 'no_plate_count'=>$POST['number__of__plate__count'], 
            'updated_date' => date('Y-m-d'),
            'updated_time' => date('H:i:s'),
            'updated_by' => $_SESSION['USER']['uid']
        ];
        // echo '<pre>';
        // print_r($updateArr);
        // die;
        $res = $this->CommonModel->updateData('notice_details', ['banner_id' => $_POST['banner__id']], $updateArr);
        if ($res) {
            $updatedData = $this->CommonModel->getSingleData('notice_details', ['banner_id' => $POST['banner__id']], '');
            $updatedJsondata = json_encode($updatedData);
            //insert log data
            $res = $this->CommonModel->insertData('logs', ['table_name' => 'notice_details', 'old_data' => $oldJsondata, 'new_data' => $updatedJsondata, 'updated_by' => $_SESSION['USER']['uid']]);
            return true;
        } else
            return false;
    }
    //deleteSignboardData
    public function deleteSignboardData()
    {
        $djson = json_decode(file_get_contents('php://input'), true);
        // echo '<pre>';
        // print_r($djson);
        // die;
        $id = $djson['id'];
        $count = $djson['count'];
        $additional_area = $djson['additional_area'];
        $additional_rateble = $djson['additional_rateble'];
        $total_area = $djson['total_area'];
        $total_rateble = $djson['total_rateble'];
        $banner_id = $djson['banner_id'];
        $isExist = $this->CommonModel->getSingleData('sign_board_details', ['sign_board_id' => $id], []);
        if (!empty($isExist)) {
            $res = $this->CommonModel->updateData('sign_board_details', ['sign_board_id' => $id], ['sign_board_details_status' => 0]);
            //update banner 
            $updateArr = [
                'sn_total_area_of_additional_signboard' => $additional_area,
                'sn_total_area_of_signboard' => $total_area,
                'sn_total_rateable_value_of_additional_signboard' => $additional_rateble,
                'sn_total_rateable_value_of_signboard' => $total_rateble,
                'sn_no_of_additional_signboards' => $count
            ];
            // echo '<pre>';
            // print_r($updateArr);die;
            $res = $this->CommonModel->updateData('banner_survey', ['banner_id' => $banner_id], $updateArr);
            http_response_code(200);
            echo json_encode(['status' => http_response_code(), 'msg' => 'Success']);
            die;
        } else {
            http_response_code(404);
            echo json_encode(['status' => http_response_code(), 'msg' => 'Data does not exist.']);
            die;
        }
    }
    private function updateBannerSurveyData($POST)
    {
       /*   echo '<pre>';
         print_r($_POST);die; */
        $old_data = $this->CommonModel->getSingleData('banner_survey', ['banner_id' => $POST['banner__id']], '');
        $oldJsondata = json_encode($old_data);
        $surveyLocation = explode(',', $POST['survey__location']);
        $bannerLocation = explode(',', $POST['banner__location']);
        /* echo '<pre>';
        print_r($POST);die; */
        switch ($POST['media__type']) { // 1 ->hoarding | 2-> signboard | 3-> lollypop
            case 1:
                $updateArr = [
                    'address' => $POST['address'],
                    'approx_banner_height' => $POST['approx_banner_height'] > 0 ? $POST['approx_banner_height'] : 0.00,
                    'approx_banner_length' => $POST['approx_banner_length'] > 0 ? $POST['approx_banner_length'] : 0.00,
                    'approx_banner_width' => $POST['approx_banner_width'] > 0 ? $POST['approx_banner_width'] : 0.00,
                    'banner_location_lat' => $bannerLocation[0] > 0 ? $bannerLocation[0] : 0.00,
                    'banner_location_lng' => $bannerLocation[1] > 0 ? $bannerLocation[1] : 0.00,
                    'landmark' => $POST['landmark'],
                    'location_lat' => $surveyLocation[0] > 0 ? $surveyLocation[0] : 0.00,
                    'location_lng' => $surveyLocation[1] > 0 ? $surveyLocation[1] : 0.00,
                    'property_contact_no' => $POST['property_contact_number'] != '' ? $POST['property_contact_number'] : NULL,
                    'property_name' => $POST['advertiser__name'],
                    'advertiser_address'=>isset($POST['advertisor__address']) ? $POST['advertisor__address'] : NULL,
                    'road_status' => $POST['road_status'],
                    'society_name' => $POST['society__name'],
                    'media_type_id' => $POST['media__type'],
                    'old_id' => $POST['old__id'],
                    'plate_available_id' => $POST['plate__available'],
                    'proper_road_name' => $POST['proper_roead_name'],
                    'updated_by' => $this->user_id,
                    'updated_date' => date('Y-m-d'),
                    'updated_time' => date('H:i:s'),
                    'remarks' => $POST['remarks'],
                    'banner_status_id' => $POST['banner__status__id'] > 0 ? $POST['banner__status__id'] : NULL,
                    'installed_on_id' => $POST['installed__on'],
                    'banner_owner_name' => $POST['banner__owner__name'],
                    'banner_owner_contact_no' => $POST['banner__contact__number'],
                    'property_no' => $POST['property__number'],
                    'existing_length' => $POST['exisiting__length'] > 0  ? $POST['exisiting__length']  : 0.00,
                    'existing_breadth' => $POST['existing__breadth'] > 0 ? $POST['existing__breadth'] : 0.00,
                    'existing_area' => $POST['existing__area'] > 0 ? $POST['existing__area'] : 0.00,
                    'actual_length' => $POST['actual__length'] > 0 ? $POST['actual__length'] : 0.00,
                    'actual_breadth' => $POST['actual__breadth'] > 0 ? $POST['actual__breadth'] : 0.00,
                    'actual_area' => $POST['actual__area'] > 0 ? $POST['actual__area'] : 0.00,
                    'difference_in_sqft' => $POST['diff__square__feet'] > 0 ? $POST['diff__square__feet'] : 0.00,
                    'additional_ratable_value' => $POST['additional__ratable__value'] > 0 ? $POST['additional__ratable__value'] : 0.00,
                    'new_square_feet' => $POST['new__square__feet'] > 0 ? $POST['new__square__feet'] : 0.00,
                    'new_ratable_value_in_square_feet' => $POST['new__ratable__value__square__feet'] > 0 ? $POST['new__ratable__value__square__feet'] : 0.00,
                    'one_and_a_half_meters_from_road_id' => $POST['1_5_meater_from_road'],
                    'board_nature_id' => $POST['board__nature'],
                    'ward_name_id'=>isset($POST['ward_name_id'])  ? $POST['ward_name_id'] : NULL,
                    // 'media_type_id' => $POST['media__type'],
                    // 'plate_available_id' => $POST['plate__available'],
                    // 'old_id' => $POST['old__id'],
                    // 'property_name' => $POST['property__name'],
                    // 'society_name' => $POST['society__name'],
                    // 'landmark' => $POST['landmark'],
                    // 'proper_road_name' => $POST['proper_roead_name'],
                    // 'road_status' => $POST['road_status'],
                    // 'property_contact_no' => $POST['property_contact_number'],
                    // 'approx_banner_height' => $POST['approx_banner_height'],
                    // 'approx_banner_length' => $POST['approx_banner_length'],
                    // 'approx_banner_width' => $POST['approx_banner_width'],
                    // 'address' => $POST['address'],
                    // 'remarks' => $POST['remarks'],
                    // 'ward_name_id' =>isset($POST['ward__name__id']) ? ($POST['ward__name__id'] > 0 ? $POST['ward__name__id'] : NULL) : NULL,
                    // 'banner_status_id' => isset($POST['banner__status__id']) ? ($POST['banner__status__id'] > 0 ?  $POST['banner__status__id'] : NULL) :NULL
                ];
                break;
            case 2;
      /*       echo '<pre>';
            print_r($POST);die;  */
                $updateArr = [
                    'address' => $POST['address'],
                    // 'approx_banner_height'=>$POST['approx_banner_height'] > 0 ? $POST['approx_banner_height'] : 0.00, 
                    // 'approx_banner_length'=>$POST['approx_banner_length'] > 0 ? $POST['approx_banner_length'] : 0.00, 
                    // 'approx_banner_width'=>$POST['approx_banner_width'] > 0 ? $POST['approx_banner_width'] : 0.00, 
                    // 'banner_location_lat'=>$bannerLocation[0] > 0 ? $bannerLocation[0] : 0.00, 
                    // 'banner_location_lng'=>$bannerLocation[1] > 0 ? $bannerLocation[1] :0.00, 
                    'landmark' => $POST['landmark'],
                    'location_lat' => $surveyLocation[0] > 0 ? $surveyLocation[0] : 0.00,
                    'location_lng' => $surveyLocation[1] > 0 ? $surveyLocation[1] : 0.00,
                    'property_contact_no' => $POST['property_contact_number'] != '' ? $POST['property_contact_number'] : NULL,
                    'property_name' => $POST['advertiser__name'],
                    'road_status' => $POST['road_status'],
                    'society_name' => $POST['society__name'],
                    'media_type_id' => $POST['media__type'],
                    // 'old_id'=>$POST['old__id'], 
                    // 'plate_available_id'=>$POST['plate__available'], 
                    'proper_road_name' => $POST['proper_roead_name'],
                    'updated_by' => $this->user_id,
                    'updated_date' => date('Y-m-d'),
                    'updated_time' => date('H:i:s'),
                    'remarks' => $POST['remarks'],
                    // 'banner_status_id'=>$POST['banner__status__id'] > 0 ? $POST['banner__status__id'] : NULL, 
                    'installed_on_id' => $POST['installed__on'],
                    'banner_owner_name' => $POST['banner__owner__name'],
                    'banner_owner_contact_no' => $POST['banner__contact__number'],
                    'property_no' => $POST['property__number'],
                    'advertiser_address'=>isset($POST['advertisor__address']) ? $POST['advertisor__address'] : NULL,
                    'ward_name_id'=> isset($POST['ward_name_id']) ? $POST['ward_name_id'] :  NULL,
                    'advertiser_address' => $POST['advertisor__address'],
                    'sn_length' => isset($POST['signboard__length'][0]) ? $POST['signboard__length'][0] : 0,
                    'sn_breadth' => isset($POST['signboard__breadth'][0]) ? $POST['signboard__breadth'][0] : 0,
                    'sn_area' =>isset($POST['signboard__sqft'][0]) ? $POST['signboard__sqft'][0]  : 0,
                    'sn_additional_sq_ft' => isset($POST['signboard__additional_sqft'][0]) ? $POST['signboard__additional_sqft'][0] : 0,
                    'sn_additional_ratable_value' => isset($POST['signboard__additional__rateble__value'][0]) ? $POST['signboard__additional__rateble__value'][0] : 0,
                    'sn_total_area_of_additional_signboard' => isset($POST['total__additional__area']) ? ($POST['total__additional__area'] > 0  ? $POST['total__additional__area'] : 0) : 0,
                    'sn_total_area_of_signboard' => isset($POST['total__area']) ? ($POST['total__area'] > 0 ? $POST['total__area'] : 0) : 0,
                    'sn_total_rateable_value_of_additional_signboard' => isset($POST['total__additional__rateble__value']) ? ($POST['total__additional__rateble__value'] >0 ? $POST['total__additional__rateble__value'] :0  ) : NULL,
                    'sn_total_rateable_value_of_signboard' => isset($POST['total__rateble__value']) ? ($POST['total__rateble__value'] > 0 ?  $POST['total__rateble__value'] : 0) : 0,
                    'sn_no_of_additional_signboards' => isset($POST['signboard__add__additional__rateble__value']) ? count($POST['signboard__add__additional__rateble__value']): $old_data['sn_no_of_additional_signboards'],
                    'signboard_type_id' => isset($POST['signboard__type__id']) ? $POST['signboard__type__id'] : 0,
                    'ward_name_id'=>isset($POST['ward_name_id']) ? $POST['ward_name_id'] : NULL,
                    'legalization_possibility_id' => isset($POST['legalization__possibility__id']) ? $POST['legalization__possibility__id'] : 0
                ];
                break;
        }
        // echo '<pre>';
        // print_r($updateArr);die;
        // echo '<pre>';
        // print_r($updateArr);die;
        // echo '<pre>';
        // print_r($_FILES);
        // die;
        $res = $this->CommonModel->updateData('banner_survey', ['banner_id' => $_POST['banner__id']], $updateArr);
        if (isset($_FILES['signboard__image1']['name']) && $_FILES['signboard__image1']['name'] <> '') {
            $image_name = $_POST['banner__id'] . '_image1';
            $res = $this->customlibrary->uploadImage($image_name, '/images_banner/signboard_images', $_FILES['signboard__image1']['name'], $_FILES['signboard__image1']['tmp_name']);
        }
        if (isset($_FILES['signboard__image2']['name']) && $_FILES['signboard__image2']['name'] <> '') {
            $image_name = $_POST['banner__id'] . '_image2';
            $res = $this->customlibrary->uploadImage($image_name, '/images_banner/signboard_images', $_FILES['signboard__image2']['name'], $_FILES['signboard__image2']['tmp_name']);
        }
  
        if ($POST['media__type'] == 2) {

   
    
  

            if(isset($_POST['signboard__additional__length'])):
            for ($i = 0; $i < count($_POST['signboard__additional__length']); $i++) {
                if($_POST['signboard__id'][$i] > 0)
                {
                    // echo "udpaete";die;
                    //update
                    $updateArr = [
                        
                        'sn_length' => $_POST['signboard__additional__length'][$i],
                        'sn_breadth' => $_POST['signboard__additional__breadth'][$i],
                        'sn_area' => $_POST['signboard__add__sqft'][$i],
                        'sn_additional_sq_ft' => $_POST['signboard__add__sqft'][$i],
                        'sn_additional_ratable_value' => $_POST['signboard__add__additional__rateble__value'][$i],
                        'survey_date' => $old_data['survey_date'],
                        'survey_time' => $old_data['survey_time'],
                        'user_id' => $this->user_id,
                    
                    ];
                    $res = $this->CommonModel->updateData('sign_board_details',['sign_board_id'=>$_POST['signboard__id'][$i]], $updateArr);
                    $id = $_POST['signboard__id'][$i];
                   
                    
                    //image 1
                    if (isset($_FILES['signboard__additional__image1']['name'][$i]) && $_FILES['signboard__additional__image1']['name'][$i] <> '') {
                        $image_name = $id . "_image1";
                        $res = $this->customlibrary->uploadImage($image_name, '/images_banner/signboard_images/additional_images', $_FILES['signboard__additional__image1']['name'][$i], $_FILES['signboard__additional__image1']['tmp_name'][$i]);
                    }
                    if (isset($_FILES['signboard__additional__image2']['name'][$i]) && $_FILES['signboard__additional__image2']['name'][$i] <> '') {
                        //image 2
                        $image_name = $id . "_image2";
                        $res = $this->customlibrary->uploadImage($image_name, '/images_banner/signboard_images/additional_images', $_FILES['signboard__additional__image2']['name'][$i], $_FILES['signboard__additional__image2']['tmp_name'][$i]);
                    }
                

                }
                else{
                    // echo "insert";die;
                $insertArr = [
                    'banner_id' => $_POST['banner__id'],
                    'sn_length' => $_POST['signboard__additional__length'][$i],
                    'sn_breadth' => $_POST['signboard__additional__breadth'][$i],
                    'sn_area' => $_POST['signboard__add__sqft'][$i],
                    'sn_additional_sq_ft' => $_POST['signboard__add__sqft'][$i],
                    'sn_additional_ratable_value' => $_POST['signboard__add__additional__rateble__value'][$i],
                    'survey_date' => $old_data['survey_date'],
                    'survey_time' => $old_data['survey_time'],
                    'user_id' => $this->user_id,
                    'sign_board_details_status' => 1
                ];
                $res = $this->CommonModel->insertData('sign_board_details', $insertArr);
                $id = $this->db->insert_id();
                // echo $id;
                // die;
                
                //image 1
                if (isset($_FILES['signboard__additional__image1']['name'][$i]) && $_FILES['signboard__additional__image1']['name'][$i] <> '') {
                    $image_name = $id . "_image1";
                    $res = $this->customlibrary->uploadImage($image_name, '/images_banner/signboard_images/additional_images', $_FILES['signboard__additional__image1']['name'][$i], $_FILES['signboard__additional__image1']['tmp_name'][$i]);
                // echo $res;die;
                }
                if (isset($_FILES['signboard__additional__image2']['name'][$i]) && $_FILES['signboard__additional__image2']['name'][$i] <> '') {
                    //image 2
                    $image_name = $id . "_image2";
                    $res = $this->customlibrary->uploadImage($image_name, '/images_banner/signboard_images/additional_images', $_FILES['signboard__additional__image2']['name'][$i], $_FILES['signboard__additional__image2']['tmp_name'][$i]);
                }
            }
            }
        endif;
        }
        if ($res) {
            $updatedData = $this->CommonModel->getSingleData('banner_survey', ['banner_id' => $POST['banner__id']], '');
            $updatedJsondata = json_encode($updatedData);
            //insert log data
            $res = $this->CommonModel->insertData('logs', ['table_name' => 'banner_survey', 'old_data' => $oldJsondata, 'new_data' => $updatedJsondata, 'updated_by' => $_SESSION['USER']['uid']]);
            return true;
        } else
            return false;
    }
    private function updateBannerContactDetailData($POST)
    {
        $old_data = $this->CommonModel->getSingleData('banner_details_contact_survey', ['banner_id' => $POST['banner__id']], '');
        $oldJsondata = json_encode($old_data);
        $updateArr = [
            'owner_name' => $POST['owner__name'],
            'owner_uid' => $POST['owner__uid'],
            'occupier_name' => $POST['occupier__name'],
            'occupier_uid' => $POST['occupier__uid'],
            // 'property_no'=> $POST['abc'], 
            'property_name' => $POST['property__name'],
            'proper_landmark' => $POST['proper__landmark'],
            'proper_road_name' => $POST['proper__road__name'],
            'more_details' => $POST['more__details'],
            'flat_no' => $POST['flat__number'],
            'house_no' => $POST['house__number'],
            'bungalow_no' => $POST['bunglow__number'],
            'pincode' => $POST['pincode'],
            'installation_year' => $POST['installation_year'],
            'license_renewed_upto' => $POST['license__renewed__till'],
            'property_owner_address' => $POST['prop__owner__address'],
            'property_owner_mobile_no' => $POST['property__owner__mobile__number'],
            'property_occupier_address' => $POST['prop__occupier__address'],
            // 'property_occupier_mobile_no'=> $POST['abc'], 
            'property_occupier_email' => $POST['property__occupier__email'],
            'name_of_shop' => $POST['name__of__shop'],
            'manufactured_sold_item' => $POST['manufactured___sold__item'],
            'age_of_building' => $POST['age__of__building'],
            'existing_length' => $POST['existing__length'],
            'existing_breadth' => $POST['existing__breadth'],
            'existing_area' => $POST['existing__area'],
            'actual_length' => $POST['actual__length'],
            'actual_breadth' => $POST['actual__breadth'],
            'actual_area' => $POST['actual__area'],
            'difference_in_sqft' => $POST['difference__in__sqft'],
            'additional_ratable_value' => $POST['additional__ratable__value'],
            'new_square_feet' => $POST['new__square__feet'],
            'new_ratable_value_in_square_feet' => $POST['new__rateble__value__squre__feet'],
            // 'ward_name_id'=> $POST['ward__id'], 
            'owner_type_id' => $POST['owner__type__id'],
            'govt_type_id' => $POST['govt__type__id'],
            'category_id' => $POST['category__id'],
            'nature_of_property_id' => $POST['nature__of__property__id'],
            'installed_on_id' => $POST['installed__on__id'],
            'one_and_a_half_meters_from_road_id' => $POST['one__half__meter__road__id'],
            'board_nature_id' => $POST['board__nature__id']
        ];
        // echo '<pre>';
        // print_r($updateArr);
        // die;
        $res = $this->CommonModel->updateData('banner_details_contact_survey', ['banner_id' => $POST['banner__id']], $updateArr);
        if ($res) {
            $updatedData = $this->CommonModel->getSingleData('banner_details_contact_survey', ['banner_id' => $POST['banner__id']], '');
            $updatedJsondata = json_encode($updatedData);
            //insert log data
            $res = $this->CommonModel->insertData('logs', ['table_name' => 'banner_details_contact_survey', 'old_data' => $oldJsondata, 'new_data' => $updatedJsondata, 'updated_by' => $_SESSION['USER']['uid']]);
            return true;
        } else
            return false;
    }
    public function downloadSurveyListCsv()
    {
        $dateRange = $this->uri->segment(2);
        $data = json_decode($dateRange);
        $start_date = 0;
        $end_date = 0;
        if ($dateRange != null) {
            $dateArr = explode('to', $dateRange);
            $start_data = str_replace('%20', '', $dateArr[0]);
            if (count($dateArr) == 2)
                $end_date = str_replace('%20', '', $dateArr[1]);
        }
        $name = 'banner-survey-list-' . date('YmdHiS') . '.csv';
        $header = [];
        $surveyData = $this->SurveyModel->getSurveyDataforCSV($start_data, $end_date);
        // echo '<pre>';
        // print_r($surveyData);die;
        $header = array_keys($surveyData[0]);
        $this->customlibrary->downloadCSV($header, $surveyData, $name);
    }
}
