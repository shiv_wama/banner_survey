<?php
class DashboardModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->surevey_table = 'banner_survey';
    }
    public function getDashboardCount()
    {
        $currDate = date('Y-m-d');
        $currMonth = date('m');
        $total_notice_signed_signboard = 0;
        $total_notice_signed_hoarding = 0;
        //toatal survey
        $totalSurveyHoarding = $this->db->from($this->surevey_table . ' t1')
            ->join('media_type mT', 't1.media_type_id = mT.media_type_id')
            ->like('mT.media_type_value', 'Hoarding', 'both')
            ->get()->num_rows();
        $totalSurveySignBoard = $this->db->from($this->surevey_table . ' t1')
            ->join('media_type mT', 't1.media_type_id = mT.media_type_id')
            ->like('mT.media_type_value', 'Sign Board', 'both')
            ->get()->num_rows();
        //ExistingCount 
        $this->db->from('banner_survey bS');
        $this->db->join('banner_status bnS', 'bS.banner_status_id = bnS.id');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        $this->db->like('mT.media_type_value', 'Hoarding', 'both');
        $this->db->like('bnS.banner_status_value', 'Existing Id found', 'both');
        $hoardingExistingCount = $this->db->get()->num_rows();
        $this->db->from('banner_survey bS');
        $this->db->join('banner_status bnS', 'bS.banner_status_id = bnS.id');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        $this->db->like('mT.media_type_value', 'Sign Board', 'both');
        $this->db->like('bnS.banner_status_value', 'Existing Id found', 'both');
        $signBoardExistingCount = $this->db->get()->num_rows();
        //illeagal
        $this->db->from('banner_survey bS');
        $this->db->join('banner_status bnS', 'bS.banner_status_id = bnS.id');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        $this->db->like('mT.media_type_value', 'Hoarding', 'both');
        $this->db->like('bnS.banner_status_value', 'Illegal', 'both');
        $hoardingIllegal = $this->db->get()->num_rows();


        $this->db->from('banner_survey bS');
        $this->db->join('banner_status bnS', 'bS.banner_status_id = bnS.id');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        $this->db->like('mT.media_type_value', 'Sign Board', 'both');
        $this->db->like('bnS.banner_status_value', 'Illegal', 'both');
        $signBoardIllegal = $this->db->get()->num_rows();
        // $existingCount = $totalSurvey - ($existing + $illegal);
        //signedProgress
        $this->db->from('banner_survey bS');
        $this->db->join('banner_status bnS', 'bS.banner_status_id = bnS.id');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        $this->db->like('mT.media_type_value', 'Hoarding', 'both');
        $this->db->like('bnS.banner_status_value', 'Ready For Sign', 'both');
        $hoardingSignedProgress = $this->db->get()->num_rows();

        $this->db->from('banner_survey bS');
        $this->db->join('banner_status bnS', 'bS.banner_status_id = bnS.id');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        $this->db->like('mT.media_type_value', 'Sign Board', 'both');
        $this->db->like('bnS.banner_status_value', 'Ready For Sign', 'both');
        $signBoardSignedProgress = $this->db->get()->num_rows();

        $this->db->from('banner_survey bS');
        $this->db->join('banner_status bnS', 'bS.banner_status_id = bnS.id');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        $this->db->like('mT.media_type_value', 'Hoarding', 'both');
        $this->db->like('bnS.banner_status_value', 'Notice Signed', 'both');
        $hoardingTotalNoticeSigned = $this->db->get()->num_rows();

        $this->db->from('banner_survey bS');
        $this->db->join('banner_status bnS', 'bS.banner_status_id = bnS.id');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        $this->db->like('mT.media_type_value', 'Sign Board', 'both');
        $this->db->like('bnS.banner_status_value', 'Notice Signed', 'both');
        $signBoardTotalNoticeSigned = $this->db->get()->num_rows();



        $this->db->from('banner_survey bS');
        $this->db->join('banner_status bnS', 'bS.banner_status_id = bnS.id');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        $this->db->like('mT.media_type_value', 'Sign Board', 'both');
        $this->db->like('bnS.banner_status_value', 'Notice delivered', 'both');
        $waiting_for_response_signbaord = $this->db->get()->num_rows();


        $this->db->from('banner_survey bS');
        $this->db->join('banner_status bnS', 'bS.banner_status_id = bnS.id');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        $this->db->like('mT.media_type_value', 'Hoarding', 'both');
        $this->db->like('bnS.banner_status_value', 'Notice delivered', 'both');
        $waiting_for_response_hoarding = $this->db->get()->num_rows();



        $this->db->from('banner_survey bS');
        $this->db->join('banner_status bnS', 'bS.banner_status_id = bnS.id');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        $this->db->like('mT.media_type_value', 'Sign Board', 'both');
        $this->db->like('bnS.banner_status_value', 'Approached for sanction', 'both');
        $approched_sanction_signbaord = $this->db->get()->num_rows();

        
        $this->db->from('banner_survey bS');
        $this->db->join('banner_status bnS', 'bS.banner_status_id = bnS.id');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        $this->db->like('mT.media_type_value', 'Hoarding', 'both');
        $this->db->like('bnS.banner_status_value', 'Approached for sanction', 'both');
        $approched_sanction_hoarding = $this->db->get()->num_rows();




        $this->db->from('banner_survey bS');
        $this->db->join('banner_status bnS', 'bS.banner_status_id = bnS.id');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        $this->db->like('mT.media_type_value', 'Sign Board', 'both');
        $this->db->like('bnS.banner_status_value', 'Ready For Action', 'both');
        $ready_for_action_signbaord = $this->db->get()->num_rows();

        
        $this->db->from('banner_survey bS');
        $this->db->join('banner_status bnS', 'bS.banner_status_id = bnS.id');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        $this->db->like('mT.media_type_value', 'Hoarding', 'both');
        $this->db->like('bnS.banner_status_value', 'Ready For Action', 'both');
        $ready_for_action_hoarding = $this->db->get()->num_rows();




        $this->db->from('banner_survey bS');
        $this->db->join('banner_status bnS', 'bS.banner_status_id = bnS.id');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        $this->db->like('mT.media_type_value', 'Sign Board', 'both');
        $this->db->like('bnS.banner_status_value', 'Legalized', 'both');
        $legalized_signbaord = $this->db->get()->num_rows();

        
        $this->db->from('banner_survey bS');
        $this->db->join('banner_status bnS', 'bS.banner_status_id = bnS.id');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        $this->db->like('mT.media_type_value', 'Hoarding', 'both');
        $this->db->like('bnS.banner_status_value', 'Legalized', 'both');
        $legalized_hoarding = $this->db->get()->num_rows();



        $this->db->from('banner_survey bS');
        $this->db->join('banner_status bnS', 'bS.banner_status_id = bnS.id');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        $this->db->like('mT.media_type_value', 'Sign Board', 'both');
        $this->db->like('bnS.banner_status_value', 'Demolished', 'both');
        $demolished_signbaord = $this->db->get()->num_rows();

        
        $this->db->from('banner_survey bS');
        $this->db->join('banner_status bnS', 'bS.banner_status_id = bnS.id');
        $this->db->join('media_type mT', 'bS.media_type_id = mT.media_type_id');
        $this->db->like('mT.media_type_value', 'Hoarding', 'both');
        $this->db->like('bnS.banner_status_value', 'Demolished', 'both');
        $demolished_hoarding = $this->db->get()->num_rows();

















        $notice_delivered_sign = $approched_sanction_signbaord + $ready_for_action_signbaord + $waiting_for_response_signbaord + $legalized_signbaord + $demolished_signbaord;
        $notice_delivered_hoarding = $approched_sanction_hoarding + $ready_for_action_hoarding + $waiting_for_response_hoarding + $legalized_hoarding + $demolished_hoarding;
       
        $notice_to_be_delivered_sign = (int)$signBoardTotalNoticeSigned;
        $notice_to_be_delivered_hord = (int)$hoardingTotalNoticeSigned;
        // $waiting_for_response = $notice_delivered;
        $total_notice_signed_signboard = $total_notice_signed_signboard + $notice_delivered_sign + $notice_to_be_delivered_sign;
        $total_notice_signed_hoarding = $total_notice_signed_hoarding + $notice_delivered_hoarding + $notice_to_be_delivered_hord;
        
        $illegalCount_hoarding =  $hoardingSignedProgress + $total_notice_signed_hoarding + $hoardingIllegal;
        $illegalCount_signvoard =  $signBoardSignedProgress + $total_notice_signed_signboard + $signBoardIllegal;

        
        
        

        // $illegalCount_hoarding = $hoardingSignedProgress + $hoardingTotalNoticeSigned + $;
        // $illegalCount_signvoard = $signBoardSignedProgress + $signBoardTotalNoticeSigned + $signBoardIllegal;
        $toBeVerifiedCount_hoarding = $totalSurveyHoarding - ($hoardingExistingCount + $illegalCount_hoarding);
        $toBeVerifiedCount_signboard = $totalSurveySignBoard - ($signBoardExistingCount + $illegalCount_signvoard);
        // //signboard
        // $this->db->from('banner_survey bS');
        // $this->db->join('media_type mT','bS.media_type_id = mT.media_type_id'); 
        // $this->db->like('mT.media_type_value','Sign Board','both');
        // $signboardCount = $this->db->get()->num_rows();
        // //hording
        // $this->db->from('banner_survey bS');
        // $this->db->join('media_type mT','bS.media_type_id = mT.media_type_id'); 
        // $this->db->like('mT.media_type_value','Hoarding','both');
        // $hordingCount = $this->db->get()->num_rows();
        // echo $hordingCount;die;
        $totalCreatedSurveyToday = $this->db->from($this->surevey_table)
            ->where('created_date', $currDate)
            ->or_where('updated_date', $currDate)
            ->get()->num_rows();
        $newCreatedSurveyToday = $this->db->from($this->surevey_table)
            ->where(['created_date' => $currDate])
            ->get()->num_rows();
        $query = 'SELECT * FROM ' . $this->surevey_table . ' where extract(month from created_date) = ' . $currMonth;
        $currentMonthSurveyTotal = $this->db->query($query)->num_rows();
        return [
            'total_survey_hoarding' => $totalSurveyHoarding,
            'total_survey_signboard' => $totalSurveySignBoard,
            // 'total_survey_created_today'=>$totalCreatedSurveyToday,
            // 'new_created_survey'=>$newCreatedSurveyToday,
            // 'current_month_survey'=>$currentMonthSurveyTotal,
            'existingCount_hoarding' => $hoardingExistingCount,
            'existingCount_signboard' => $signBoardExistingCount,
            'illegalCount_hoarding' => $illegalCount_hoarding,
            'illegalCount_signboard' => $illegalCount_signvoard,
            'toBeVerifiedCount_hoarding' => $toBeVerifiedCount_hoarding,
            'toBeVerifiedCount_signboard' => $toBeVerifiedCount_signboard,
            // 'signboardCount'=>$signboardCount,
            // 'hordingCount'=>$hordingCount,
        ];
    }
    //getLastSevenDaysSurveyData
    public function getLastSevenDaysSurveyData()
    {
        // $currDate = date('Y-m-d');
        $hordingArr = [];
        $signboardArr = [];
        $dateArr  = [];
        // $lastSevenDays = date('Y-m-d',strtotime('-15 days'));
        for ($i = 0; $i < 15; $i++) {
            array_push($dateArr, date('Y-m-d', strtotime('- ' . $i . ' days')));
        }
        for ($j = 0; $j < count($dateArr); $j++) {
            $hordingData =  $this->db->select('count(banner_id) as hording_count')
                ->from('banner_survey bS')
                ->join('media_type mT', 'bS.media_type_id = mT.media_type_id')
                ->like('mT.media_type_value', 'Hoarding', 'both')
                ->where(['bS.created_date' => $dateArr[$j]])
                ->get()->row_array();
            if (!empty($hordingData))
                array_push($hordingArr, (int)$hordingData['hording_count']);
            else
                array_push($hordingArr, 0);
            $signBoard =  $this->db->select('count(banner_id) as signboard_count')
                ->from('banner_survey bS')
                ->join('media_type mT', 'bS.media_type_id = mT.media_type_id')
                ->like('mT.media_type_value', 'Sign Board', 'both')
                ->where(['bS.created_date' => $dateArr[$j]])
                ->get()->row_array();
            if (!empty($signBoard))
                array_push($signboardArr, (int)$signBoard['signboard_count']);
            else
                array_push($signboardArr, 0);
        }
        return [
            'hoarding' => $hordingArr,
            'signboard' => $signboardArr,
            'dateArr' => $dateArr
        ];
    }
    //getMediaTypeData
    public function getMediaTypeData()
    {
        return $this->db->select('count(media_type_id) as count,media_type_id')
            ->from('banner_survey')
            ->where('media_type_id >', 0)
            ->group_by('media_type_id')
            // ->order_by('mT.media_type_id','DESC')
            ->get()->result_array();
    }
    //getAvailablePlateData
    public function getAvailablePlateData()
    {
        return $this->db->select('count(plate_available_id) as count,plate_available_id')
            ->from('banner_survey')
            ->group_by('plate_available_id')
            // ->order_by('mT.media_type_id','DESC')
            ->get()->result_array();
    }
}
