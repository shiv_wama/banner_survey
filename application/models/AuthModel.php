<?php
class AuthModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->table = "banner_user";
        $this->table2 = "user_roles";
    }



    ////////////////////////////////////-----getUserList --------------////////////////////////////////
    public function getUserList($postData)
    {
        $this->_get_datatables_query__banner__survey__list($postData);
        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function countAllUserDetail()
    {
        $this->db->from($this->table.' t1');
        $this->db->join($this->table2.' t2','t1.user_type = t2.role_type',"LEFT");
        return $this->db->count_all_results();
    }

    public function countFilterUserDetail($postData)
    {
        $this->_get_datatables_query__banner__survey__list($postData);
        $query = $this->db->get();
        return $query->num_rows();
    }

    private function _get_datatables_query__banner__survey__list($postData)
    {
        $this->db->select('
            t1.uid, 
            t1.first_name,    
            t1.last_name,    
            t1.email,       
            t1.mobile_no,    
            t1.user_type,    
            t1.sqid,  
            t1.enabled,   
            t2.user_role     
        ');
        $this->db->from($this->table . ' t1');
        $this->db->join($this->table2 . ' t2', 't1.user_type = t2.role_type','LEFT');
        $i = 0;
        // loop searchable columns
        $column_order = array(
            't1.uid', 
            't1.first_name',    
           
            't1.user_type',    
            't1.sqid',     
            't2.user_role'   
        );
        $column_search = array(
            't1.uid',
            't1.first_name',
            't1.last_name',
            't1.email',
            't1.mobile_no',
            't1.user_type',
            't2.user_role' 
        );
        $order = array('t1.uid' => 'DESC');
        foreach ($column_search as $item) {
            // if datatable send POST for search
            if ($postData['search']['value']) {
                // first loop
                if ($i === 0) {
                    $this->db->group_start();
                    if (((int)$postData['search']['value'] > 0)) {
                        $this->db->where($item, $postData['search']['value']);
                    }
                    else
                    $i++;
                } else {
                    $this->db->or_like($item, $postData['search']['value']);
                }

                // last loop
                if (count($column_search) - 1 == $i) {
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($postData['order'])) {
            $this->db->order_by($column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($order)) {
            $order = $order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    ///////////////////////////--------------getUserList end -----------------////////////////////////



}
