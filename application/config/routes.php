<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'AuthController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//AuthController
$route['logout'] = 'AuthController/logout';
$route['users-list'] = 'AuthController/users';
$route['get-user-data'] = 'AuthController/getUserData';
$route['store-user-data'] =  'AuthController/store';
$route['get-user-type'] = 'AuthController/getUserRoles';
$route['activate-user'] = 'AuthController/activateUser';

//DashboardController
$route['get-survey-data'] = 'DashboardController/getSurveyData';
$route['get-media-type-data'] = 'DashboardController/getMediaTypeData';
// $route['get-available-plate-data'] = 'DashboardController/getAvailablePlateData';
$route['get-existing-illegal-data'] = "DashboardController/getExistingIllegalGraphData";



$route['dashboard'] = 'DashboardController/index';
$route['survey-list'] = 'SurveyController/index';


$route['get-surveyour-counts'] = 'SurveyController/getSurveyourCounts';
$route['download-survey-list-csv/(:any)'] = 'SurveyController/downloadSurveyListCsv';
$route['survey-count']  ='SurveyController/surveyCount';
$route['sign-in'] = 'AuthController/signIn';
$route['get-banner-data'] = 'SurveyController/getBannerData';
$route['update-survey-data'] = 'SurveyController/update';
$route['delete-signboard-data'] = 'SurveyController/deleteSignboardData';

$route['map'] = 'MapController/index';
$route['get-map-lat-long'] = 'MapController/getMapLatLong';
$route['get-marker-data'] = 'MapController/getMarkerFormOneData';
$route['update-map-lat-long'] = 'MapController/updateMapLatLong';
$route['filter-map-data'] = 'MapController/filterMapData';


//MasterController
$route['master/(:any)'] = 'MasterController/editMaster';
$route['store-master-data'] = 'MasterController/store';
$route['delete-data'] = "MasterController/deleteData";
$route['get-master-data'] = 'MasterController/getMasterData';
$route['update-master-data'] = 'MasterController/updateData';
$route['change-map-marker'] = 'MasterController/updateMapMarker';

//WardSummaryController
$route['ward-summary-list'] = 'WardSummaryController/index';
$route['download-ward-summary'] = 'WardSummaryController/downloadWardSummaryCSV';

//Signboard
$route['signboard-list'] = 'Signboard/index';
//Report
$route['users-report'] = 'Report/index';