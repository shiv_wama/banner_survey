        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">
            <div class="page-content">
                <div class="container-fluid">
                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0">Banner Survey Count (Enumerator wise)</h4>
                                <!-- <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Tables</a></li>
                                        <li class="breadcrumb-item active">Datatables</li>
                                    </ol>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="mb-1">
                                                <label for="">Surveyour Name</label>
                                                <select multiple name="surveyour___id[]" id="surveyour__id" class="form-select">
                                                    <?php
                                                    if (isset($surveyour_data) && !empty($surveyour_data)) :
                                                        foreach ($surveyour_data as $key => $value) :
                                                            echo '<option value="' . $value['uid'] . '">' . $value['first_name'] . ' ' . $value['last_name'] . '</option>';
                                                        endforeach;
                                                    endif;
                                                    ?></select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <div class="mb-1">
                                                <a class="btn btn-primary btn-sm" id="btn__count" onclick="showFilteredCount();">Counts</a>
                                            </div>
                                        </div>

                                        <div class="col-md-12 ward__name__div" style="display:none">
                                            <div class="mb-1">
                                                <label for="">Ward Name</label>
                                                <select multiple name="ward__id[]" id="ward__ids" class="form-select">
                                                    <?php
                                                    if (isset($ward_data_arr) && !empty($ward_data_arr)) :
                                                        foreach ($ward_data_arr as $key => $value) :
                                                            echo '<option value="' . $value['ward_name_id'] . '">' . $value['ward_name'] . '</option>';
                                                        endforeach;
                                                    endif;
                                                    ?></select>
                                            </div>
                                        </div>


                                        <!--  -->
                                        <div class="col-md-3">
                                            <div class="mb-1">
                                                <label for="">Date Range</label>
                                                <input type="text" name="date_filter_count" data-provider="flatpickr" data-range-date="true" data-date-format="Y-m-d" id="date_filter_count" class="form-control survey_count_date_range">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <table  id="survey_count_datatable" class="banner__surveyour__count__datatable table nowrap align-middle" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>surveyor Name/ID</th>
                                                <!-- <th>Ward Name</th> -->
                                                <th> <?= date('d M, Y') ?> (Form1 Count)</th>
                                                <th><?= date('d M, Y') ?> (Form1 Updated Count)</th>
                                                <th>Total Count (Form1)</th>
                                                <th> <?= date('d M, Y') ?> (Form2 Count)</th>
                                                <th><?= date('d M, Y') ?> (Form2 Updated Count)</th>
                                                <th>Total Count (Form2)</th>
                                                <th> <?= date('d M, Y') ?> (Form3 Count)</th>
                                                <th><?= date('d M, Y') ?> (Form3 Updated Count)</th>
                                                <th>Total Count (Form3)</th>
                                            </tr>
                                        </thead>
                                        <tbody class="survey__count__tbody">
                                            <?php
                                            if (isset($surveyour_counts) && !empty($surveyour_counts)) :
                                                foreach ($surveyour_counts as $key => $value) :
                                                    // echo '<pre>';
                                                    // print_r($surveyour_counts);die;
                                                    echo '<tr>
                                                           <td>' . $value['name'] . '</td>
                                                          
                                                           ';
                                                           for ($i=0; $i < count($value['formDataArr']); $i++):
                                                            echo ' <td>' .   $value['formDataArr'][$i] . '</td>';
                                                           endfor;
                                                          
                                                           
                                                        echo '</tr>';
                                                endforeach;
                                            endif;
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>surveyor Name/ID</th>
                                                <!-- <th>Ward Name</th> -->
                                                <th> <?= date('d M, Y') ?> (Form1 Count)</th>
                                                <th><?= date('d M, Y') ?> (Form1 Updated Count)</th>
                                                <th>Total Count (Form1)</th>
                                                <th> <?= date('d M, Y') ?> (Form2 Count)</th>
                                                <th><?= date('d M, Y') ?> (Form2 Updated Count)</th>
                                                <th>Total Count (Form2)</th>
                                                <th> <?= date('d M, Y') ?> (Form3 Count)</th>
                                                <th><?= date('d M, Y') ?> (Form3 Updated Count)</th>
                                                <th>Total Count (Form3)</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>