        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

               

                    <!-- AIzaSyD5Y5rIwFn8d_H5dvGlZJR1BWYKLUahlf0 -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Banner Type</label>





                                                  <select name="banner_type" class="form-select media_id" onchange="filterMapData(this)" data-type="media_type">
                                                    <option value="" selected disabled>Please select media type</option>
                                                    <?php
                                                    if (isset($banner_type) && !empty($banner_type)) :
                                                        foreach ($banner_type as $key => $value) :
                                                            echo '<option value="' . $value['media_type_id'] . '">' . $value['media_type_value'] . '</option>';
                                                        endforeach;
                                                    endif;
                                                 
                                                    ?>
                                                       <option value="NULL">All</option>
                                                
                                                </select>


                                             
                                            </div>


                                            


                                            <div class="col-md-4">
                                                <label for="">Banner Id</label>
                                                <select name="banner_id"  class="form-select banner_id" onchange="filterMapData(this)" data-type="banner_id">
                                                    <option value="" selected disabled>Please select banner id</option>
                                                    <?php
                                                    if(isset($banner_id) && !empty($banner_id)):
                                                            foreach ($banner_id as $key => $value) {
                                                                echo '<option value="'.$value['banner_id'].'"># '.$value['banner_id'].'</option>';
                                                            }
                                                    endif;
                                                    
                                                    ?>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                

                                <div class="card-body">

                                    <div id="map" style="width: 100%; height: 520px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>